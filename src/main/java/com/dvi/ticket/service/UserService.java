package com.dvi.ticket.service;

import com.dvi.ticket.model.*;
import com.dvi.ticket.model.security.OauthClientDetails;
import com.dvi.ticket.repository.RoleRepository;
import com.dvi.ticket.repository.UserRepository;
import com.dvi.ticket.repository.UserRoleRepository;
import com.dvi.ticket.repository.security.OauthClientDetailsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private OauthClientDetailsRepository oauthClientDetailsRepository;
    @Autowired
    private TenantService tenantService;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private DataSource dataSource;

    @Transactional
    public User addUser(User user) {
        String encodedPassword = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(encodedPassword);
        return this.userRepository.save(user);
    }

    @Transactional
    public void updateQuery(User user, Tenant tenant) {
        this.userRepository.updateUser(user.getId(), tenant.getId());
    }

    @Transactional
    public void addUserRole(UserRole userRole) {
        this.userRoleRepository.save(userRole);
    }

    @Transactional
    public User updateUser(User user) {
        return this.userRepository.save(user);
    }

    @Transactional
    public void update(User user) {
        this.userRepository.save(user);
    }
    @Transactional
    public void remove(User user) {
        this.userRoleRepository.deleteAll(this.userRoleRepository.findAllByUser(user));
        this.userRepository.delete(user);
    }

    @Transactional
    public void updatePassword(User user) {
        String encodedPassword = new BCryptPasswordEncoder().encode(user.getPassword());
        user.setPassword(encodedPassword);
        this.userRepository.save(user);
    }

    @PostConstruct
    private void setupDefaultUser() {
        if (this.userRepository.count() == 0) {
            List<Role> roleList = new ArrayList<>();
            if (this.roleRepository.findByName("ROLE_USER") == null) {
                roleList.add(new Role("ROLE_USER"));
            }
            if (this.roleRepository.findByName("ROLE_ADMIN") == null) {
                roleList.add(new Role("ROLE_ADMIN"));
            }
            if (this.roleRepository.findByName("ROLE_SUPER_ADMIN") == null) {
                roleList.add(new Role("ROLE_SUPER_ADMIN"));
            }
            if (this.roleRepository.findByName("ROLE_AGENT") == null) {
                roleList.add(new Role("ROLE_AGENT"));
            }
            if (!roleList.isEmpty()) {
                this.roleRepository.saveAll(roleList);
            }

            String encodedPassword = new BCryptPasswordEncoder().encode("admin@1");
            User user = new User("admin@dvicore.com", encodedPassword, Arrays.asList(this.roleRepository.findByName("ROLE_USER"), this.roleRepository.findByName("ROLE_SUPER_ADMIN")));
            user.setFullName("Admin Dvi Core");
            user.setPhoneNumber("255700000000");
            user.setEnabled(true);
            user.setAccountNonLocked(true);
            user.setEmail("admin@dvicore.com");
            user.setTenant(this.tenantService.findFirstByNameContaining("Software"));
            this.userRepository.save(user);

            /*ResourceDatabasePopulator schemaDatabasePopulator = new ResourceDatabasePopulator(false, false, "UTF-8", new ClassPathResource("schema.sql"));
            schemaDatabasePopulator.execute(this.dataSource);
            ResourceDatabasePopulator resourceDatabasePopulator = new ResourceDatabasePopulator(false, false, "UTF-8", new ClassPathResource("data.sql"));
            resourceDatabasePopulator.execute(this.dataSource);*/
        }

        if (this.oauthClientDetailsRepository.count() == 0) {
            OauthClientDetails oauthClientDetails = new OauthClientDetails();
            oauthClientDetails.setClientId("dvicore");
            oauthClientDetails.setClientSecret(new BCryptPasswordEncoder().encode("admin@1"));
            oauthClientDetails.setScope("read,write,trust");
            oauthClientDetails.setAuthorizedGrantTypes("password,access_token,refresh_token,authorization_code'");
            oauthClientDetails.setAuthorities("ROLE_CLIENT,ROLE_TRUSTED_CLIENT");
            oauthClientDetails.setAccess_token_validity(0);
            oauthClientDetails.setRefreshTokenValidity(0);
            oauthClientDetails.setAutoapprove(true);
            this.oauthClientDetailsRepository.save(oauthClientDetails);
        }
    }

    @Transactional
    public Role addRole(Role role) {
        if(!role.getName().startsWith("ROLE_")) {
            role.setName("ROLE_" + role.getName());
        }
        return this.roleRepository.save(role);
    }

    @Transactional
    public UserRole addUserRole(User user, Role role) {
        UserRole userRole = new UserRole(user, role);
        return this.userRoleRepository.save(userRole);
    }
    @Transactional
    public void addUserRole(List<UserRole> userRoles) {
        this.userRoleRepository.saveAll(userRoles);
    }
    @Transactional
    public void updateUserRole(User user, Role role) {
        UserRole userRole = new UserRole(user, role);
        this.userRoleRepository.save(userRole);
    }

    @Transactional
    public void removeRole(Role role) {
        this.roleRepository.delete(role);
    }

    @Transactional
    public void removeUserRole(UserRole userRole) {
        this.userRoleRepository.delete(userRole);
    }

    @Transactional
    public void removeUserRole(List<UserRole> userRoles) {
        this.userRoleRepository.deleteAll(userRoles);
    }

    public User findById(long id) {
        return this.userRepository.findById(id);
    }

    public User findByUsername(String username) {
        return this.userRepository.findByUsername(username);
    }

    public User findByEmail(String email) {
        return this.userRepository.findByEmail(email);
    }

    public User findByUsernameOrEmail(String username, String email) {
        return this.userRepository.findByUsernameOrEmail(username, email);
    }

    public Role findRoleByRoleId(long id) {
        return this.roleRepository.findById(id);
    }

    public UserRole findUserRole(User user, Role role) {
        return this.userRoleRepository.findByUserAndRole(user, role);
    }

    public UserRole findFirstUserRoleByRoleId(long roleId) {
        return this.userRoleRepository.findFirstUserRoleByRoleId(roleId);
    }

    public List<UserRole> findUserRolesByUser(User user) {
        return this.userRoleRepository.findAllByUser(user);
    }

    public List<UserRole> findUserRoleByRole(Role role) {
        return this.userRoleRepository.findAllByRole(role);
    }

    public Role findRoleByName(String name) {
        return this.roleRepository.findByName(name);
    }

    public List<Role> findAllRoles() {
        return this.roleRepository.findAll();
    }

    public List<UserRole> findAllUserRoles(User user) {
        return this.userRoleRepository.findAllByUser(user);
    }

    public List<User> findAll() {
        return this.userRepository.findAll();
    }

    public List<Long> findAllByTenantAndStatus(Tenant tenant, String status, int limit) {
        return this.userRepository.findAllByTenantIdAndStatus(tenant.getId(), status, limit);
    }

    public Page<User> findAll(int page, int size, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.userRepository.findAll(PageRequest.of(page, size, Sort.by(sort).ascending()));
        } else {
            return this.userRepository.findAll(PageRequest.of(page, size, Sort.by(sort).descending()));
        }
    }

    public Page<User> findAll(String searchKey, int page, int size, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.userRepository.findAllByFullNameContainingOrUsernameContainingOrEmailContainingOrPhoneNumberContaining(searchKey, searchKey, searchKey, searchKey, PageRequest.of(page, size, Sort.by(sort).ascending()));
        } else {
            return this.userRepository.findAllByFullNameContainingOrUsernameContainingOrEmailContainingOrPhoneNumberContaining(searchKey, searchKey, searchKey, searchKey, PageRequest.of(page, size, Sort.by(sort).descending()));
        }
    }

    public Page<User> findAllAgents(UserGroup userGroup, String searchKey, int page, int size, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.userRepository.findAllByUserGroupAndFullNameContainingOrUsernameContainingOrEmailContainingOrPhoneNumberContaining(userGroup, searchKey, searchKey, searchKey, searchKey, PageRequest.of(page, size, Sort.by(sort).ascending()));
        } else {
            return this.userRepository.findAllByUserGroupAndFullNameContainingOrUsernameContainingOrEmailContainingOrPhoneNumberContaining(userGroup, searchKey, searchKey, searchKey, searchKey, PageRequest.of(page, size, Sort.by(sort).descending()));
        }
    }

    public Page<User> findAllAgents(Tenant tenant, UserGroup userGroup, String searchKey, int page, int size, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.userRepository.findAllBySearch(tenant.getId(), userGroup.getId(), searchKey, PageRequest.of(page, size, Sort.by(sort).ascending()));
        } else {
            return this.userRepository.findAllBySearch(tenant.getId(), userGroup.getId(), searchKey, PageRequest.of(page, size, Sort.by(sort).descending()));
        }
    }
}
