package com.dvi.ticket.service;
import com.dvi.ticket.model.*;
import com.dvi.ticket.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TicketSettingsService {
    private  DictionaryItem lockSemantic;
    private Status status;
    private Topic topic;
    private TicketSettingsRepository ticketSettingsRepository;
    private StatusRepository statusRepository;
    private TopicRepository topicRepository;

    private TicketSetting ticketSettings;
    private DictionaryRepository dictionaryRepository;
    private DictionaryItemRepository dictionaryItemRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public  TicketSettingsService(TopicRepository topicRepository,StatusRepository statusRepository,DictionaryItemRepository dictionaryItemRepository,TicketSettingsRepository ticketSettingsRepository,DictionaryRepository dictionaryRepository){
        this.ticketSettingsRepository=ticketSettingsRepository;
        this.dictionaryRepository=dictionaryRepository;
        this.dictionaryItemRepository=dictionaryItemRepository;
        this.statusRepository=statusRepository;
        this.topicRepository=topicRepository;
    }
    @Transactional
    public TicketSetting save(TicketSetting ticketSettingsItem){
        return this.ticketSettingsRepository.save(ticketSettingsItem);
    }

    public TicketSetting findById(long id) {
        return this.ticketSettingsRepository.findById(id);
    }
    public TicketSetting findTopByOrderByIdDesc() {
        return this.ticketSettingsRepository.findTopByOrderByIdDesc();
    }

    @PostConstruct
    private void setupDefaultUser() {
        if (this.ticketSettingsRepository.count() == 0) {
            TicketSetting ticketSettings=new TicketSetting();
                lockSemantic = dictionaryItemRepository.findByCode("LOA");
                status = statusRepository.findByName("Open");
                topic = topicRepository.findByName("Default");

                ticketSettings.setLockSemantic(lockSemantic);
                ticketSettings.setDefaultStatus(status);
                ticketSettings.setDefaultTopic(topic);
                ticketSettings.setDefaultNumberFormat("12345678");
                this.ticketSettingsRepository.save(ticketSettings);
            }
        }

    @Transactional
    public void update(TicketSetting ticketSettings)
    {
        this.ticketSettingsRepository.save(ticketSettings);
    }

}
