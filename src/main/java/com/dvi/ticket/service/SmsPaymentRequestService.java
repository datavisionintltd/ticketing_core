package com.dvi.ticket.service;

import com.dvi.ticket.model.SmsPaymentRequest;
import com.dvi.ticket.model.Tenant;
import com.dvi.ticket.repository.SmsPaymentRequestRepository;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SmsPaymentRequestService {

    private SmsPaymentRequestRepository smsPaymentRequestRepository;

    public SmsPaymentRequestService(SmsPaymentRequestRepository smsPaymentRequestRepository) {
        this.smsPaymentRequestRepository = smsPaymentRequestRepository;
    }

    @Transactional
    public void addSmsPaymentRequest(SmsPaymentRequest smsPaymentRequest) {
        this.smsPaymentRequestRepository.save(smsPaymentRequest);
    }
    @Transactional
    public void update(SmsPaymentRequest smsPaymentRequest) {
        this.smsPaymentRequestRepository.save(smsPaymentRequest);
    }

    public SmsPaymentRequest findById(long id) {
        return this.smsPaymentRequestRepository.findById(id);
    }

    public SmsPaymentRequest findByTenant(Tenant tenant) {
        return this.smsPaymentRequestRepository.findByTenant(tenant);
    }

    public SmsPaymentRequest findByPaymentReference(String paymentReference) {
        return this.smsPaymentRequestRepository.findByPaymentReference(paymentReference);
    }
}
