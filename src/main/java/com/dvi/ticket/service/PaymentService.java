package com.dvi.ticket.service;

import com.dvi.ticket.model.Payment;
import com.dvi.ticket.model.Tenant;
import com.dvi.ticket.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PaymentService {

    private PaymentRepository paymentRepository;

    @Autowired
    public PaymentService(PaymentRepository paymentRepository) {
        this.paymentRepository = paymentRepository;
    }

    @Transactional
    public Payment addPayment(Payment payment) {
        return this.paymentRepository.save(payment);
    }

    @Transactional
    public void update(Payment payment) {
        this.paymentRepository.save(payment);
    }

    @Transactional
    public void remove(Payment payment) {
        this.paymentRepository.delete(payment);
    }

    public Payment findById(long id) {
        return this.paymentRepository.findById(id);
    }

    public Page<Payment> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.paymentRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.paymentRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Payment> findAll(String searchKey, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.paymentRepository.findAllBySearchKey(searchKey, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.paymentRepository.findAllBySearchKey(searchKey, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Payment> findAll(Tenant tenant, String searchKey, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.paymentRepository.findAllByTenantIdAndSearchKey(tenant.getId(), searchKey, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.paymentRepository.findAllByTenantIdAndSearchKey(tenant.getId(), searchKey, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
