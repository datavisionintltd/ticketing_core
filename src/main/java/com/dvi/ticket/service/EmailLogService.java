package com.dvi.ticket.service;

import com.dvi.ticket.model.EmailLog;
import com.dvi.ticket.repository.EmailLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EmailLogService {

    private EmailLogRepository emailLogRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public EmailLogService(EmailLogRepository emailLogRepository) {
        this.emailLogRepository = emailLogRepository;
    }

    @Transactional
    public EmailLog addEmailLog(EmailLog emailLog) {
        return this.emailLogRepository.save(emailLog);
    }

    @Transactional
    public void update(EmailLog emailLog) {
        this.emailLogRepository.save(emailLog);
    }

    @Transactional
    public void remove(EmailLog emailLog) {
        this.emailLogRepository.delete(emailLog);
    }

    public EmailLog findById(long id) {
        return this.emailLogRepository.findById(id);
    }

    public Page<EmailLog> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.emailLogRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.emailLogRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
