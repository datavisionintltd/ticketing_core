package com.dvi.ticket.service;

import com.dvi.ticket.model.Project;
import com.dvi.ticket.model.Tenant;
import com.dvi.ticket.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProjectService {

    private ProjectRepository projectRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Transactional
    public Project addProject(Project project) {
        return this.projectRepository.save(project);
    }

    @Transactional
    public void update(Project project) {
        this.projectRepository.save(project);
    }

    @Transactional
    public void remove(Project project) {
        this.projectRepository.delete(project);
    }

    public Project findById(long id) {
        return this.projectRepository.findById(id);
    }

    public List<Long> findAll(Tenant tenant, String status, int limit) {
        return this.projectRepository.findAllByTenantIdAndStatus(tenant.getId(), status, limit);
    }

    public Page<Project> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.projectRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.projectRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Project> findAll(String name, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.projectRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.projectRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
