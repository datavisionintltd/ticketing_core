package com.dvi.ticket.service;

import com.dvi.ticket.model.TenantSubscription;
import com.dvi.ticket.repository.TenantSubscriptionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TenantSubscriptionService {

    private TenantSubscriptionRepository tenantSubscriptionRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TenantSubscriptionService(TenantSubscriptionRepository tenantSubscriptionRepository) {
        this.tenantSubscriptionRepository = tenantSubscriptionRepository;
    }

    @Transactional
    public TenantSubscription addTenantSubscription(TenantSubscription tenantSubscription) {
        return this.tenantSubscriptionRepository.save(tenantSubscription);
    }

    @Transactional
    public void update(TenantSubscription tenantSubscription) {
        this.tenantSubscriptionRepository.save(tenantSubscription);
    }

    public void remove(long tenantSubscriptionId) {
        TenantSubscription tenantSubscription = this.findById(tenantSubscriptionId);
        this.tenantSubscriptionRepository.delete(tenantSubscription);
    }

    public TenantSubscription findById(long id) {
        return this.tenantSubscriptionRepository.findById(id);
    }

    public Page<TenantSubscription> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.tenantSubscriptionRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.tenantSubscriptionRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
