package com.dvi.ticket.service;

import com.dvi.ticket.model.SmsFee;
import com.dvi.ticket.model.Tenant;
import com.dvi.ticket.repository.SmsFeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

@Service
public class SmsFeeService {

    @Autowired
    private SmsFeeRepository smsFeeRepository;

    @Transactional
    public SmsFee newSmsFee(SmsFee smsFee) {
        return this.smsFeeRepository.save(smsFee);
    }
    @Transactional
    public void addSmsFee(SmsFee smsFee) {
        this.smsFeeRepository.save(smsFee);
    }
    @Transactional
    public SmsFee updateSmsFee(SmsFee smsFee) {
        return this.smsFeeRepository.save(smsFee);
    }
    @Transactional
    public void update(SmsFee smsFee) {
        this.smsFeeRepository.save(smsFee);
    }

    public SmsFee findById(long id) {
        return this.smsFeeRepository.findById(id);
    }

    public Page<SmsFee> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.smsFeeRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.smsFeeRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<SmsFee> findAll(Tenant tenant, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.smsFeeRepository.findAllByTenant(tenant, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.smsFeeRepository.findAllByTenant(tenant, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
