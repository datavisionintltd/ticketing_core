package com.dvi.ticket.service;

import com.dvi.ticket.model.TenantSetting;
import com.dvi.ticket.repository.TenantSettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TenantSettingService {

    private TenantSettingRepository tenantSettingRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TenantSettingService(TenantSettingRepository tenantSettingRepository) {
        this.tenantSettingRepository = tenantSettingRepository;
    }

    @Transactional
    public TenantSetting addTenantSetting(TenantSetting tenantSetting) {
        return this.tenantSettingRepository.save(tenantSetting);
    }

    @Transactional
    public void update(TenantSetting tenantSetting) {
        this.tenantSettingRepository.save(tenantSetting);
    }

    public void remove(TenantSetting tenantSetting) {
        this.tenantSettingRepository.delete(tenantSetting);
    }

    public TenantSetting findById(long id) {
        return this.tenantSettingRepository.findById(id);
    }

    public Page<TenantSetting> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.tenantSettingRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.tenantSettingRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
