package com.dvi.ticket.service;

import com.dvi.ticket.model.SmsBalance;
import com.dvi.ticket.model.Tenant;
import com.dvi.ticket.repository.SmsBalanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SmsBalanceService {

    @Autowired
    private SmsBalanceRepository smsBalanceRepository;

    @Transactional
    public SmsBalance newSmsBalance(SmsBalance smsBalance) {
        return this.smsBalanceRepository.save(smsBalance);
    }
    @Transactional
    public void addSmsBalance(SmsBalance smsBalance) {
        this.smsBalanceRepository.save(smsBalance);
    }
    @Transactional
    public SmsBalance updateSmsBalance(SmsBalance smsBalance) {
        return this.smsBalanceRepository.save(smsBalance);
    }
    @Transactional
    public void update(SmsBalance smsBalance) {
        this.smsBalanceRepository.save(smsBalance);
    }

    public SmsBalance findById(long id) {
        return this.smsBalanceRepository.findById(id);
    }

    public SmsBalance findByTenant(Tenant tenant) {
        return this.smsBalanceRepository.findByTenant(tenant);
    }
}
