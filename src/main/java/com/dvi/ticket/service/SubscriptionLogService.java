package com.dvi.ticket.service;

import com.dvi.ticket.model.SubscriptionLog;
import com.dvi.ticket.repository.SubscriptionLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SubscriptionLogService {

    private SubscriptionLogRepository subscriptionLogRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public SubscriptionLogService(SubscriptionLogRepository subscriptionLogRepository) {
        this.subscriptionLogRepository = subscriptionLogRepository;
    }

    @Transactional
    public SubscriptionLog addSubscriptionLog(SubscriptionLog subscriptionLog) {
        return this.subscriptionLogRepository.save(subscriptionLog);
    }

    @Transactional
    public void update(SubscriptionLog subscriptionLog) {
        this.subscriptionLogRepository.save(subscriptionLog);
    }

    @Transactional
    public void remove(SubscriptionLog subscriptionLog) {
        this.subscriptionLogRepository.delete(subscriptionLog);
    }

    public SubscriptionLog findById(long id) {
        return this.subscriptionLogRepository.findById(id);
    }

    public Page<SubscriptionLog> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.subscriptionLogRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.subscriptionLogRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
