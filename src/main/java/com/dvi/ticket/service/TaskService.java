package com.dvi.ticket.service;

import com.dvi.ticket.model.Task;
import com.dvi.ticket.model.Ticket;
import com.dvi.ticket.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TaskService {

    private TaskRepository taskRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Transactional
    public Task addTask(Task task) {
        return this.taskRepository.save(task);
    }

    @Transactional
    public void update(Task task) {
        this.taskRepository.save(task);
    }

    @Transactional
    public void remove(Task task) {
        this.taskRepository.delete(task);
    }

    public Task findById(long id) {
        return this.taskRepository.findById(id);
    }

    public Page<Task> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.taskRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.taskRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Task> findAll(String name, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.taskRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.taskRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Task> findAll(Ticket ticket, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.taskRepository.findAllByTicket(ticket, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.taskRepository.findAllByTicket(ticket, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
