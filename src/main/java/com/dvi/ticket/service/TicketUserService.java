package com.dvi.ticket.service;

import com.dvi.ticket.model.TicketUser;
import com.dvi.ticket.repository.TicketUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TicketUserService {

    private TicketUserRepository ticketUserRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TicketUserService(TicketUserRepository ticketUserRepository) {
        this.ticketUserRepository = ticketUserRepository;
    }

    @Transactional
    public TicketUser addTicketUser(TicketUser ticketUser) {
        return this.ticketUserRepository.save(ticketUser);
    }

    @Transactional
    public void update(TicketUser ticketUser) {
        this.ticketUserRepository.save(ticketUser);
    }

    @Transactional
    public void remove(TicketUser ticketUser) {
        this.ticketUserRepository.delete(ticketUser);
    }

    public TicketUser findById(long id) {
        return this.ticketUserRepository.findById(id);
    }

    public Page<TicketUser> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketUserRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketUserRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
