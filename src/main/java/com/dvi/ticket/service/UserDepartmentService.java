package com.dvi.ticket.service;

import com.dvi.ticket.model.Department;
import com.dvi.ticket.model.User;
import com.dvi.ticket.model.UserDepartment;
import com.dvi.ticket.repository.UserDepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserDepartmentService {

    private UserDepartmentRepository userDepartmentRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public UserDepartmentService(UserDepartmentRepository userDepartmentRepository) {
        this.userDepartmentRepository = userDepartmentRepository;
    }

    @Transactional
    public UserDepartment addUserDepartment(UserDepartment userDepartment) {
        return this.userDepartmentRepository.save(userDepartment);
    }

    @Transactional
    public void update(UserDepartment userDepartment) {
        this.userDepartmentRepository.save(userDepartment);
    }

    @Transactional
    public void remove(UserDepartment userDepartment) {
        this.userDepartmentRepository.delete(userDepartment);
    }

    public UserDepartment findById(long id) {
        return this.userDepartmentRepository.findById(id);
    }

    public UserDepartment findByUser(User user) {
        return this.userDepartmentRepository.findByUser(user);
    }

    public Page<UserDepartment> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.userDepartmentRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.userDepartmentRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<UserDepartment> findAll(Department department, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.userDepartmentRepository.findAllByDepartment(department, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.userDepartmentRepository.findAllByDepartment(department, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
