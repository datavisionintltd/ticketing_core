package com.dvi.ticket.service;

import com.dvi.ticket.model.ValidatePaymentLog;
import com.dvi.ticket.repository.ValidatePaymentLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ValidatePaymentLogService {

    private ValidatePaymentLogRepository validatePaymentLogRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public ValidatePaymentLogService(ValidatePaymentLogRepository validatePaymentLogRepository) {
        this.validatePaymentLogRepository = validatePaymentLogRepository;
    }

    @Transactional
    public ValidatePaymentLog addValidatePaymentLog(ValidatePaymentLog validatePaymentLog) {
        return this.validatePaymentLogRepository.save(validatePaymentLog);
    }

    @Transactional
    public void update(ValidatePaymentLog validatePaymentLog) {
        this.validatePaymentLogRepository.save(validatePaymentLog);
    }

    @Transactional
    public void remove(ValidatePaymentLog validatePaymentLog) {
        this.validatePaymentLogRepository.delete(validatePaymentLog);
    }

    public ValidatePaymentLog findById(long id) {
        return this.validatePaymentLogRepository.findById(id);
    }

    public Page<ValidatePaymentLog> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.validatePaymentLogRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.validatePaymentLogRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
