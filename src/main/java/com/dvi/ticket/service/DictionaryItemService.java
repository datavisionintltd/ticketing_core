package com.dvi.ticket.service;

import com.dvi.ticket.model.Dictionary;
import com.dvi.ticket.model.DictionaryItem;
import com.dvi.ticket.repository.DictionaryItemRepository;
import com.dvi.ticket.repository.DictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DictionaryItemService {
    Dictionary dictionary;
    private DictionaryItemRepository dictionaryItemRepository;
    private DictionaryRepository dictionaryRepository;
    private DictionaryItem lockSemanticsItem,languageItem,regionsItem,districtsItem,dateFormatsItem,timeZonesItem,pageSizeItem;
@PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public DictionaryItemService(DictionaryItemRepository dictionaryItemRepository, DictionaryRepository dictionaryRepository){
        this.dictionaryItemRepository=dictionaryItemRepository;
        this.dictionaryRepository=dictionaryRepository;
    }
    public DictionaryItem findById(long id) {
        return this.dictionaryItemRepository.findById(id);
    }
    @Transactional
   public DictionaryItem create(DictionaryItem dictionaryItem){
       return this.dictionaryItemRepository.save(dictionaryItem);
   }
   @Transactional
    public DictionaryItem update(DictionaryItem dictionaryItem){
        return this.dictionaryItemRepository.save(dictionaryItem);
   }
   @Transactional
    public void delete(DictionaryItem dictionaryItem){
        this.dictionaryItemRepository.delete(dictionaryItem);
   }
  /* @Transactional
    public List<DictionaryItem> list(Dictionary dictionary){
       return this.dictionaryItemRepository.findAllByDicctionary(dictionary);
   }*/
    public Page<DictionaryItem> findAll(Dictionary dictionary, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.dictionaryItemRepository.findAllByDictionary(dictionary, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.dictionaryItemRepository.findAllByDictionary(dictionary, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
    @PostConstruct
    private void setupDefaultDictionary() {

        if (this.dictionaryItemRepository.findByCode("LOA") == null) {
            dictionary=dictionaryRepository.findByCode("LCKSM");
            lockSemanticsItem = new DictionaryItem();
            lockSemanticsItem.setCode("LOA");
            lockSemanticsItem.setName("Lock On Activity");
            dictionaryItemRepository.save(lockSemanticsItem);
        }

        if (this.dictionaryItemRepository.findByCode("EN") == null) {
            dictionary=dictionaryRepository.findByCode("LNG");
            languageItem = new DictionaryItem();
            languageItem.setCode("EN");
            languageItem.setName("English");
            dictionaryItemRepository.save(languageItem);
        }
        if(this.dictionaryItemRepository.findByCode("FMT1") == null) {
            dictionary=dictionaryRepository.findByCode("DATFMTS");
            dateFormatsItem = new DictionaryItem();
            dateFormatsItem.setCode("FMT1");
            dateFormatsItem.setName("yyyy-mm-dd");
            dictionaryItemRepository.save(dateFormatsItem);
        }
        if(this.dictionaryItemRepository.findByCode("AFDSM") == null) {
            dictionary=dictionaryRepository.findByCode("ZON");
            timeZonesItem = new DictionaryItem();
            timeZonesItem.setDictionary(dictionary);
            timeZonesItem.setCode("AFDSM");
            timeZonesItem.setName("Africa/Dar es Salaam");
            dictionaryItemRepository.save(timeZonesItem);
        }
        if(this.dictionaryItemRepository.findByCode("TEN") == null) {
            dictionary=dictionaryRepository.findByCode("PGSZ");
            pageSizeItem = new DictionaryItem();
            pageSizeItem.setDictionary(dictionary);
            pageSizeItem.setCode("TEN");
            pageSizeItem.setName("10");
            dictionaryItemRepository.save(pageSizeItem);
        }

    }
}
