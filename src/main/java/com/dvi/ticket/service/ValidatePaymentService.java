package com.dvi.ticket.service;

import com.dvi.ticket.model.ValidatePayment;
import com.dvi.ticket.repository.ValidatePaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ValidatePaymentService {

    private ValidatePaymentRepository validatePaymentRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public ValidatePaymentService(ValidatePaymentRepository validatePaymentRepository) {
        this.validatePaymentRepository = validatePaymentRepository;
    }

    @Transactional
    public ValidatePayment addValidatePayment(ValidatePayment validatePayment) {
        return this.validatePaymentRepository.save(validatePayment);
    }

    @Transactional
    public void update(ValidatePayment validatePayment) {
        this.validatePaymentRepository.save(validatePayment);
    }

    @Transactional
    public void remove(ValidatePayment validatePayment) {
        this.validatePaymentRepository.delete(validatePayment);
    }

    public ValidatePayment findById(long id) {
        return this.validatePaymentRepository.findById(id);
    }

    public ValidatePayment findByMkey(String mkey) {
        return this.validatePaymentRepository.findByMkey(mkey);
    }
}
