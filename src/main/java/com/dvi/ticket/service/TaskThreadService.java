package com.dvi.ticket.service;

import com.dvi.ticket.model.Task;
import com.dvi.ticket.model.TaskThread;
import com.dvi.ticket.repository.TaskThreadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TaskThreadService {

    private TaskThreadRepository taskThreadRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TaskThreadService(TaskThreadRepository taskThreadRepository) {
        this.taskThreadRepository = taskThreadRepository;
    }

    @Transactional
    public TaskThread addTaskThread(TaskThread taskThread) {
        return this.taskThreadRepository.save(taskThread);
    }

    @Transactional
    public void update(TaskThread taskThread) {
        this.taskThreadRepository.save(taskThread);
    }

    @Transactional
    public void remove(TaskThread taskThread) {
        this.taskThreadRepository.delete(taskThread);
    }

    public TaskThread findById(long id) {
        return this.taskThreadRepository.findById(id);
    }

    public Page<TaskThread> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.taskThreadRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.taskThreadRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<TaskThread> findAll(Task task, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.taskThreadRepository.findAllByTask(task, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.taskThreadRepository.findAllByTask(task, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
