package com.dvi.ticket.service;

import com.dvi.ticket.model.UserGroup;
import com.dvi.ticket.repository.UserGroupRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class UserGroupService {
    
    private UserGroupRepository userGroupRepository;
    
    public UserGroupService(UserGroupRepository userGroupRepository) {
        this.userGroupRepository = userGroupRepository;
    }

    @Transactional
    public UserGroup addUserGroup(UserGroup userGroup) {
        return this.userGroupRepository.save(userGroup);
    }

    @Transactional
    public void update(UserGroup userGroup) {
        this.userGroupRepository.save(userGroup);
    }

    @Transactional
    public void update(List<UserGroup> countries) {
        this.userGroupRepository.saveAll(countries);
    }

    public void remove(UserGroup userGroup) {
        this.userGroupRepository.delete(userGroup);
    }

    public UserGroup findById(long id) {
        return this.userGroupRepository.findById(id);
    }

    public UserGroup findByName(String name) {
        return this.userGroupRepository.findByName(name);
    }

    @PostConstruct
    private void initiateGroups() {
        if (this.userGroupRepository.count() < 1) {
            List<UserGroup> userGroups = new ArrayList<>();
            UserGroup customers = new UserGroup("CUSTOMERS");
            userGroups.add(customers);
            UserGroup agents = new UserGroup("AGENTS");
            userGroups.add(agents);
            UserGroup managers = new UserGroup("MANAGERS");
            userGroups.add(managers);
            this.userGroupRepository.saveAll(userGroups);
        }
    }

    public Page<UserGroup> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.userGroupRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.userGroupRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<UserGroup> findAll(String userGroupName, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.userGroupRepository.findAllByNameContaining(userGroupName, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.userGroupRepository.findAllByNameContaining(userGroupName, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
