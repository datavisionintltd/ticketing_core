package com.dvi.ticket.service;

import com.dvi.ticket.model.TaskUser;
import com.dvi.ticket.repository.TaskUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TaskUserService {

    private TaskUserRepository taskUserRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TaskUserService(TaskUserRepository taskUserRepository) {
        this.taskUserRepository = taskUserRepository;
    }

    @Transactional
    public TaskUser addTaskUser(TaskUser taskUser) {
        return this.taskUserRepository.save(taskUser);
    }

    @Transactional
    public void update(TaskUser taskUser) {
        this.taskUserRepository.save(taskUser);
    }

    @Transactional
    public void remove(TaskUser taskUser) {
        this.taskUserRepository.delete(taskUser);
    }

    public TaskUser findById(long id) {
        return this.taskUserRepository.findById(id);
    }

    public Page<TaskUser> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.taskUserRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.taskUserRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
