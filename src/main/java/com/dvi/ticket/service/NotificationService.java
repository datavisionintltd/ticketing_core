package com.dvi.ticket.service;

import com.dvi.ticket.model.Notification;
import com.dvi.ticket.repository.NotificationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class NotificationService {

    private NotificationRepository notificationRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public NotificationService(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    @Transactional
    public Notification addNotification(Notification notification) {
        return this.notificationRepository.save(notification);
    }

    @Transactional
    public void update(Notification notification) {
        this.notificationRepository.save(notification);
    }

    @Transactional
    public void remove(Notification notification) {
        this.notificationRepository.delete(notification);
    }

    public Notification findById(long id) {
        return this.notificationRepository.findById(id);
    }

    public Page<Notification> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.notificationRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.notificationRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
