package com.dvi.ticket.service;

import com.dvi.ticket.model.settings.EmailTemplatesSettings;
import com.dvi.ticket.repository.EmailTemplatesSettingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class EmailTemplatesSettingsService {
    private EmailTemplatesSettingsRepository emailTemplatesSettingRepository;

    @Autowired
    public EmailTemplatesSettingsService(EmailTemplatesSettingsRepository emailTemplatesSettingRepository) {
        this.emailTemplatesSettingRepository = emailTemplatesSettingRepository;
    }
    @Transactional
    public EmailTemplatesSettings addEmailTemplatesSettings(EmailTemplatesSettings emailTemplatesSettings) {
        return this.emailTemplatesSettingRepository.save(emailTemplatesSettings);
    }

    @Transactional
    public void update(EmailTemplatesSettings emailTemplatesSettings) {
        this.emailTemplatesSettingRepository.save(emailTemplatesSettings);
    }

    @Transactional
    public void remove(EmailTemplatesSettings emailTemplatesSettings) {
        this.emailTemplatesSettingRepository.delete(emailTemplatesSettings);
    }

    public EmailTemplatesSettings findById(long id) {
        return this.emailTemplatesSettingRepository.findById(id);
    }

    public Page<EmailTemplatesSettings> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.emailTemplatesSettingRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.emailTemplatesSettingRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
    public List<EmailTemplatesSettings> findAll() {
        return this.emailTemplatesSettingRepository.findAll();
    }


}
