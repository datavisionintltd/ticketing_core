package com.dvi.ticket.service;

import com.dvi.ticket.model.GeneralSetting;
import com.dvi.ticket.repository.GeneralSettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class GeneralSettingService {

    private GeneralSettingRepository generalSettingRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public GeneralSettingService(GeneralSettingRepository generalSettingRepository) {
        this.generalSettingRepository = generalSettingRepository;
    }

    @Transactional
    public GeneralSetting addGeneralSetting(GeneralSetting generalSetting) {
        return this.generalSettingRepository.save(generalSetting);
    }

    @Transactional
    public void update(GeneralSetting generalSetting) {
        this.generalSettingRepository.save(generalSetting);
    }

    @Transactional
    public void remove(GeneralSetting generalSetting) {
        this.generalSettingRepository.delete(generalSetting);
    }

    public GeneralSetting findById(long id) {
        return this.generalSettingRepository.findById(id);
    }

    public Page<GeneralSetting> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.generalSettingRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.generalSettingRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<GeneralSetting> findAll(String name, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.generalSettingRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.generalSettingRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
