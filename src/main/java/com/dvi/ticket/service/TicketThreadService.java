package com.dvi.ticket.service;

import com.dvi.ticket.model.Ticket;
import com.dvi.ticket.model.TicketThread;
import com.dvi.ticket.repository.TicketThreadRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TicketThreadService {

    private TicketThreadRepository ticketThreadRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TicketThreadService(TicketThreadRepository ticketThreadRepository) {
        this.ticketThreadRepository = ticketThreadRepository;
    }

    @Transactional
    public TicketThread addTicketThread(TicketThread ticketThread) {
        return this.ticketThreadRepository.save(ticketThread);
    }

    @Transactional
    public void update(TicketThread ticketThread) {
        this.ticketThreadRepository.save(ticketThread);
    }

    @Transactional
    public void remove(TicketThread ticketThread) {
        this.ticketThreadRepository.delete(ticketThread);
    }

    public TicketThread findById(long id) {
        return this.ticketThreadRepository.findById(id);
    }

    public Page<TicketThread> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketThreadRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketThreadRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<TicketThread> findAll(Ticket ticket,int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketThreadRepository.findAllByTicket(ticket, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketThreadRepository.findAllByTicket(ticket, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
 
    
}
