package com.dvi.ticket.service;

import com.dvi.ticket.model.Country;
import com.dvi.ticket.repository.CountryRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CountryService {
    
    private CountryRepository countryRepository;
    
    public CountryService(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @Transactional
    public Country addCountry(Country country) {
        return this.countryRepository.save(country);
    }

    @Transactional
    public void update(Country country) {
        this.countryRepository.save(country);
    }

    @Transactional
    public void update(List<Country> countries) {
        this.countryRepository.saveAll(countries);
    }

    public void remove(Country country) {
        this.countryRepository.delete(country);
    }

    public Country findById(long id) {
        return this.countryRepository.findById(id);
    }

    public Country findByDialCode(String dialCode) {
        return this.countryRepository.findByDialCode(dialCode);
    }

    public Country findByCode(String code) {
        return this.countryRepository.findByCode(code);
    }

    public Page<Country> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.countryRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.countryRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Country> findAll(String countryName, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.countryRepository.findAllByNameContaining(countryName, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.countryRepository.findAllByNameContaining(countryName, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
