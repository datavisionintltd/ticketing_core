package com.dvi.ticket.service;

import com.dvi.ticket.model.Tenant;
import com.dvi.ticket.repository.TenantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class TenantService {

    private TenantRepository tenantRepository;

    @Autowired
    public TenantService(TenantRepository tenantRepository) {
        this.tenantRepository = tenantRepository;
    }

    @Transactional
    public Tenant addTenant(Tenant tenant) {
        return this.tenantRepository.save(tenant);
    }

    @Transactional
    public void update(Tenant tenant) {
        this.tenantRepository.save(tenant);
    }

    @Transactional
    public void remove(Tenant tenant) {
        this.tenantRepository.delete(tenant);
    }

    public Tenant findById(long id) {
        return this.tenantRepository.findById(id);
    }

    public Tenant findByTillNumber(String tillNumber) {
        return this.tenantRepository.findByTillNumber(tillNumber);
    }

    public Tenant findByLastAdded() {
        return this.tenantRepository.findByLastAdded();
    }

    public Tenant findFirstByNameContaining(String name) {
        return this.tenantRepository.findFirstByNameContaining(name);
    }

    @PostConstruct
    private void bootTenant() {
        try {
            if (this.tenantRepository.count() == 0) {
                Tenant softwareGalaxy = new Tenant();
                softwareGalaxy.setName("Software Galaxy");
                softwareGalaxy.setPhone("+255752191969");
                softwareGalaxy.setAddress("Mikocheni, Garden Road");
                softwareGalaxy.setWebsite("softwaregalaxy.co.tz");
                softwareGalaxy.setEmail("info@softwaregalaxy.co.tz");
                softwareGalaxy.setDescription("Software Galaxy Support Office");
                this.tenantRepository.save(softwareGalaxy);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Page<Tenant> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.tenantRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.tenantRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
    public List<Tenant> findAll() {
        return this.tenantRepository.findAll();
    }
}
