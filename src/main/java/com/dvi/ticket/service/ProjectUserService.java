package com.dvi.ticket.service;

import com.dvi.ticket.model.ProjectUser;
import com.dvi.ticket.repository.ProjectUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ProjectUserService {

    private ProjectUserRepository projectUserRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public ProjectUserService(ProjectUserRepository projectUserRepository) {
        this.projectUserRepository = projectUserRepository;
    }

    @Transactional
    public ProjectUser addProjectUser(ProjectUser projectUser) {
        return this.projectUserRepository.save(projectUser);
    }

    @Transactional
    public void update(ProjectUser projectUser) {
        this.projectUserRepository.save(projectUser);
    }

    @Transactional
    public void remove(ProjectUser projectUser) {
        this.projectUserRepository.delete(projectUser);
    }

    public ProjectUser findById(long id) {
        return this.projectUserRepository.findById(id);
    }

    public Page<ProjectUser> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.projectUserRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.projectUserRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
