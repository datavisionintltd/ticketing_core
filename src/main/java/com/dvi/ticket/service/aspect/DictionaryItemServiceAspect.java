package com.dvi.ticket.service.aspect;

import com.dvi.ticket.config.tenant.TenantContext;
import com.dvi.ticket.service.DictionaryItemService;
import com.dvi.ticket.util.DviUtilities;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class DictionaryItemServiceAspect {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Before("execution(* com.dvi.ticket.service.DictionaryItemService.*(..)) && target(dictionaryItemService)")
    public void aroundExecution(JoinPoint pjp, DictionaryItemService dictionaryItemService) throws Throwable {
        if (!DviUtilities.checkAuthority("ROLE_SUPER_ADMIN")) {
            org.hibernate.Filter filter = dictionaryItemService.entityManager.unwrap(Session.class).enableFilter("dictionaryItemFilter");
            filter.setParameter("tenantId", (int) TenantContext.getCurrentTenant().getId());
            filter.validate();
        }
    }
}
