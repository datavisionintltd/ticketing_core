package com.dvi.ticket.service.aspect;

import com.dvi.ticket.config.tenant.TenantContext;
import com.dvi.ticket.service.TenantSubscriptionService;
import com.dvi.ticket.util.DviUtilities;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TenantSubscriptionServiceAspect {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Before("execution(* com.dvi.ticket.service.TenantSubscriptionService.*(..)) && target(tenantSubscriptionService)")
    public void aroundExecution(JoinPoint pjp, TenantSubscriptionService tenantSubscriptionService) throws Throwable {
        if (!DviUtilities.checkAuthority("ROLE_SUPER_ADMIN")) {
            org.hibernate.Filter filter = tenantSubscriptionService.entityManager.unwrap(Session.class).enableFilter("tenantSubscriptionFilter");
            filter.setParameter("tenantId", (int) TenantContext.getCurrentTenant().getId());
            filter.validate();
        }
    }
}
