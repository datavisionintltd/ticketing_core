package com.dvi.ticket.service.aspect;

import com.dvi.ticket.config.tenant.TenantContext;
import com.dvi.ticket.service.TaskUserService;
import com.dvi.ticket.util.DviUtilities;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class TaskUserServiceAspect {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Before("execution(* com.dvi.ticket.service.TaskUserService.*(..)) && target(taskUserService)")
    public void aroundExecution(JoinPoint pjp, TaskUserService taskUserService) throws Throwable {
        if (!DviUtilities.checkAuthority("ROLE_SUPER_ADMIN")) {
            org.hibernate.Filter filter = taskUserService.entityManager.unwrap(Session.class).enableFilter("taskUserFilter");
            filter.setParameter("tenantId", (int) TenantContext.getCurrentTenant().getId());
            filter.validate();
        }
    }
}
