package com.dvi.ticket.service;

import com.dvi.ticket.model.Department;
import com.dvi.ticket.model.Topic;
import com.dvi.ticket.repository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
public class TopicService {

    private TopicRepository topicRepository;
    private Topic topic;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TopicService(TopicRepository topicRepository) {
        this.topicRepository = topicRepository;
    }

    @Transactional
    public Topic addTopic(Topic topic) {
        return this.topicRepository.save(topic);
    }

    @Transactional
    public List<Topic> addTopic(List<Topic> topics) {
        return this.topicRepository.saveAll(topics);
    }

    @Transactional
    public void update(Topic topic) {
        this.topicRepository.save(topic);
    }

    @Transactional
    public void remove(Topic topic) {
        this.topicRepository.delete(topic);
    }

    public Topic findById(long id) {
        return this.topicRepository.findById(id);
    }

    public Page<Topic> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.topicRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.topicRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Topic> findAll(String name, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.topicRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.topicRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Topic> findAll(Department department, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.topicRepository.findAllByDepartment(department, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.topicRepository.findAllByDepartment(department, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Topic> findAll(Department department, String name, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.topicRepository.findAllByDepartmentAndNameContaining(department, name, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.topicRepository.findAllByDepartmentAndNameContaining(department, name, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
    @PostConstruct
    private void setupDefaultUser() {
        if (this.topicRepository.count() == 0) {
            topic = new Topic();
            topic.setName("Default");
            this.topicRepository.save(topic);
        }
    }
}
