package com.dvi.ticket.service;

import com.dvi.ticket.model.Client;
import com.dvi.ticket.model.Tenant;
import com.dvi.ticket.repository.ClientRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@Transactional
public class ClientService {

    private ClientRepository clientRepository;
    @PersistenceContext
    public EntityManager entityManager;

    public ClientService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Transactional
    public Client addClient(Client client) {
        return this.clientRepository.save(client);
    }

    @Transactional
    public List<Client> addClient(List<Client> clients) {
        return this.clientRepository.saveAll(clients);
    }

    @Transactional
    public void update(Client client) {
        this.clientRepository.save(client);
    }

    @Transactional
    public void remove(Client client) {
        this.clientRepository.delete(client);
    }

    public Client findById(long id) {
        return this.clientRepository.findById(id);
    }

    public List<Long> findAll(Tenant tenant, String status, int limit) {
        return this.clientRepository.findAllByTenantIdAndStatus(tenant.getId(), status, limit);
    }

    public Page<Client> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.clientRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.clientRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Client> findAll(String name, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.clientRepository.findAllByNameContainingOrEmailContainingOrPhoneContaining(name, name, name, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.clientRepository.findAllByNameContainingOrEmailContainingOrPhoneContaining(name, name, name, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
