package com.dvi.ticket.service.config;

import com.dvi.ticket.model.config.SysConfig;
import com.dvi.ticket.repository.config.SysConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class SysConfigService {

    @Autowired
    private SysConfigRepository sysConfigRepository;

    @Transactional
    public SysConfig newSysConfig(SysConfig sysConfig) {
        return this.sysConfigRepository.save(sysConfig);
    }
    @Transactional
    public void addSysConfig(SysConfig sysConfig) {
        this.sysConfigRepository.save(sysConfig);
    }
    @Transactional
    public SysConfig updateSysConfig(SysConfig sysConfig) {
        return this.sysConfigRepository.save(sysConfig);
    }
    @Transactional
    public void update(SysConfig sysConfig) {
        this.sysConfigRepository.save(sysConfig);
    }
    @Transactional
    public void remove(SysConfig sysConfig) {
        this.sysConfigRepository.delete(sysConfig);
    }

    public SysConfig findById(long id) {
        return this.sysConfigRepository.findById(id);
    }

    public SysConfig findByConfigName(String configName) {
        return this.sysConfigRepository.findByConfigName(configName);
    }

    public List<SysConfig> findAll() {
        return this.sysConfigRepository.findAll();
    }
}
