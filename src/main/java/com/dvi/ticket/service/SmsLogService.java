package com.dvi.ticket.service;

import com.dvi.ticket.model.SmsLog;
import com.dvi.ticket.repository.SmsLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class SmsLogService {

    private SmsLogRepository smsLogRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public SmsLogService(SmsLogRepository smsLogRepository) {
        this.smsLogRepository = smsLogRepository;
    }

    @Transactional
    public SmsLog addSmsLog(SmsLog smsLog) {
        return this.smsLogRepository.save(smsLog);
    }

    @Transactional
    public void update(SmsLog smsLog) {
        this.smsLogRepository.save(smsLog);
    }

    @Transactional
    public void remove(SmsLog smsLog) {
        this.smsLogRepository.delete(smsLog);
    }

    public SmsLog findById(long id) {
        return this.smsLogRepository.findById(id);
    }

    public Page<SmsLog> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.smsLogRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.smsLogRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public List<SmsLog> findAllBySender(String sender) {
        return this.smsLogRepository.findAllBySender(sender);
    }

    public List<SmsLog> findAllByRecipient(String recipient) {
        return this.smsLogRepository.findAllByRecipient(recipient);
    }
}
