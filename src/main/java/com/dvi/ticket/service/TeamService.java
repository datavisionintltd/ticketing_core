package com.dvi.ticket.service;

import com.dvi.ticket.model.Department;
import com.dvi.ticket.model.Team;
import com.dvi.ticket.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TeamService {

    private TeamRepository teamRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TeamService(TeamRepository teamRepository) {
        this.teamRepository = teamRepository;
    }

    @Transactional
    public Team addTeam(Team team) {
        return this.teamRepository.save(team);
    }

    @Transactional
    public void update(Team team) {
        this.teamRepository.save(team);
    }

    @Transactional
    public void remove(Team team) {
        this.teamRepository.delete(team);
    }

    public Team findById(long id) {
        return this.teamRepository.findById(id);
    }

    public Page<Team> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.teamRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.teamRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Team> findAll(String name, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.teamRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.teamRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Team> findAll(Department department, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.teamRepository.findAllByDepartment(department, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.teamRepository.findAllByDepartment(department, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Team> findAll(Department department, String name, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.teamRepository.findAllByDepartmentAndNameContaining(department, name, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.teamRepository.findAllByDepartmentAndNameContaining(department, name, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
