package com.dvi.ticket.service;

import com.dvi.ticket.model.EmailToken;
import com.dvi.ticket.repository.EmailTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
public class EmailTokenService {

    @Autowired
    private EmailTokenRepository emailTokenRepository;

    @Transactional
    public EmailToken newEmailToken(EmailToken emailToken) {
        return this.emailTokenRepository.save(emailToken);
    }
    @Transactional
    public void addEmailToken(EmailToken emailToken) {
        this.emailTokenRepository.save(emailToken);
    }
    @Transactional
    public EmailToken updateEmailToken(EmailToken emailToken) {
        return this.emailTokenRepository.save(emailToken);
    }
    @Transactional
    public void update(EmailToken emailToken) {
        this.emailTokenRepository.save(emailToken);
    }
    @Transactional
    public void remove(EmailToken emailToken) {
        this.emailTokenRepository.delete(emailToken);
    }

    public EmailToken findById(long id) {
        return this.emailTokenRepository.findById(id);
    }

    public EmailToken findByEmail(String email) {
        return this.emailTokenRepository.findByEmail(email);
    }

    public EmailToken findByToken(String token) {
        return this.emailTokenRepository.findByToken(token);
    }

    public List<EmailToken> findAll() {
        return this.emailTokenRepository.findAll();
    }

    public List<EmailToken> findAllByAddedTimeAfter(Timestamp addedTime) {
        return this.emailTokenRepository.findAllByAddedTimeAfter(addedTime);
    }
}
