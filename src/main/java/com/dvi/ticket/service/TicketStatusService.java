package com.dvi.ticket.service;

import com.dvi.ticket.model.TicketStatus;
import com.dvi.ticket.repository.TicketStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class TicketStatusService {
    
    private TicketStatusRepository ticketStatusRepository;
    
    @Autowired
    public TicketStatusService(TicketStatusRepository ticketStatusRepository) {
        this.ticketStatusRepository = ticketStatusRepository;
    }

    @Transactional
    public TicketStatus addTicketStatus(TicketStatus ticketStatus) {
        return this.ticketStatusRepository.save(ticketStatus);
    }

    @Transactional
    public List<TicketStatus> addTicketStatus(List<TicketStatus> ticketStatuss) {
        return this.ticketStatusRepository.saveAll(ticketStatuss);
    }

    @Transactional
    public void update(TicketStatus ticketStatus) {
        this.ticketStatusRepository.save(ticketStatus);
    }

    @Transactional
    public void remove(TicketStatus ticketStatus) {
        this.ticketStatusRepository.delete(ticketStatus);
    }

    @PostConstruct
    private void initiateStatus() {
        try {
            if (this.ticketStatusRepository.count() < 1) {
                List<TicketStatus> statuses = new ArrayList<>();
                TicketStatus opened = new TicketStatus("opened");
                statuses.add(opened);
                TicketStatus pending = new TicketStatus("pending");
                statuses.add(pending);
                TicketStatus closed = new TicketStatus("closed");
                statuses.add(closed);
                /*TicketStatus customerResponded = new TicketStatus("customer responded");
                statuses.add(customerResponded);
                TicketStatus missed = new TicketStatus("missed");
                statuses.add(missed);
                TicketStatus onHold = new TicketStatus("on hold");
                statuses.add(onHold);
                TicketStatus open = new TicketStatus("open");
                statuses.add(open);
                TicketStatus overdue = new TicketStatus("overdue");
                statuses.add(overdue);
                TicketStatus responseOverdue = new TicketStatus("response overdue");
                statuses.add(responseOverdue);
                TicketStatus unassigned = new TicketStatus("unassigned");
                statuses.add(unassigned);
                TicketStatus spam = new TicketStatus("spam");
                statuses.add(spam);
                TicketStatus archived = new TicketStatus("archived");
                statuses.add(archived);*/
                this.ticketStatusRepository.saveAll(statuses);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public TicketStatus findById(long id) {
        return this.ticketStatusRepository.findById(id);
    }

    public TicketStatus findByDetails(String details) {
        return this.ticketStatusRepository.findByDetails(details);
    }

    public Page<TicketStatus> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketStatusRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketStatusRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<TicketStatus> findAll(String details, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketStatusRepository.findAllByDetailsContaining(details, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketStatusRepository.findAllByDetailsContaining(details, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
