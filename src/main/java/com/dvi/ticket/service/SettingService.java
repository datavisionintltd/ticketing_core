package com.dvi.ticket.service;

import com.dvi.ticket.model.Setting;
import com.dvi.ticket.repository.SettingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SettingService {

    private SettingRepository settingRepository;

    @Autowired
    public SettingService(SettingRepository settingRepository) {
        this.settingRepository = settingRepository;
    }

    @Transactional
    public Setting addSetting(Setting setting) {
        return this.settingRepository.save(setting);
    }

    @Transactional
    public void update(Setting setting) {
        this.settingRepository.save(setting);
    }

    @Transactional
    public void remove(Setting setting) {
        this.settingRepository.delete(setting);
    }

    public Setting findById(long id) {
        return this.settingRepository.findById(id);
    }

    public Page<Setting> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.settingRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.settingRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
