package com.dvi.ticket.service;

import com.dvi.ticket.model.TenantPackage;
import com.dvi.ticket.repository.TenantPackageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TenantPackageService {

    private TenantPackageRepository tenantPackageRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TenantPackageService(TenantPackageRepository tenantPackageRepository) {
        this.tenantPackageRepository = tenantPackageRepository;
    }

    @Transactional
    public TenantPackage addTenantPackage(TenantPackage tenantPackage) {
        return this.tenantPackageRepository.save(tenantPackage);
    }

    @Transactional
    public void update(TenantPackage tenantPackage) {
        this.tenantPackageRepository.save(tenantPackage);
    }

    public void remove(long clientPackageId) {
        TenantPackage tenantPackage = this.findById(clientPackageId);
        this.tenantPackageRepository.delete(tenantPackage);
    }

    public TenantPackage findById(long id) {
        return this.tenantPackageRepository.findById(id);
    }

    public Page<TenantPackage> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.tenantPackageRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.tenantPackageRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
