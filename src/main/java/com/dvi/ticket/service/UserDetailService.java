package com.dvi.ticket.service;

import com.dvi.ticket.model.UserDetail;
import com.dvi.ticket.repository.UserDetailRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserDetailService {

    private UserDetailRepository userDetailRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public UserDetailService(UserDetailRepository userDetailRepository) {
        this.userDetailRepository = userDetailRepository;
    }

    @Transactional
    public UserDetail addUserDetail(UserDetail userDetail) {
        return this.userDetailRepository.save(userDetail);
    }

    @Transactional
    public void update(UserDetail userDetail) {
        this.userDetailRepository.save(userDetail);
    }

    @Transactional
    public void remove(UserDetail userDetail) {
        this.userDetailRepository.delete(userDetail);
    }

    public UserDetail findById(long id) {
        return this.userDetailRepository.findById(id);
    }

    public Page<UserDetail> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.userDetailRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.userDetailRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
