package com.dvi.ticket.service;
import com.dvi.ticket.model.DictionaryItem;
import com.dvi.ticket.model.settings.SystemSettings;
import com.dvi.ticket.repository.DictionaryItemRepository;
import com.dvi.ticket.repository.SystemSettingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import javax.annotation.PostConstruct;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SystemSettingsService {
    private SystemSettingsRepository systemSettingsRepository;
    private SystemSettings systemSettings;
    private DictionaryItemRepository dictionaryItemRepository;

    @Autowired
    public  SystemSettingsService( DictionaryItemRepository dictionaryItemRepository,SystemSettingsRepository systemSettingsRepository){
        this.systemSettingsRepository=systemSettingsRepository;
        this.dictionaryItemRepository=dictionaryItemRepository;
    }
    @Transactional
    public SystemSettings save(SystemSettings systemSettingsItem){
        return this.systemSettingsRepository.save(systemSettingsItem);
    }

    public SystemSettings findById(long id) {
        return this.systemSettingsRepository.findById(id);
    }
    public SystemSettings findTopByOrderByIdDesc() {
        return this.systemSettingsRepository.findTopByOrderByIdDesc();
    }
    @Transactional
    public void update(SystemSettings systemSettings) {
        this.systemSettingsRepository.save(systemSettings);
    }

    @PostConstruct
    private void setupDefaultUser() {
        if (this.systemSettingsRepository.count() == 0) {
            SystemSettings systemSettings=new SystemSettings();
            DictionaryItem defaultLanguage = dictionaryItemRepository.findByCode("EN");
            DictionaryItem defaultTimeZone = dictionaryItemRepository.findByCode("AFDSM");
            DictionaryItem defaultPageSize = dictionaryItemRepository.findByCode("PGSZ");



            systemSettings.setDefaultPageSize(defaultPageSize);
            systemSettings.setLanguage(defaultLanguage);
            systemSettings.setDefaultTimeZone(defaultTimeZone);
            systemSettings.setFileSize("5");
            this.systemSettingsRepository.save(systemSettings);
        }
    }
}
