package com.dvi.ticket.service;

import com.dvi.ticket.model.PaymentLog;
import com.dvi.ticket.repository.PaymentLogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PaymentLogService {

    private PaymentLogRepository paymentLogRepository;

    @Autowired
    public PaymentLogService(PaymentLogRepository paymentLogRepository) {
        this.paymentLogRepository = paymentLogRepository;
    }

    @Transactional
    public PaymentLog addPaymentLog(PaymentLog paymentLog) {
        return this.paymentLogRepository.save(paymentLog);
    }

    @Transactional
    public void update(PaymentLog paymentLog) {
        this.paymentLogRepository.save(paymentLog);
    }

    @Transactional
    public void remove(PaymentLog paymentLog) {
        this.paymentLogRepository.delete(paymentLog);
    }

    public PaymentLog findById(long id) {
        return this.paymentLogRepository.findById(id);
    }

    public Page<PaymentLog> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.paymentLogRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.paymentLogRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
