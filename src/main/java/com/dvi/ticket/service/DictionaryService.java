package com.dvi.ticket.service;

import com.dvi.ticket.model.Dictionary;
import com.dvi.ticket.repository.DictionaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class DictionaryService {

    private DictionaryRepository dictionaryRepository;
    private Dictionary lockSemantics,language,regions,districts,dateFormats,timeZones,pageSize;
    @PersistenceContext
    public EntityManager entityManager;
    @Autowired
    public  DictionaryService(DictionaryRepository dictionaryRepository){
        this.dictionaryRepository=dictionaryRepository;
    }

    public Dictionary findById(long id) {
        return this.dictionaryRepository.findById(id);
    }

    public Dictionary findByCode(String code) {
        return this.dictionaryRepository.findByCode(code);
    }
    public Dictionary listItemsByCode(String code) {
        return this.dictionaryRepository.findByCode(code);
    }
    public Dictionary findByIdAndCode(long id, String code) {
        return this.dictionaryRepository.findByIdAndCode(id, code);
    }

    public Dictionary create(Dictionary dictionary){
        return  this.dictionaryRepository.save(dictionary);
    }

    public void delete(Dictionary dictionary){
         this.dictionaryRepository.delete(dictionary);
    }

    public Dictionary update(Dictionary dictionary){
        return  this.dictionaryRepository.save(dictionary);
    }

    public List<Dictionary> list(){
        return this.dictionaryRepository.findAll();
    }

    public Page<Dictionary> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.dictionaryRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.dictionaryRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    @PostConstruct
    private void setupDefaultDictionary() {

            if (this.dictionaryRepository.findByCode("LCKSM") == null) {
                lockSemantics = new Dictionary();
                lockSemantics.setCode("LCKSM");
                lockSemantics.setName("Lock Semantics");
                dictionaryRepository.save(lockSemantics);
            }

           if (this.dictionaryRepository.findByCode("LNG") == null) {
               language = new Dictionary();
               language.setCode("LNG");
               language.setName("Language");
            dictionaryRepository.save(language);
           }
          if(this.dictionaryRepository.findByCode("RGN") == null) {
              regions = new Dictionary();
              regions.setCode("RGN");
              regions.setName("Regions");
            dictionaryRepository.save(regions);
          }
        if(this.dictionaryRepository.findByCode("DSTR") == null) {
            districts = new Dictionary();
            districts.setCode("DSTR");
            districts.setName("Districts");
            dictionaryRepository.save(districts);
        }
        if(this.dictionaryRepository.findByCode("DATFMTS") == null) {
            dateFormats = new Dictionary();
            dateFormats.setCode("DATFMTS");
            dateFormats.setName("Date Formats");
            dictionaryRepository.save(dateFormats);
        }
        if(this.dictionaryRepository.findByCode("ZON") == null) {
            timeZones = new Dictionary();
            timeZones.setCode("ZON");
            timeZones.setName("Time zones ");
            dictionaryRepository.save(timeZones);
        }
        if(this.dictionaryRepository.findByCode("PGSZ") == null) {
            pageSize = new Dictionary();
            pageSize.setCode("PGSZ");
            pageSize.setName("Page size");
            dictionaryRepository.save(pageSize);
        }
    }
}
