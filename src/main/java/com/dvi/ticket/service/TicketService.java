package com.dvi.ticket.service;

import com.dvi.ticket.model.*;
import com.dvi.ticket.repository.TicketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TicketService {

    private TicketRepository ticketRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TicketService(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Transactional
    public Ticket addTicket(Ticket ticket) {
        return this.ticketRepository.save(ticket);
    }

    @Transactional
    public void update(Ticket ticket) {
        this.ticketRepository.save(ticket);
    }

    @Transactional
    public void remove(Ticket ticket) {
        this.ticketRepository.delete(ticket);
    }

    public long countByClientAndStatus(Client client, String status) {
        return this.ticketRepository.countByTenantIdAndClientIdAndStatus(client.getTenant().getId(), client.getId(), status);
    }

    public long countByProjectAndStatus(Project project, String status) {
        return this.ticketRepository.countByProjectAndStatus(project, status);
    }

    public long countByAgentAndStatus(User user, String status) {
        return this.ticketRepository.countByTenantIdAndAgentIdAndStatus(user.getTenant().getId(), user.getId(), status);
    }

    public Ticket findById(long id) {
        return this.ticketRepository.findById(id);
    }

    public Ticket findByLastAdded(Tenant tenant) {
        return this.ticketRepository.findByLastAdded(tenant.getId());
    }

    public Page<Ticket> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Ticket> findAll(User user, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketRepository.findAllByUser(user, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketRepository.findAllByUser(user, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Ticket> findAll(Topic topic, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketRepository.findAllByTopic(topic, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketRepository.findAllByTopic(topic, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Ticket> findAll(User user, Topic topic, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketRepository.findAllByUserAndTopic(user, topic, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketRepository.findAllByUserAndTopic(user, topic, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Ticket> findAll(Project project, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketRepository.findAllByProject(project, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketRepository.findAllByProject(project, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Ticket> findAll(User user, Project project, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketRepository.findAllByUserAndProject(user, project, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketRepository.findAllByUserAndProject(user, project, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Ticket> findAll(Topic topic, Project project, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketRepository.findAllByTopicAndProject(topic, project, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketRepository.findAllByTopicAndProject(topic, project, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Ticket> findAll(User user, Topic topic, Project project, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketRepository.findAllByUserAndTopicAndProject(user, topic, project, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketRepository.findAllByUserAndTopicAndProject(user, topic, project, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Ticket> findAll(Tenant tenant, String status, String createdAt, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.ticketRepository.findAllByStatusAndAndCreatedAt(tenant.getId(), status, createdAt, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.ticketRepository.findAllByStatusAndAndCreatedAt(tenant.getId(), status, createdAt, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
    public long countByStatusAndCreatedAt(Tenant tenant, String status, String createdAt) {
        return this.ticketRepository.countByStatusAndCreatedAt(tenant.getId(), status, createdAt);
    }
}
