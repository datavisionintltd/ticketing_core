package com.dvi.ticket.service;

import com.dvi.ticket.model.User;
import com.dvi.ticket.model.VerificationCode;
import com.dvi.ticket.repository.VerificationCodeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class VerificationCodeService {

    @Autowired
    private VerificationCodeRepository verificationCodeRepository;

    @Transactional
    public VerificationCode newVerificationCode(VerificationCode verificationCode) {
        return this.verificationCodeRepository.save(verificationCode);
    }
    @Transactional
    public void addVerificationCode(VerificationCode verificationCode) {
        this.verificationCodeRepository.save(verificationCode);
    }
    @Transactional
    public VerificationCode updateVerificationCode(VerificationCode verificationCode) {
        return this.verificationCodeRepository.save(verificationCode);
    }
    @Transactional
    public void update(VerificationCode verificationCode) {
        this.verificationCodeRepository.save(verificationCode);
    }

    @Transactional
    public void remove(VerificationCode verificationCode) {
        this.verificationCodeRepository.delete(verificationCode);
    }

    public VerificationCode findById(long id) {
        return this.verificationCodeRepository.findById(id);
    }

    public VerificationCode findByUser(User user) {
        return this.verificationCodeRepository.findByUser(user);
    }

    public VerificationCode findByCurrentCode(String currentCode) {
        return this.verificationCodeRepository.findByCurrentCode(currentCode);
    }

    public List<VerificationCode> findAllByUser(User user) {
        return this.verificationCodeRepository.findAllByUser(user);
    }
}
