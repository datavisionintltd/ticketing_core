package com.dvi.ticket.service;

import com.dvi.ticket.model.ServiceLevelAgreement;
import com.dvi.ticket.repository.ServiceLevelAgreementRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ServiceLevelAgreementService {

    private ServiceLevelAgreementRepository serviceLevelAgreementRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public ServiceLevelAgreementService(ServiceLevelAgreementRepository serviceLevelAgreementRepository) {
        this.serviceLevelAgreementRepository = serviceLevelAgreementRepository;
    }

    @Transactional
    public ServiceLevelAgreement addServiceLevelAgreement(ServiceLevelAgreement serviceLevelAgreement) {
        return this.serviceLevelAgreementRepository.save(serviceLevelAgreement);
    }

    @Transactional
    public List<ServiceLevelAgreement> addServiceLevelAgreement(List<ServiceLevelAgreement> serviceLevelAgreements) {
        return this.serviceLevelAgreementRepository.saveAll(serviceLevelAgreements);
    }

    @Transactional
    public void update(ServiceLevelAgreement serviceLevelAgreement) {
        this.serviceLevelAgreementRepository.save(serviceLevelAgreement);
    }

    @Transactional
    public void remove(ServiceLevelAgreement serviceLevelAgreement) {
        this.serviceLevelAgreementRepository.delete(serviceLevelAgreement);
    }

    public ServiceLevelAgreement findById(long id) {
        return this.serviceLevelAgreementRepository.findById(id);
    }

    public Page<ServiceLevelAgreement> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.serviceLevelAgreementRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.serviceLevelAgreementRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
