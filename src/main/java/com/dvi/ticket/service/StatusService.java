package com.dvi.ticket.service;

import com.dvi.ticket.model.Status;
import com.dvi.ticket.repository.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

@Service
@Transactional
public class StatusService {

    private StatusRepository statusRepository;
    private Status status;
    @PersistenceContext
    public EntityManager entityManager;
    @Autowired
    public StatusService(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Transactional
    public Status addStatus(Status status) {
        return this.statusRepository.save(status);
    }

    @Transactional
    public void update(Status status) {
        this.statusRepository.save(status);
    }

    @Transactional
    public void remove(Status status) {
        this.statusRepository.delete(status);
    }

    public Status findById(long id) {
        return this.statusRepository.findById(id);
    }

    public Status findByName(String name) {
        return this.statusRepository.findByName(name);
    }

    public Page<Status> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.statusRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.statusRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public List<Status> findAll() {
        return this.statusRepository.findAll();
    }

    @PostConstruct
    private void setupDefaultUser() {
        if (this.statusRepository.count() == 0) {
            status = new Status();
            status.setName("Open");
            this.statusRepository.save(status);
        }
    }
}
