package com.dvi.ticket.service;

import com.dvi.ticket.model.Department;
import com.dvi.ticket.repository.DepartmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class DepartmentService {

    private DepartmentRepository departmentRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public DepartmentService(DepartmentRepository departmentRepository) {
        this.departmentRepository = departmentRepository;
    }

    @Transactional
    public Department addDepartment(Department department) {
        return this.departmentRepository.save(department);
    }

    @Transactional
    public List<Department> addDepartment(List<Department> departments) {
        return this.departmentRepository.saveAll(departments);
    }

    @Transactional
    public void update(Department department) {
        this.departmentRepository.save(department);
    }

    @Transactional
    public void remove(Department department) {
        this.departmentRepository.delete(department);
    }

    public Department findById(long id) {
        return this.departmentRepository.findById(id);
    }

    public Page<Department> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.departmentRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.departmentRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<Department> findAll(String name, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.departmentRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.departmentRepository.findAllByNameContaining(name, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
