package com.dvi.ticket.service;

import com.dvi.ticket.model.Team;
import com.dvi.ticket.model.TeamUser;
import com.dvi.ticket.model.User;
import com.dvi.ticket.repository.TeamUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TeamUserService {

    private TeamUserRepository teamUserRepository;
    @PersistenceContext
    public EntityManager entityManager;

    @Autowired
    public TeamUserService(TeamUserRepository teamUserRepository) {
        this.teamUserRepository = teamUserRepository;
    }

    @Transactional
    public TeamUser addTeamUser(TeamUser teamUser) {
        return this.teamUserRepository.save(teamUser);
    }

    @Transactional
    public void update(TeamUser teamUser) {
        this.teamUserRepository.save(teamUser);
    }

    @Transactional
    public void remove(TeamUser teamUser) {
        this.teamUserRepository.delete(teamUser);
    }

    public TeamUser findById(long id) {
        return this.teamUserRepository.findById(id);
    }

    public TeamUser findByUserAndTeam(User user, Team team) {
        return this.teamUserRepository.findByUserAndTeam(user, team);
    }

    public Page<TeamUser> findAll(int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.teamUserRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.teamUserRepository.findAll(PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<TeamUser> findAll(User user, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.teamUserRepository.findAllByUser(user, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.teamUserRepository.findAllByUser(user, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }

    public Page<TeamUser> findAll(Team team, int page, int limit, String sort, String order) {
        if (order.equalsIgnoreCase("asc")) {
            return this.teamUserRepository.findAllByTeam(team, PageRequest.of(page, limit, Sort.by(sort).ascending()));
        } else {
            return this.teamUserRepository.findAllByTeam(team, PageRequest.of(page, limit, Sort.by(sort).descending()));
        }
    }
}
