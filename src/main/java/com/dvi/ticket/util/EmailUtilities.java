package com.dvi.ticket.util;

import com.dvi.ticket.model.EmailToken;
import com.dvi.ticket.service.EmailTokenService;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Calendar;

@Component
public class EmailUtilities {

    private JavaMailSender javaMailSender;
    private EmailTokenService emailTokenService;

    public EmailUtilities(JavaMailSender javaMailSender, EmailTokenService emailTokenService) {
        this.javaMailSender = javaMailSender;
        this.emailTokenService = emailTokenService;
    }

    public void registrationEmail(String email, String password) {
        try {
            String emailSubject = "Max Desk account registration";
            String emailTemplate = "<html>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div style=\"width: 70%; margin-left: 15%;\">\n" +
                    "<div style=\"background-color: #dcdcdc; padding: 2%;\">\n" +
                    "<div style=\"text-align: center; background-color: #f9f9f9; padding: 2%;\">\n" +
                    "<h2 style=\"color: #000000; font-weight: bold;\"><strong>Welcome?</strong></h2><br/>\n" +
                    "<h3 style=\"color: #000000;\">We are excited to have you get started. First, you need to confirm your account.<br>Just press the button below</h3><br/>\n" +
                    "<h3>\n" +
                    "<a href=\"http://max Desk.co.tz\" style=\"text-decoration: none; color: #f9f9f9;\" target=\"_blank\">\n" +
                    "<fieldset style=\"background-color: #e00000; border-radius: 9px; min-width: 9em;\"><strong><small>Visit Max Desk</small></strong></fieldset></a></h3><br/>\n" +
                    "<h3 style=\"color: #000000;\">If that doesn't work, copy and paste the following link in your browser:</h3>\n" +
                    "<h3 style=\"color: #428bca; font-size: 11px;\"><a href='#'><strong> Max Desk.co.tz </strong></a></h3>\n" +
                    "<h3 style=\"color: #000000;\">Your initial password is: " + password + ", you can change it after login into the system</h3>\n" +
                    "<h3 style=\"color: #000000;\">If you have questions, just reply to this email - we are always happy to help out.<br/><br/>Cheers,<br/>Max Desk Team</h3>\n" +
                    "</div>\n" +
                    "<div style=\"background-color: #e00000; padding: 2%;\">\n" +
                    "<br/>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\"><strong>Is this weird?</strong></h3>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\">You received this email because you just signed up for a new account on Max Desk.<br/>\n" +
                    "<!--br/>If these emails get annoying, please feel free to\n" +
                    "<a href=\"#\" style=\"color: #d9534f\">unsubscribe</a-->\n" +
                    "<br/><br/>Max Desk | powered by DataVision International\n" +
                    "</h3>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</body>\n" +
                    "</html>";
            this.sendEmailTemplate(email, emailSubject, emailTemplate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resetPasswordEmail(String email, String fullName, String verCode) {
        try {
            String emailSubject = "Reset Password on Max Desk";
            String emailTemplate = "<html>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div style=\"width: 70%; margin-left: 15%;\">\n" +
                    "<div style=\"background-color: #dcdcdc; padding: 2%;\">\n" +
                    "<div style=\"text-align: center; background-color: #f9f9f9; padding: 2%;\">\n" +
                    "<h2 style=\"color: #000000; font-weight: bold;\"><strong>Welcome?</strong></h2><br/>\n" +
                    "<h3 style=\"color: #000000;\">Hello " + fullName + ". We are excited to have you back</h3><br/>\n" +
                    "<h3 style=\"color: #000000;\">To reset your password, use this verification code B-" + verCode + " together with your new password</h3>\n" +
                    "<h3 style=\"color: #000000;\">If you have questions, just reply to this email - we are always happy to help out.<br/><br/>Cheers,<br/>Max Desk Team</h3>\n" +
                    "</div>\n" +
                    "<div style=\"background-color: #e00000; padding: 2%;\">\n" +
                    "<br/>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\"><strong>Is this weird?</strong></h3>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\">You received this email because you requested to change password on your Max Desk account.<br/>\n" +
                    "<!--br/>If these emails get annoying, please feel free to\n" +
                    "<a href=\"#\" style=\"color: #d9534f\">unsubscribe</a-->\n" +
                    "<br/><br/>Max Desk | powered by DataVision International\n" +
                    "</h3>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</body>\n" +
                    "</html>";
            this.sendEmailTemplate(email, emailSubject, emailTemplate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void verificationEmail(String email, String password) {
        try {
            String emailSubject = "Max Desk account registration";
            String emailTemplate = "<html>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div style=\"width: 70%; margin-left: 15%;\">\n" +
                    "<div style=\"background-color: #dcdcdc; padding: 2%;\">\n" +
                    "<div style=\"text-align: center; background-color: #f9f9f9; padding: 2%;\">\n" +
                    "<h2 style=\"color: #000000; font-weight: bold;\"><strong>Welcome?</strong></h2><br/>\n" +
                    "<h3 style=\"color: #000000;\">We are excited to have you get started. First, you need to confirm your account using verification code.</h3><br/>\n" +
                    "<h3 style=\"color: #000000;\">Use this verification code: " + password + ", on your app or website</h3>\n" +
                    "<h3 style=\"color: #000000;\">If you have questions, just reply to this email - we are always happy to help out.<br/><br/>Cheers,<br/>Max Desk Team</h3>\n" +
                    "</div>\n" +
                    "<div style=\"background-color: #e00000; padding: 2%;\">\n" +
                    "<br/>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\"><strong>Is this weird?</strong></h3>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\">You received this email because you just signed up for a new account on Max Desk.<br/>\n" +
                    "<!--br/>If these emails get annoying, please feel free to\n" +
                    "<a href=\"#\" style=\"color: #d9534f\">unsubscribe</a-->\n" +
                    "<br/><br/>Max Desk | powered by DataVision International\n" +
                    "</h3>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</body>\n" +
                    "</html>";
            this.sendEmailTemplate(email, emailSubject, emailTemplate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void resendActivationCode(String email, String password) {
        try {
            String emailSubject = "Activate Max Desk account";
            String emailTemplate = "<html>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div style=\"width: 70%; margin-left: 15%;\">\n" +
                    "<div style=\"background-color: #dcdcdc; padding: 2%;\">\n" +
                    "<div style=\"text-align: center; background-color: #f9f9f9; padding: 2%;\">\n" +
                    "<h2 style=\"color: #000000; font-weight: bold;\"><strong>Welcome?</strong></h2><br/>\n" +
                    "<h3 style=\"color: #000000;\">We are excited to have you back. Activate your account using verification code.</h3><br/>\n" +
                    "<h3 style=\"color: #000000;\">Use this verification code: " + password + ", on your app or website</h3>\n" +
                    "<h3 style=\"color: #000000;\">If you have questions, just reply to this email - we are always happy to help out.<br/><br/>Cheers,<br/>Max Desk Team</h3>\n" +
                    "</div>\n" +
                    "<div style=\"background-color: #e00000; padding: 2%;\">\n" +
                    "<br/>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\"><strong>Is this weird?</strong></h3>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\">You received this email because you requested to activate your Max Desk account.<br/>\n" +
                    "<!--br/>If these emails get annoying, please feel free to\n" +
                    "<a href=\"#\" style=\"color: #d9534f\">unsubscribe</a-->\n" +
                    "<br/><br/>Max Desk | powered by DataVision International\n" +
                    "</h3>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</body>\n" +
                    "</html>";
            this.sendEmailTemplate(email, emailSubject, emailTemplate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void paaswordResetEmail(String email) {
        try {
            Calendar calendarTime = Calendar.getInstance();
            EmailToken emailToken = new EmailToken();
            if (this.emailTokenService.findByEmail(email) != null) {
                emailToken = this.emailTokenService.findByEmail(email);
            } else {
                emailToken.setEmail(email);
            }
            emailToken.setAddedTime(new Timestamp(calendarTime.getTime().getTime()));
            long token = System.currentTimeMillis();
            String encodedToken = String.format("%d", token);
            emailToken.setToken(encodedToken);
            this.emailTokenService.addEmailToken(emailToken);
            String emailTemplate = "<html>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div style=\"width: 70%; margin-left: 15%;\">\n" +
                    "<div style=\"background-color: #dcdcdc; padding: 2%;\">\n" +
                    "<div style=\"text-align: center; background-color: #f9f9f9; padding: 2%;\">\n" +
                    "<h2 style=\"color: #000000; font-weight: bold;\"><strong>Welcome Back?</strong></h2><br/>\n" +
                    "<h3 style=\"color: #000000;\">We are excited to have you back. Just press the button below to reset your password</h3><br/>\n" +
                    "<h3>\n" +
                    "<a href=\"https://maxdesk/reset-password/" + encodedToken + "\" style=\"text-decoration: none; color: #f9f9f9;\">\n" +
                    "<fieldset style=\"background-color: #e00000; border-radius: 9px; min-width: 9em;\"><strong><small>Reset Password</small></strong></fieldset></a></h3><br/>\n" +
                    "<h3 style=\"color: #000000;\">If that doesn't work, copy and paste the following link in your browser:</h3>\n" +
                    "<h3 style=\"color: #428bca; font-size: 11px;\"><a href='#'><strong>https://maxdesk/reset-password/" + encodedToken + "</strong></a></h3>\n" +
                    "<h3 style=\"color: #000000;\">If you have questions, just reply to this email - we are always happy to help out.<br/><br/>Cheers,<br/>Max Desk Team</h3>\n" +
                    "</div>\n" +
                    "<div style=\"background-color: #e00000; padding: 2%;\">\n" +
                    "<br/>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\"><strong>Is this weird?</strong></h3>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\">\n" +
                    "You received this email because you have just requested to reset password of your account on Max Desk system.<br/>\n" +
                    "<br/><br/>Max Desk | powered by DataVision International\n" +
                    "</h3>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</body>\n" +
                    "</html>";
            this.sendEmailTemplate(email, "Max Desk Password Reset", emailTemplate);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendEmailTemplate(String toEmail, String subject, String htmlMessage) throws MessagingException, IOException {

        try {
            MimeMessage msg = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(msg, true);

            helper.setFrom("chloruko@gmail.com");
            helper.setTo(toEmail);

            helper.setSubject(subject);
            helper.setText(htmlMessage, true);
            javaMailSender.send(msg);
        } catch (Exception e) {
            boolean res = false;
        }
    }

    public void sendEmailWithAttachment(String toEmail, String subject, String htmlMessage, String pathToAttachment, String sendFileName) throws MessagingException, IOException {

        try {
            MimeMessage msg = javaMailSender.createMimeMessage();

            MimeMessageHelper helper = new MimeMessageHelper(msg, true);

            helper.setFrom("chloruko@gmail.com");
            helper.setTo(toEmail);
            helper.setSubject(subject);
            helper.setText(htmlMessage, true);

            FileSystemResource file = new FileSystemResource(new File(pathToAttachment));

            helper.addAttachment(sendFileName, file);

            javaMailSender.send(msg);
        } catch (Exception e) {
            boolean res = false;
        }
    }

    public void sendEmail(String toEmail, String subject, String message) {

        try {
            MimeMessage msg = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(msg, true, "UTF-8");

            helper.setFrom("chloruko@gmail.com");
            helper.setTo(toEmail);

            helper.setSubject(subject);
            helper.setText(message);

            javaMailSender.send(msg);
        } catch (Exception e) {
            boolean res = false;
        }
    }

    /*
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587);

        mailSender.setUsername("chloruko@gmail.com");
        mailSender.setPassword("Babakii1");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }*/
}
