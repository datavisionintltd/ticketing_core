package com.dvi.ticket.util;

import com.dvi.ticket.model.User;
import com.dvi.ticket.service.UserService;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class DbUtilities {

    private UserService userService;

    public DbUtilities(UserService userService) {
        this.userService = userService;
    }

    public User getUserInfo() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return this.userService.findByUsername(authentication.getName());
    }
}
