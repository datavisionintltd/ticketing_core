package com.dvi.ticket.util;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.TimeZone;
import java.util.function.Predicate;

@Component
public class DviUtilities {

    public ResponseEntity<String> getResponseEntity(HttpStatus httpStatus) {
        JSONObject response = new JSONObject();
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    public HttpHeaders reportOutput(InputStreamResource inputStreamResource, String fileType) {
        HttpHeaders responseHeaders = new HttpHeaders();
        try {
            responseHeaders.setContentLength(inputStreamResource.contentLength());
            if (fileType.equalsIgnoreCase("html")) {
                responseHeaders.setContentType(MediaType.valueOf("text/html"));
            } else if (fileType.equalsIgnoreCase("xlsx")) {
                responseHeaders.setContentType(MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
                responseHeaders.put("Content-Disposition", Collections.singletonList("attachment; filename=PartnerTransactions.xlsx"));
            } else {
                responseHeaders.setContentType(MediaType.valueOf("application/pdf"));
                responseHeaders.put("Content-Disposition", Collections.singletonList("filename=PartnerTransactions.pdf"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseHeaders;
    }

    public HttpHeaders reportOutputError(String fileType) {
        HttpHeaders responseHeaders = new HttpHeaders();
        try {
            String invalidFile = "Invalid File";
            InputStreamResource inputStreamResource = new InputStreamResource(new ByteArrayInputStream(invalidFile.getBytes()));
            responseHeaders.setContentLength(inputStreamResource.contentLength());
            if (fileType.equalsIgnoreCase("html")) {
                responseHeaders.setContentType(MediaType.valueOf("text/html"));
            } else if (fileType.equalsIgnoreCase("xlsx")) {
                responseHeaders.setContentType(MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"));
                responseHeaders.put("Content-Disposition", Collections.singletonList("attachment; filename=invalid.xlsx"));
            } else {
                responseHeaders.setContentType(MediaType.valueOf("application/pdf"));
                responseHeaders.put("Content-Disposition", Collections.singletonList("inline; filename=invalid.pdf"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return responseHeaders;
    }

    public String sortableString(String search) {
        try {
            search = search.substring(search.lastIndexOf(".") + 1).replaceAll("([A-Z])", "_$1").toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return search;
    }

    public boolean isLong(String longValue) {
        boolean res = false;
        try {
            if (Long.parseLong(longValue) >= 0) {
                res = true;
            }
        } catch (Exception e) {
            res = false;
        }
        return res;
    }

    public boolean isDouble(String doubleValue) {
        boolean res = false;
        try {
            if (Double.parseDouble(doubleValue) >= 0) {
                res = true;
            }
        } catch (Exception e) {
            res = false;
        }
        return res;
    }

    public String getRandomNumber() {
        String res = "9999";
        try {
            double randomNum = Math.random() * 999999 + 1;
            String strRandom = Double.toString(randomNum);
            int intRandom = Integer.parseInt(strRandom.substring(0, strRandom.indexOf(".")));
            res = String.format("%06d", intRandom);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public String toCapitalize(String sentence) {
        String capitalized = "";

        try{
            //Capitalize first character
            capitalized = sentence.substring(0,1).toUpperCase() + sentence.substring(1);
        }catch (Exception e){
            e.printStackTrace();
        }

        return capitalized;
    }

    public String replaceChars(String word, String oldWord, String newWord) {
        String replaced = "";

        try{
            replaced = word.replace(oldWord, newWord);
        }catch (Exception e){
            /**e.printStackTrace();*/
        }

        return replaced;
    }

    public String dateFormat(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date_string = "";
        try{
            Date simple_date = sdf.parse(dateString);
            date_string = df.format(simple_date);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return date_string;
    }

    public String dateDefaultFormat(String dateString) {
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String date_string = "";
        try{
            Date simple_date = df.parse(dateString);
            date_string = sdf.format(simple_date);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return date_string;
    }

    public String dateTimeFormat(String dateString) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String date_string = "";
        try{
            Date simple_date = sdf.parse(dateString);
            date_string = df.format(simple_date);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return date_string;
    }

    public String textColors(String choices){
        String text_color = "";

        try {
            Float choice = Float.parseFloat(choices);
            if (choice <= 20) {
                text_color = "danger";
            } else if (choice <= 50) {
                text_color = "warning";
            } else if (choice <= 80) {
                text_color = "info";
            } else {
                text_color = "success";
            }
        }catch (Exception e) {
            /**e.printStackTrace();*/
        }

        return text_color;
    }

    public boolean deleteFile(String file_name, String file_type, String file_folder){
        boolean resp = false;
        try{
            File the_file = new File( file_folder + file_name + "." + file_type);
            if (!the_file.exists()){
                resp = true;
            }else {
                if (the_file.delete()) {
                    resp = true;
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return resp;
    }

    public String amountFormat(double amt) {
        String amount = "0";
        try {
            //double amt = Double.parseDouble(amount);
            DecimalFormat formatter = new DecimalFormat("#,###.00");
            amount = formatter.format(amt);
            if (amt == 0) {
                amount = "0.00";
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return amount;
    }

    public String amountFormatInt(String amount) {
        try {
            double amt = Double.parseDouble(amount);
            DecimalFormat formatter = new DecimalFormat("#,###");
            amount = formatter.format(amt);
            if (amt == 0) {
                amount = "0";
            }
        }catch (Exception e) {
            e.printStackTrace();
        }
        return amount;
    }

    public String latestTime(int days, Timestamp start_date) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();

        try{
            c.setTime(sdf.parse(start_date.toString()));
        }catch(ParseException e){
            e.printStackTrace();
        }

        c.add(Calendar.DAY_OF_MONTH, days);
        String newDate = sdf.format(c.getTime());

        return newDate;
    }

    public String timeDifference(String GivenTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.setTimeZone(TimeZone.getTimeZone("Africa/Dar_es_Salaam"));
        String Differences = "";
        try {
            Date GivenTimes = sdf.parse(GivenTime);
            long Today = System.currentTimeMillis();
            long PostedDate = GivenTimes.getTime();
            long Difference = (Today - PostedDate);
            //System.out.println("Received Time is " + GivenTime + " Converted Time is " + GivenTimes);
            /*if (Difference >= 2419200000) {
                long Interval = Difference / (1000 * 60 * 60 * 24 * 28);
                if (Interval > 1) {
                    Differences = Interval + " Months Ago";
                } else {
                    Differences = Interval + " Month Ago";
                }
            } else */if (Difference >= 604800000) {
                long Interval = Difference / (1000 * 60 * 60 * 24 * 7);
                if (Interval > 1) {
                    Differences = Interval + " Weeks Ago";
                } else {
                    Differences = Interval + " Week Ago";
                }
            } else if (Difference >= 86400000) {
                long Interval = Difference / (1000 * 60 * 60 * 24);
                if (Interval > 1) {
                    Differences = Interval + " Days Ago";
                } else {
                    Differences = Interval + " Day Ago";
                }
            } else if (Difference >= 3600000) {
                long Interval = Difference / (1000 * 60 * 60);
                if (Interval > 1) {
                    Differences = Interval + " Hours Ago";
                } else {
                    Differences = Interval + " Hour Ago";

                }
            } else {
                long Interval = Difference / (1000 * 60);
                if (Interval > 1) {
                    Differences = Interval + " Minutes Ago";
                } else {
                    Differences = Interval + " Minute Ago";

                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return Differences;
    }

    public String trimLeadingZeros(String source) {
        String converted = "0";
        try {
            converted = source.replaceFirst ("^0*", "");
        }catch (Exception e) {
            e.printStackTrace();
        }
        return converted;
    }

    public String getGender(String sex) {
        try {
            if (sex.equalsIgnoreCase("male") || sex.equalsIgnoreCase("m") || sex.equalsIgnoreCase("mme") || sex.equalsIgnoreCase("me") || sex.equalsIgnoreCase("mwanaume") || sex.equalsIgnoreCase("man") || sex.equalsIgnoreCase("boy")) {
                sex = "Mme";
            } else {
                sex = "Mke";
            }
        } catch (Exception e) {
            e.printStackTrace();
            sex = "Mme";
        }
        return sex;
    }

    public boolean isJSONValid(String test) {
        try {
            new JSONObject(test);
        } catch (JSONException ex) {
            return false;
        }
        return true;
    }

    public Date parseDate(String dateStr) {
        Date parsedDate = new Date();
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            if (isParsable1(dateStr)) {
                parsedDate = sdf1.parse(dateStr);
            } else if (isParsable2(dateStr)) {
                parsedDate = sdf2.parse(dateStr);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return parsedDate;
    }

    private boolean isParsable1(String dateStr) {
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        sdf1.setLenient(false);
        try {
            sdf1.parse(dateStr);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
    private boolean isParsable2(String dateStr) {
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf2.setLenient(false);
        try {
            sdf2.parse(dateStr);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public String formatPhoneNumber(String dialCode, String phoneNumber) {
        String formattedNumber = phoneNumber;
        try {
            String validNumber = phoneNumber.replaceAll("\\D", "");
            String validDialCode = dialCode.replaceAll("\\D", "");
            int phoneLength = validNumber.length();
            if (phoneLength <= 9) {
                formattedNumber = validDialCode + validNumber;
            } else {
                formattedNumber = validDialCode + validNumber.substring(phoneLength - 9, phoneLength);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return formattedNumber;
    }

    public static boolean checkAuthority(String authority) {
        boolean res = false;
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            Predicate<GrantedAuthority> bySuperAdmin = role -> role.getAuthority().equalsIgnoreCase(authority);
            if (authentication != null) {
                res = authentication.getAuthorities().stream().anyMatch(bySuperAdmin);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    public static boolean checkUri(String uri, ServletRequest servletRequest){
        boolean res = false;
        try {
            res = ((HttpServletRequest) servletRequest).getRequestURI().contains(uri);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
