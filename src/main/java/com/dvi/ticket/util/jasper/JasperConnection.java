package com.dvi.ticket.util.jasper;

import java.sql.Connection;
import java.sql.DriverManager;

public class JasperConnection {
    public Connection JasperConnections(){
        Connection conn = null;
        try
        {
            Class.forName("com.mysql.cj.jdbc.Driver");
            String str = "jdbc:mysql://127.0.0.1:3306/dvisys?allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=utf8&verifyServerCertificate=false&useSSL=false&requireSSL=false";
            conn = DriverManager.getConnection(str, "deposit", "Vision@123");
        }
        catch (Exception localException)
        {
            localException.printStackTrace();
        }
        return conn;
    }
}
