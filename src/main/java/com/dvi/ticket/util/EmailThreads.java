package com.dvi.ticket.util;

public class EmailThreads implements Runnable {

    private EmailModule emailModule;
    private Thread thread;
    private String threadName;
    private String email;
    private String verCode;
    private String fullName;

    public EmailThreads(String threadName, EmailModule emailModule, String email, String verCode, String fullName) {
        this.threadName = threadName;
        this.emailModule = emailModule;
        this.email = email;
        this.verCode = verCode;
        this.fullName = fullName;
    }

    public void run() {
        try {
            this.emailModule.verificationEmail(this.email, this.verCode, this.fullName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start () {
        if (this.thread == null) {
            this.thread = new Thread(this, this.threadName);
            this.thread.start();
        }
    }
}
