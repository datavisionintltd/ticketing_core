package com.dvi.ticket.util;

import com.dvi.ticket.model.config.SysConfig;
import com.dvi.ticket.service.config.SysConfigService;
import com.ecwid.mailchimp.MailChimpClient;
import com.ecwid.mailchimp.MailChimpObject;
import com.ecwid.mailchimp.method.v1_3.campaign.CampaignCreateMethod;
import com.ecwid.mailchimp.method.v1_3.campaign.CampaignSendNowMethod;
import com.ecwid.mailchimp.method.v1_3.campaign.CampaignType;
import com.ecwid.mailchimp.method.v1_3.list.ListInformation;
import com.ecwid.mailchimp.method.v1_3.list.ListsMethod;
import com.ecwid.mailchimp.method.v1_3.list.ListsResult;
import com.google.gson.Gson;
import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessageStatus;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmailModule {

    private SysConfigService sysConfigService;

    public EmailModule(SysConfigService sysConfigService) {
        this.sysConfigService = sysConfigService;
    }

    private String getEmailResponse(MandrillMessageStatus[] messageStatusReports) {
        JSONObject message_response = new JSONObject();
        String receiver_email = "Max Desk Email";
        String email_id = "Id";
        String email_status = "Status";
        String rejection_reason = "Rejection reason";
        try {
            for (MandrillMessageStatus status : messageStatusReports) {
                //message_response.put(receiver_email, status.getEmail());
                //message_response.put(email_id, status.getId());
                message_response.put(email_status, status.getStatus());
                //message_response.put(rejection_reason, status.getRejectReason());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return message_response.toString();
    }

    public String resetPasswordEmail(String emailAddress, String fullName, String verCode) {
        MandrillMessageStatus[] messageStatusReports = null;
        try {
            SysConfig sysConfig = this.sysConfigService.findByConfigName("mandrill_key");
            MandrillApi mandrillApi = new MandrillApi(sysConfig.getConfigValue());

            String emailSubject = "Reset Password on Max Desk";
            String emailTemplate = "<html>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div style=\"width: 70%; margin-left: 15%;\">\n" +
                    "<div style=\"background-color: #dcdcdc; padding: 2%;\">\n" +
                    "<div style=\"text-align: center; background-color: #f9f9f9; padding: 2%;\">\n" +
                    "<h2 style=\"color: #000000; font-weight: bold;\"><strong>Welcome?</strong></h2><br/>\n" +
                    "<h3 style=\"color: #000000;\">Hello " + fullName + ". We are excited to have you back</h3><br/>\n" +
                    "<h3 style=\"color: #000000;\">To reset your password, use this verification code: M-" + verCode + " together with your new password</h3>\n" +
                    "<h3 style=\"color: #000000;\">If you have questions, just ask to our support team - we are always happy to help out.<br/><br/>Cheers,<br/>Max Desk Team</h3>\n" +
                    "</div>\n" +
                    "<div style=\"background-color: #8882BD; padding: 2%;\">\n" +
                    "<br/>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\"><strong>Is this weird?</strong></h3>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\">You received this email because you requested to change password on your Max Desk account.<br/>\n" +
                    "<!--br/>If these emails get annoying, please feel free to\n" +
                    "<a href=\"#\" style=\"color: #d9534f\">unsubscribe</a-->\n" +
                    "<br/><br/>Max Desk | powered by DataVision International\n" +
                    "</h3>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</body>\n" +
                    "</html>";
            MandrillMessage message = new MandrillMessage();
            message.setSubject(emailSubject);
            message.setHtml(emailTemplate);
            message.setAutoText(true);
            message.setFromEmail("no-reply@datavision.co.tz");
            message.setFromName("Max Desk");

            List<MandrillMessage.Recipient> recipients = new ArrayList<>();
            final MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
            recipient.setEmail(emailAddress);
            recipient.setName(emailAddress);
            recipients.add(recipient);
            if (message.getTo() == null) {
                message.setTo(recipients);
            }
            message.setPreserveRecipients(true);
            ArrayList<String> tags = new ArrayList<String>();
            tags.add("Max Desk Password Reset Request");
            tags.add("Max Desk");
            message.setTags(tags);

            messageStatusReports = mandrillApi.messages().send(message, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.getEmailResponse(messageStatusReports);
    }

    public String verificationEmail(String emailAddress, String verCode, String name) {
        MandrillMessageStatus[] messageStatusReports = null;
        try {
            SysConfig sysConfig = this.sysConfigService.findByConfigName("mandrill_key");
            /*String mandrill_key = "8q0ZKvYWVGNrULGlNZCWGw";*/
            MandrillApi mandrillApi = new MandrillApi(sysConfig.getConfigValue());

            String emailSubject = "Confirm Max Desk account registration";
            String emailTemplate = "<html>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div style=\"width: 70%; margin-left: 15%;\">\n" +
                    "<div style=\"background-color: #dcdcdc; padding: 2%;\">\n" +
                    "<div style=\"text-align: center; background-color: #f9f9f9; padding: 2%;\">\n" +
                    "<h2 style=\"color: #000000; font-weight: bold;\"><strong>Welcome " + name + "</strong></h2><br/>\n" +
                    "<h3 style=\"color: #000000;\">We are excited to have you get started. First, you need to confirm your account using verification code.</h3><br/>\n" +
                    "<h3 style=\"color: #000000;\">Use this verification code: M-" + verCode + ", on your app or website</h3>\n" +
                    "<h3 style=\"color: #000000;\">If you have questions, just ask to our support team - we are always happy to help out.<br/><br/>Cheers,<br/>Max Desk Team</h3>\n" +
                    "</div>\n" +
                    "<div style=\"background-color: #8882BD; padding: 2%;\">\n" +
                    "<br/>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\"><strong>Is this weird?</strong></h3>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\">You received this email because you just signed up for a new account on Max Desk.<br/>\n" +
                    "<!--br/>If these emails get annoying, please feel free to\n" +
                    "<a href=\"#\" style=\"color: #d9534f\">unsubscribe</a-->\n" +
                    "<br/><br/>Max Desk | powered by DataVision International\n" +
                    "</h3>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</body>\n" +
                    "</html>";

            MandrillMessage message = new MandrillMessage();
            message.setSubject(emailSubject);
            message.setHtml(emailTemplate);
            message.setAutoText(true);
            message.setFromEmail("no-reply@datavision.co.tz");
            message.setFromName("Max Desk");

            List<MandrillMessage.Recipient> recipients = new ArrayList<>();
            final MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
            recipient.setEmail(emailAddress);
            recipient.setName(emailAddress);
            recipients.add(recipient);
            if (message.getTo() == null) {
                message.setTo(recipients);
            }
            /*message.getTo().add(recipient);*/
            message.setPreserveRecipients(true);
            ArrayList<String> tags = new ArrayList<String>();
            tags.add("Max Desk Account Confirmation");
            tags.add("Max Desk");
            message.setTags(tags);

            messageStatusReports = mandrillApi.messages().send(message, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.getEmailResponse(messageStatusReports);
    }

    public String resendActivationCode(String emailAddress, String verCode, String name) {
        MandrillMessageStatus[] messageStatusReports = null;
        try {
            SysConfig sysConfig = this.sysConfigService.findByConfigName("mandrill_key");
            MandrillApi mandrillApi = new MandrillApi(sysConfig.getConfigValue());

            String emailSubject = "Activate Max Desk account";
            String emailTemplate = "<html>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div style=\"width: 70%; margin-left: 15%;\">\n" +
                    "<div style=\"background-color: #dcdcdc; padding: 2%;\">\n" +
                    "<div style=\"text-align: center; background-color: #f9f9f9; padding: 2%;\">\n" +
                    "<h2 style=\"color: #000000; font-weight: bold;\"><strong>Welcome " + name + "</strong></h2><br/>\n" +
                    "<h3 style=\"color: #000000;\">We are excited to have you back. Activate your account using verification code.</h3><br/>\n" +
                    "<h3 style=\"color: #000000;\">Use this verification code: M-" + verCode + ", on your app or website</h3>\n" +
                    "<h3 style=\"color: #000000;\">If you have questions, just ask to our support team - we are always happy to help out.<br/><br/>Cheers,<br/>Max Desk Team</h3>\n" +
                    "</div>\n" +
                    "<div style=\"background-color: #8882BD; padding: 2%;\">\n" +
                    "<br/>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\"><strong>Is this weird?</strong></h3>\n" +
                    "<h3 style=\"color: #f9f9f9; text-align: center;\">You received this email because you requested to activate your Max Desk account.<br/>\n" +
                    "<!--br/>If these emails get annoying, please feel free to\n" +
                    "<a href=\"#\" style=\"color: #d9534f\">unsubscribe</a-->\n" +
                    "<br/><br/>Max Desk | powered by DataVision International\n" +
                    "</h3>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</div>\n" +
                    "</body>\n" +
                    "</html>";
            // 355619082118223 J727F
            // 865514032254967 (75)

            MandrillMessage message = new MandrillMessage();
            message.setSubject(emailSubject);
            message.setHtml(emailTemplate);
            message.setAutoText(true);
            message.setFromEmail("no-reply@datavision.co.tz");
            message.setFromName("Max Desk");

            List<MandrillMessage.Recipient> recipients = new ArrayList<>();
            final MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
            recipient.setEmail(emailAddress);
            recipient.setName(emailAddress);
            recipients.add(recipient);
            if (message.getTo() == null) {
                message.setTo(recipients);
            }
            /*message.getTo().add(recipient);*/
            message.setPreserveRecipients(true);
            ArrayList<String> tags = new ArrayList<String>();
            tags.add("Max Desk Account Confirmation");
            tags.add("Max Desk");
            message.setTags(tags);

            messageStatusReports = mandrillApi.messages().send(message, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return this.getEmailResponse(messageStatusReports);
    }

    public String sendMail(String key, String emailSubject, String emailTemplate, String emailAddress, String receiverName, String emailTag) {
        MandrillMessageStatus[] messageStatusReports = null;
        try {
            MandrillApi mandrillApi = new MandrillApi(key);

            MandrillMessage message = new MandrillMessage();
            message.setSubject(emailSubject);
            message.setHtml(emailTemplate);
            message.setAutoText(true);
            message.setFromEmail("no-reply@datavision.co.tz");
            message.setFromName("Max Desk");

            List<MandrillMessage.Recipient> recipients = new ArrayList<>();
            final MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
            recipient.setEmail(emailAddress);
            recipient.setName(receiverName);
            recipients.add(recipient);
            /**recipient = new MandrillMessage.Recipient();
             recipient.setEmail("wilydammas@gmail.com");
             recipients.add(recipient);*/
            if (message.getTo() == null) {
                message.setTo(recipients);
            }
            /**message.getTo().add(recipient);*/
            message.setPreserveRecipients(true);
            ArrayList<String> tags = new ArrayList<String>();
            tags.add(emailTag);
            tags.add("Max Desk");
            message.setTags(tags);

            messageStatusReports = mandrillApi.messages().send(message, false);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return this.getEmailResponse(messageStatusReports);
    }

    public String sendMailWithDoc(String key, String emailSubject, String emailText, String attachment, String emailAddress, String receiverName, String emailTag) {
        MandrillMessageStatus[] messageStatusReports = null;
        try {
            MandrillApi mandrillApi = new MandrillApi(key);

            List<MandrillMessage.MessageContent> attachments = new ArrayList<>();
            MandrillMessage.MessageContent attachmentData = new MandrillMessage.MessageContent();
            attachmentData.setType("xlsx");
            attachmentData.setName("Orders");
            attachmentData.setContent(attachment);
            attachments.add(attachmentData);

            MandrillMessage message = new MandrillMessage();
            message.setSubject(emailSubject);
            message.setText(emailText);
            message.setAutoText(true);
            message.setFromEmail("no-reply@datavision.co.tz");
            message.setFromName("Max Desk");
            message.setAttachments(attachments);

            List<MandrillMessage.Recipient> recipients = new ArrayList<>();
            final MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
            recipient.setEmail(emailAddress);
            recipient.setName(receiverName);
            recipients.add(recipient);
            if (message.getTo() == null) {
                message.setTo(recipients);
            }
            /**message.getTo().add(recipient);*/
            message.setPreserveRecipients(true);
            ArrayList<String> tags = new ArrayList<String>();
            tags.add(emailTag);
            tags.add("Max Desk");
            message.setTags(tags);

            messageStatusReports = mandrillApi.messages().send(message, false);

        }catch (Exception e) {
            e.printStackTrace();
        }
        return this.getEmailResponse(messageStatusReports);
    }

    public String sendMailText(String key, String emailSubject, String emailText, String emailAddress, String receiverName, String emailTag) {
        MandrillMessageStatus[] messageStatusReports = null;
        try {
            MandrillApi mandrillApi = new MandrillApi(key);

            MandrillMessage message = new MandrillMessage();
            message.setSubject(emailSubject);
            message.setText(emailText);
            message.setAutoText(true);
            message.setFromEmail("no-reply@datavision.co.tz");
            message.setFromName("Max Desk");

            List<MandrillMessage.Recipient> recipients = new ArrayList<>();
            final MandrillMessage.Recipient recipient = new MandrillMessage.Recipient();
            recipient.setEmail(emailAddress);
            recipient.setName(receiverName);
            recipients.add(recipient);
            if (message.getTo() == null) {
                message.setTo(recipients);
            }
            /**message.getTo().add(recipient);*/
            message.setPreserveRecipients(true);
            ArrayList<String> tags = new ArrayList<String>();
            tags.add(emailTag);
            tags.add("Max Desk");
            message.setTags(tags);

            messageStatusReports = mandrillApi.messages().send(message, false);

        }catch (Exception e) {
            e.printStackTrace();
        }
        return this.getEmailResponse(messageStatusReports);
    }

    public String mailChimpText(String htmlStr) {
        String res = "";
        try {
            MailChimpClient mailChimpClient = new MailChimpClient();
            ListsMethod listsMethod = new ListsMethod();
            listsMethod.apikey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
            ListsResult listsResult = mailChimpClient.execute(listsMethod);
            ListInformation data = listsResult.data.get(0);

            CampaignCreateMethod campaignCreateMethod = new CampaignCreateMethod();
            campaignCreateMethod.apikey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
            campaignCreateMethod.type = CampaignType.regular;

            MailChimpObject options = new MailChimpObject();

            campaignCreateMethod.options = new MailChimpObject();
            options.put("list_id", "xxxxxxxxxx");
            options.put("subject", "xxxxxxxxxxxxxxxx");
            options.put("from_email", "xxxx@ssss.in");
            options.put("from_name", "xxxxx");
            options.put("authenticate", true);
            options.put("title", "xxxxxxx");
            options.put("tracking", "");
            campaignCreateMethod.options = options;

            MailChimpObject content = new MailChimpObject();
            content.put("html", htmlStr);
            campaignCreateMethod.content = content;
            Gson gson = new Gson();
            res = gson.toJson(content);
            String compaignId = mailChimpClient.execute(campaignCreateMethod);

            CampaignSendNowMethod campaignSendNowMethod = new CampaignSendNowMethod();
            campaignSendNowMethod.apikey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
            campaignSendNowMethod.cid = compaignId;
            mailChimpClient.execute(campaignSendNowMethod);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    /**
     public void springMailWithAttachment(String to, String subject, String text, String pathToAttachment) {
     try {
     MimeMessage message = emailSender.createMimeMessage();
     MimeMessageHelper helper = new MimeMessageHelper(message, true);

     helper.setTo(to);
     helper.setSubject(subject);
     helper.setText(text);

     FileSystemResource file = new FileSystemResource(new File(pathToAttachment));
     helper.addAttachment("Order Details", file);

     emailSender.send(message);
     } catch (Exception e) {
     e.printStackTrace();
     }
     }
     */
}
