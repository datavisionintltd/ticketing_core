package com.dvi.ticket.util;

import com.dvi.ticket.model.SmsBalance;
import com.dvi.ticket.model.SmsLog;
import com.dvi.ticket.model.Tenant;
import com.dvi.ticket.repository.SmsBalanceRepository;
import com.dvi.ticket.service.SmsBalanceService;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

@Component
public class SmsUtilities {

    private DviUtilities dviUtilities;
    private SmsBalanceRepository smsBalanceRepository;

    public SmsUtilities(DviUtilities dviUtilities, SmsBalanceRepository smsBalanceRepository) {
        this.dviUtilities = dviUtilities;
        this.smsBalanceRepository = smsBalanceRepository;
    }

    public String sendSms(SmsLog smsApi) {
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("status", "not_processed");
        String res = jsonResponse.toString();
        CloseableHttpClient httpClient = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
        try {
            JSONObject receiver = new JSONObject();
            int key_id = 0;
            String jsonString = smsApi.getRecipient();
            String[] recipients = jsonString.split(",");
            for(String recipient : recipients) {
                key_id += 1;
                receiver.put("message_id" + key_id, Long.parseLong(recipient.replaceAll("\\p{P}","").trim()));
            }
            JSONObject sms_content = new JSONObject();
            sms_content.put("token", "f41d4790c47bca4bc056ab32a3099f3c");
            sms_content.put("sender", smsApi.getSender());
            sms_content.put("message", smsApi.getMessage());
            sms_content.put("push", smsApi.getPush());
            sms_content.put("recipient", receiver);

            HttpPost post = new HttpPost("http://login.smsmtandao.com/smsmtandaoapi/send");
            post.setHeader("Content-type", "application/json");
            post.setEntity(new StringEntity(sms_content.toString()));

            HttpResponse response = httpClient.execute(post);
            InputStream inputStream = response.getEntity().getContent();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                /*throw new Exception(response.getStatusLine().getReasonPhrase());*/
                JSONObject jResponse = new JSONObject();
                jResponse.put("status", "insufficient_balance");
                res = jResponse.toString();
            }

            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
            }

            bufferedReader.close();
            inputStream.close();

            JSONObject jsonResult = new JSONObject(stringBuilder.toString());
            if(jsonResult != null) {
                res = jsonResult.toString();
            }
        } catch (Exception e) {
            e.printStackTrace();
            JSONObject jsonString = new JSONObject();
            jsonString.put("status", "internal_server_error");
            res = jsonString.toString();
        }
        return res;
    }

    public String sendTenantSms(SmsLog smsApi, Tenant tenant) {
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("status", "not_processed");
        String res = jsonResponse.toString();
        CloseableHttpClient httpClient = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();

        try {
            JSONObject receiver = new JSONObject();
            int key_id = 0;
            String jsonString = smsApi.getRecipient();
            String[] recipients = jsonString.split(",");
            for(String recipient : recipients) {
                key_id += 1;
                receiver.put("message_id" + key_id, Long.parseLong(recipient.replaceAll("\\p{P}","").trim()));
            }
            JSONObject sms_content = new JSONObject();
            sms_content.put("token", "f41d4790c47bca4bc056ab32a3099f3c");
            sms_content.put("sender", smsApi.getSender());
            sms_content.put("message", smsApi.getMessage());
            sms_content.put("push", smsApi.getPush());
            sms_content.put("recipient", receiver);

            int numberOfMessages;
            if (smsApi.getMessage().length() > 160) {
                double message_ratio = smsApi.getMessage().length() / 153;
                numberOfMessages = key_id * (int) Math.ceil(message_ratio);
            } else {
                numberOfMessages = key_id;
            }

            if (this.smsBalanceRepository.findByTenant(tenant).getBalanceSms() < numberOfMessages) {
                JSONObject jResponse = new JSONObject();
                jResponse.put("status", "insufficient_balance");
                res = jResponse.toString();
            }

            HttpPost post = new HttpPost("http://login.smsmtandao.com/smsmtandaoapi/send");
            post.setHeader("Content-type", "application/json");
            post.setEntity(new StringEntity(sms_content.toString()));

            HttpResponse response = httpClient.execute(post);
            InputStream inputStream = response.getEntity().getContent();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                /*throw new Exception(response.getStatusLine().getReasonPhrase());*/
                JSONObject jResponse = new JSONObject();
                jResponse.put("status", "insufficient_balance");
                res = jResponse.toString();
            }

            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
            }

            bufferedReader.close();
            inputStream.close();

            JSONObject jsonResult = new JSONObject(stringBuilder.toString());
            if(this.dviUtilities.isJSONValid(stringBuilder.toString()) && jsonResult.has("status")) {
                res = jsonResult.toString();
                if (jsonResult.getString("status").equalsIgnoreCase("succeed")) {
                    this.deductSMSBalance(tenant, numberOfMessages);
                } else {
                    System.out.println(sms_content);
                    System.out.println(jsonResult.toString());
                }
            }
        } catch(Exception e) {
            e.printStackTrace();
            JSONObject jsonString = new JSONObject();
            jsonString.put("status", "internal_server_error");
            res = jsonString.toString();
        }
        return res;
    }

    private void deductSMSBalance(Tenant tenant, int sms) {
        SmsBalance smsBalanceData = this.smsBalanceRepository.findByTenant(tenant);
        float amount = sms * 40;
        smsBalanceData.setBalanceAmt(smsBalanceData.getBalanceAmt() - amount);
        smsBalanceData.setBalanceSms(smsBalanceData.getBalanceSms() - sms);
        this.smsBalanceRepository.save(smsBalanceData);
    }
}
