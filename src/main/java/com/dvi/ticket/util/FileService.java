package com.dvi.ticket.util;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Service
public class FileService {

    private static final String FILE_DIRECTORY = "/opt/tomcat/files/products";
    private static final String PROFILE_IMAGES_DIRECTORY = "/var/www/image.cibo.co.tz/profile";
    private static final String IMAGES_DIRECTORY_ONE = "/var/www/image.cibo.co.tz/uploads";
    private static final String IMAGES_DIRECTORY_TWO = "/var/lib/fcc/products";

    public void storeFile(MultipartFile file, String fileName, String fileType, String fileDirectory) throws IOException {
        try {
            /*Path filePath = Paths.get(fileDirectory + "/" + file.getOriginalFilename());*/
            Path filePath = Paths.get(fileDirectory + "/" + fileName + "." + fileType);

            Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void storeDocument(MultipartFile file, String fileName, String fileType, String fileDirectory) throws IOException {
        try {
            /*Path filePath = Paths.get(fileDirectory + "/" + file.getOriginalFilename());*/
            Path filePath = Paths.get(fileDirectory + "/" + fileName + "." + fileType);

            Files.copy(file.getInputStream(), filePath, StandardCopyOption.REPLACE_EXISTING);

            boolean permission = true;
            File file1 = new File(fileDirectory + "/" + fileName + "." + fileType);
            if (file1.exists()) {
                permission = file1.setExecutable(true, false);
                permission = file1.setReadable(true, false);
                permission = file1.setWritable(true, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void storeProductImage(MultipartFile file, String fileName) throws IOException {
        try {
            Path filePath1 = Paths.get(IMAGES_DIRECTORY_ONE + "/" + fileName);
            Path filePath2 = Paths.get(IMAGES_DIRECTORY_TWO + "/" + fileName);
            Files.copy(file.getInputStream(), filePath1, StandardCopyOption.REPLACE_EXISTING);
            Files.copy(file.getInputStream(), filePath2, StandardCopyOption.REPLACE_EXISTING);

            boolean permission = true;
            File file1 = new File(IMAGES_DIRECTORY_ONE + "/" + fileName);
            if (file1.exists()) {
                permission = file1.setExecutable(true, false);
                permission = file1.setReadable(true, false);
                permission = file1.setWritable(true, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copyOriginalImage(String extension, long productId) throws IOException {
        try {
            File originalImage = new File("/var/www/cibo.co.tz/cibo/assets/uploads/product_" + productId + "." + extension);
            File filePath1 = new File("/var/www/image.cibo.co.tz/uploads/product_" + productId + "." + extension);
            if (originalImage.exists()) {
                FileUtils.copyFile(originalImage, filePath1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copyDefaultImage(long productId) throws IOException {
        try {
            File defaultImage = new File("/var/www/image.cibo.co.tz/default.jpg");
            File filePath1 = new File("/var/www/image.cibo.co.tz/uploads/product_" + productId + ".jpg");
            FileUtils.copyFile(defaultImage, filePath1);

            boolean permission = true;
            File file1 = new File("/var/www/image.cibo.co.tz/uploads/product_" + productId + ".jpg");
            if (file1.exists()) {
                permission = file1.setExecutable(true, false);
                permission = file1.setReadable(true, false);
                permission = file1.setWritable(true, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void storeProfileImage(MultipartFile file, String fileName) throws IOException {
        try {
            Path filePath1 = Paths.get(PROFILE_IMAGES_DIRECTORY + "/" + fileName);
            Files.copy(file.getInputStream(), filePath1, StandardCopyOption.REPLACE_EXISTING);

            boolean permission = true;
            File file1 = new File(PROFILE_IMAGES_DIRECTORY + "/" + fileName);
            if (file1.exists()) {
                permission = file1.setExecutable(true, false);
                permission = file1.setReadable(true, false);
                permission = file1.setWritable(true, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void copyDefaultProfile(long userId) throws IOException {
        try {
            File defaultImage = new File(PROFILE_IMAGES_DIRECTORY + "/user.png");
            File filePath1 = new File(PROFILE_IMAGES_DIRECTORY + "/user_" + userId + ".png");
            FileUtils.copyFile(defaultImage, filePath1);

            boolean permission = true;
            File file1 = new File(PROFILE_IMAGES_DIRECTORY + "/user_" + userId + ".png");
            if (file1.exists()) {
                permission = file1.setExecutable(true, false);
                permission = file1.setReadable(true, false);
                permission = file1.setWritable(true, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
