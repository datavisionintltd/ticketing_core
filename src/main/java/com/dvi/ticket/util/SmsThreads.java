package com.dvi.ticket.util;

import com.dvi.ticket.model.SmsLog;
import com.dvi.ticket.repository.SmsLogRepository;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SmsThreads implements Runnable {

    private SmsUtilities smsUtilities;
    private SmsLogRepository smsLogRepository;
    private Thread thread;
    private String threadName;
    private String phoneNumber;
    private String verCode;

    public SmsThreads(String threadName, SmsUtilities smsUtilities, SmsLogRepository smsLogRepository, String phoneNumber, String verCode) {
        this.threadName = threadName;
        this.smsUtilities = smsUtilities;
        this.smsLogRepository = smsLogRepository;
        this.phoneNumber = phoneNumber;
        this.verCode = verCode;
        System.out.println("Creating " + threadName);
    }

    public void run() {
        System.out.println("Running " + threadName);
        try {
            SmsLog smsLog = new SmsLog();
            List<String> receivers_list = new ArrayList<String>();
            receivers_list.add(this.phoneNumber);
            smsLog.setSender("INFO");
            smsLog.setPush("your delivery link");
            smsLog.setStatus("Committed");
            smsLog.setMessage("Your Max Desk Account is ready. To activate this account, enter this verification code M-" + this.verCode);
            smsLog.setRecipient(receivers_list.toString());
            SmsLog sms_results = this.smsLogRepository.save(smsLog);

            JSONObject sent_response = new JSONObject(smsUtilities.sendSms(sms_results));
            if (sent_response.getString("status").equalsIgnoreCase("succeed")) {
                sms_results.setStatus("SUCCESSFUL");
            } else {
                sms_results.setStatus(sent_response.getString("status"));
            }
            this.smsLogRepository.save(sms_results);
        } catch (Exception e) {
            System.out.println(threadName + " is interrupted");
            e.printStackTrace();
        }
        System.out.println("Exiting " + threadName);
    }

    public void start () {
        if (this.thread == null) {
            this.thread = new Thread(this, this.threadName);
            this.thread.start();
        }
    }
}
