package com.dvi.ticket.util.constants;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class LocaleConstants {
    public static List<Locale> LOCALES = Arrays.asList(new Locale("en"),
            new Locale("sw"));
}
