package com.dvi.ticket.util;

import com.dvi.ticket.model.Country;
import com.dvi.ticket.model.SmsLog;
import com.dvi.ticket.service.CountryService;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Component
public class RemoteUtilities {

    private CountryService countryService;

    public RemoteUtilities(CountryService countryService) {
        this.countryService = countryService;
    }

    public void getCountries(String url, String type) {
        try {
            CloseableHttpClient httpClient = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
            HttpGet get = new HttpGet(url);
            HttpResponse response = httpClient.execute(get);
            InputStream inputStream = response.getEntity().getContent();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

            if (response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new Exception(response.getStatusLine().getReasonPhrase());
            }

            StringBuilder stringBuilder = new StringBuilder();
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
            }

            bufferedReader.close();
            inputStream.close();

            if (type.equalsIgnoreCase("names")) {
                List<Country> countries = new ArrayList<>();
                JSONObject countryJson = new JSONObject(stringBuilder.toString());
                countryJson.keySet().forEach(code -> {
                    Country country = new Country();
                    country.setCode(code);
                    country.setName(countryJson.getString(code));
                    countries.add(country);
                });
                this.countryService.update(countries);
            } else if (type.equalsIgnoreCase("codes")) {
                JSONObject countryJson = new JSONObject(stringBuilder.toString());
                countryJson.keySet().forEach(code -> {
                    Country country = this.countryService.findByCode(code);
                    if (country != null) {
                        country.setDialCode(countryJson.getString(code));
                        this.countryService.update(country);
                    }
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
