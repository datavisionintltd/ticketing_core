package com.dvi.ticket.resource;

import com.dvi.ticket.model.Department;
import com.dvi.ticket.service.DepartmentService;
import com.dvi.ticket.service.TeamService;
import com.dvi.ticket.service.TopicService;
import com.dvi.ticket.service.UserDepartmentService;
import org.springframework.stereotype.Component;

@Component
public class DepartmentResource {

    private DepartmentService departmentService;
    private TopicService topicService;
    private TeamService teamService;
    private UserDepartmentService userDepartmentService;

    public DepartmentResource(DepartmentService departmentService, TopicService topicService, TeamService teamService, UserDepartmentService userDepartmentService) {
        this.departmentService = departmentService;
        this.topicService = topicService;
        this.teamService = teamService;
        this.userDepartmentService = userDepartmentService;
    }

    public boolean deleteDepartment(Department department) {
        boolean response = false;
        try {
            boolean topics = this.topicService.findAll(department, 0, 1, "id", "asc").getContent().isEmpty();
            boolean teams = this.teamService.findAll(department, 0, 1, "id", "asc").getContent().isEmpty();
            boolean userDepartments = this.userDepartmentService.findAll(department, 0, 1, "id", "asc").getContent().isEmpty();
            if (topics && teams && userDepartments) {
                this.departmentService.remove(department);
                response = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
