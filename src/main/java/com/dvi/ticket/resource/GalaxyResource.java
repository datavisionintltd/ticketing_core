package com.dvi.ticket.resource;

import com.dvi.ticket.model.Role;
import com.dvi.ticket.model.User;
import com.dvi.ticket.model.UserRole;
import com.dvi.ticket.service.UserService;
import com.dvi.ticket.util.DviUtilities;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.LaxRedirectStrategy;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Component
public class GalaxyResource {

    private DviUtilities dviUtilities;
    private UserService userService;

    public GalaxyResource(DviUtilities dviUtilities, UserService userService) {
        this.dviUtilities = dviUtilities;
        this.userService = userService;
    }

    public String galaxyUser(String ssoToken, String clientSecret) {
        JSONObject jsonResponse = new JSONObject();
        jsonResponse.put("success", false);
        String res = jsonResponse.toString();
        try {
            CloseableHttpClient httpClient = HttpClients.custom().setRedirectStrategy(new LaxRedirectStrategy()).build();
            HttpPost post = new HttpPost("https://api.softwaregalaxy.co.tz/auth/s-auth");
            post.setHeader("Accept", "application/json");
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("sso_token", ssoToken));
            params.add(new BasicNameValuePair("client_secret", clientSecret));
            post.setEntity(new UrlEncodedFormEntity(params));
            // post.setEntity(new StringEntity(mlipaJson));

            HttpResponse response = httpClient.execute(post);
            InputStream ips  = response.getEntity().getContent();
            BufferedReader buf = new BufferedReader(new InputStreamReader(ips));
            if(response.getStatusLine().getStatusCode()!= org.apache.http.HttpStatus.SC_OK)
            {
                JSONObject jsonString = new JSONObject();
                jsonString.put("success", false);
                res = jsonString.toString();
            }
            StringBuilder sb = new StringBuilder();
            String line;
            while((line = buf.readLine()) != null)
            {
                sb.append(line);
                sb.append(System.lineSeparator());
            }
            buf.close();
            ips.close();

            if (this.dviUtilities.isJSONValid(sb.toString())) {
                JSONObject jsonString = new JSONObject(sb.toString());
                res = jsonString.toString();
            } else {
                System.out.println(sb.toString());
            }
        } catch(Exception e) {
            e.printStackTrace();
            JSONObject jsonString = new JSONObject();
            jsonString.put("success", false);
            res = jsonString.toString();
        }
        return res;
    }

    public User registerUser(String galaxyStr) {
        User results = null;
        try {
            JSONObject userJson = new JSONObject(galaxyStr);
            User userData = new User();
            String user_type = "Manager";

            if (userJson.has("first_name") && userJson.has("last_name") && userJson.has("phone_number") && userJson.has("username")) {
                if (!userJson.isNull("first_name") && !userJson.isNull("last_name") && !userJson.isNull("phone_number") && !userJson.isNull("username")) {
                    String fullName = userJson.getString("first_name")
                            + (userJson.has("middle_name") ? " " + userJson.getString("middle_name") : "")
                            + " " + userJson.getString("last_name");
                    userData.setUsername(userJson.getString("username"));
                    userData.setFullName(fullName);
                    userData.setEmail(userJson.has("email") ? " " + userJson.getString("email") : userJson.getString("username"));
                    userData.setPhoneNumber(this.dviUtilities.formatPhoneNumber("+255", userJson.getString("phone_number")));
                    userData.setPassword("123456");
                    userData.setEnabled(true);
                    User userResult = this.userService.addUser(userData);
                    User userResults = this.userService.updateUser(userResult);

                    Role roleInfo = this.userService.findRoleByName("ROLE_USER");
                    UserRole userRole = this.userService.addUserRole(userResults, roleInfo);
                    if (userRole != null) {
                        results = userRole.getUser();
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return results;
    }
}
