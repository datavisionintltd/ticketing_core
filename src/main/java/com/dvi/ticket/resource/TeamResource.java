package com.dvi.ticket.resource;

import com.dvi.ticket.model.Team;
import com.dvi.ticket.model.TeamUser;
import com.dvi.ticket.service.TeamService;
import com.dvi.ticket.service.TeamUserService;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class TeamResource {

    private TeamService teamService;
    private TeamUserService teamUserService;

    public TeamResource(TeamService teamService, TeamUserService teamUserService) {
        this.teamService = teamService;
        this.teamUserService = teamUserService;
    }

    public boolean deleteTeam(Team team) {
        boolean response = false;
        try {
            List<TeamUser> teamUsers = this.teamUserService.findAll(team, 0, Integer.MAX_VALUE, "id", "asc").getContent();
            if (teamUsers.isEmpty()) {
                this.teamService.remove(team);
                response = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return  response;
    }
}
