package com.dvi.ticket.resource;

import com.dvi.ticket.model.Payment;
import com.dvi.ticket.model.SmsLog;
import com.dvi.ticket.model.Tenant;
import com.dvi.ticket.service.SmsLogService;
import com.dvi.ticket.service.TenantService;
import com.dvi.ticket.util.DviUtilities;
import com.dvi.ticket.util.SmsUtilities;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SmsResource {

    private SmsLogService smsLogService;
    private SmsUtilities smsUtilities;
    private DviUtilities dviUtilities;
    private TenantService tenantService;

    public SmsResource(SmsLogService smsLogService, SmsUtilities smsUtilities, DviUtilities dviUtilities, TenantService tenantService) {
        this.smsLogService = smsLogService;
        this.smsUtilities = smsUtilities;
        this.dviUtilities = dviUtilities;
        this.tenantService = tenantService;
    }

    public void tenantPaymentSms(Tenant tenant, Payment payment) {
        try {
            SmsLog smsLog = new SmsLog();
            List<String> receivers_list = new ArrayList<>();
            receivers_list.add(payment.getMsisdn());
            smsLog.setSender("INFO");
            smsLog.setPush("your delivery link");
            smsLog.setStatus("Committed");
            smsLog.setMessage(this.paymentMsg(tenant, payment.getAmount(), payment.getCharges()));
            smsLog.setRecipient(receivers_list.toString());
            SmsLog results = this.smsLogService.addSmsLog(smsLog);

            Tenant sms_institute = this.tenantService.findById(1);

            if (this.smsUtilities.sendTenantSms(results, tenant).equalsIgnoreCase("succeed")) {
                results.setStatus("SUCCESSFUL");
            } else {
                results.setStatus("FAILED");
            }
            this.smsLogService.update(results);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String paymentMsg(Tenant tenant, float amt, float charges) {
        String sender = "M-Lipa";
        float amount = amt + charges;
        return "Umefanikiwa kulipia TZS " + amount + " kwenda " + tenant.getName() + ", makato ya muamala ni Tshs " + charges + ". Ahsante kwa kutumia " + sender + ".";
    }
}
