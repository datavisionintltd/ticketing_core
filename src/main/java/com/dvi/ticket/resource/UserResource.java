package com.dvi.ticket.resource;

import com.dvi.ticket.model.Role;
import com.dvi.ticket.service.UserService;
import org.springframework.stereotype.Component;

@Component
public class UserResource {

    private UserService userService;

    public UserResource(UserService userService) {
        this.userService = userService;
    }

    public boolean deleteRole(Role role) {
        boolean response = false;
        try {
            boolean userRoles = this.userService.findUserRoleByRole(role).isEmpty();
            if (userRoles) {
                this.userService.removeRole(role);
                response = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
}
