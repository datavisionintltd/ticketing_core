package com.dvi.ticket.rest;

import com.dvi.ticket.model.Dictionary;
import com.dvi.ticket.model.DictionaryItem;
import com.dvi.ticket.service.DictionaryItemService;
import com.dvi.ticket.service.DictionaryService;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import org.springframework.transaction.annotation.Transactional;

@RestController
@Transactional
@RequestMapping(value = "/dictionaries")
@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
public class DictionaryController {

    private DictionaryService dictionaryService;
    private DictionaryItemService dictionaryItemService;

    public DictionaryController(DictionaryService dictionaryService, DictionaryItemService dictionaryItemService) {
        this.dictionaryService = dictionaryService;
        this.dictionaryItemService = dictionaryItemService;
    }

    @RequestMapping(value = "/{dictionaryId}", method = RequestMethod.GET, produces = "application/json")
    public Dictionary getDictionary(@PathVariable long dictionaryId) {
        return this.dictionaryService.findById(dictionaryId);
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addDictionary(@RequestBody Dictionary dictionary) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Dictionary results = this.dictionaryService.create(dictionary);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> editDictionaryItem(@RequestBody Dictionary dictionary) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            this.dictionaryService.update(dictionary);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/{dictionaryId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeDictionary(@PathVariable long dictionaryId) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            Dictionary dictionary = this.dictionaryService.findById(dictionaryId);
            this.dictionaryService.delete(dictionary);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public Page<Dictionary> getDictionaries(@RequestParam(value = "page", defaultValue = "0") int page,
                                            @RequestParam(value = "size", defaultValue = "10") int size,
                                            @RequestParam(value = "sort", defaultValue = "id") String sort,
                                            @RequestParam(value = "order", defaultValue = "desc") String order) {
        return this.dictionaryService.findAll(page, size, sort, order);
    }

    @RequestMapping(value = "/items", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addDictionaryItem(@RequestBody String dictionaryItem) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject dictionaryItemJson = new JSONObject(dictionaryItem);
            DictionaryItem item = new DictionaryItem();//
            long id = dictionaryItemJson.getLong("dictionary");
            Dictionary dictionary = dictionaryService.findById(id);
            item.setDictionary(dictionary);
            item.setCode(dictionaryItemJson.getString("code"));
            item.setName(dictionaryItemJson.getString("name"));
            DictionaryItem results = this.dictionaryItemService.create(item);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/items", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> editDictionaryItem(@RequestBody String dictionaryItem) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            DictionaryItem ditem = new DictionaryItem();
            JSONObject dictionaryItemJson = new JSONObject(dictionaryItem);
            long did = dictionaryItemJson.getLong("itemId");
            DictionaryItem item = dictionaryItemService.findById(did);
            item.setCode(dictionaryItemJson.getString("code"));
            item.setName(dictionaryItemJson.getString("name"));
            this.dictionaryItemService.update(item);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/items/{dictionaryItemId}", method = RequestMethod.GET, produces = "application/json")
    public DictionaryItem getDictionaryItem(@PathVariable long dictionaryItemId) {
        DictionaryItem dictionaryItem = new DictionaryItem();
        try {
            dictionaryItem = this.dictionaryItemService.findById(dictionaryItemId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dictionaryItem;
    }

    @RequestMapping(value = "/items/{dictionaryItemId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeDictionaryItem(@PathVariable long dictionaryItemId) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            DictionaryItem dictionaryItem = this.dictionaryItemService.findById(dictionaryItemId);
            this.dictionaryItemService.delete(dictionaryItem);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/items", method = RequestMethod.GET, produces = "application/json")
    public Page<DictionaryItem> getDictionaryItemList(@RequestParam(value = "dictionaryId", defaultValue = "0") long dictionaryId,
                                                      @RequestParam(value = "code", defaultValue = "") String code,
                                                      @RequestParam(value = "page", defaultValue = "0") int page,
                                                      @RequestParam(value = "size", defaultValue = "10") int size,
                                                      @RequestParam(value = "sort", defaultValue = "id") String sort,
                                                      @RequestParam(value = "order", defaultValue = "desc") String order) {
        Dictionary dictionary = null;
        if (dictionaryId != 0 && code.length() > 0) {
            dictionary = dictionaryService.findByIdAndCode(dictionaryId, code);
        } else if (dictionaryId != 0) {
            dictionary = dictionaryService.findById(dictionaryId);
        } else if (code.length() > 0) {
            dictionary = dictionaryService.findByCode(code);
        }
        return this.dictionaryItemService.findAll(dictionary, page, size, sort, order);
    }
}
