package com.dvi.ticket.rest;

import com.dvi.ticket.model.Dictionary;
import com.dvi.ticket.model.DictionaryItem;
import com.dvi.ticket.service.DictionaryItemService;
import com.dvi.ticket.service.DictionaryService;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/items")
@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
public class DictionaryItemController {

    private DictionaryItemService dictionaryItemService;
    private DictionaryService dictionaryService;

    public DictionaryItemController(DictionaryItemService dictionaryItemService, DictionaryService dictionaryService) {
        this.dictionaryItemService = dictionaryItemService;
        this.dictionaryService = dictionaryService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public List<DictionaryItem> listItems(@RequestParam(value = "id") long id) {
        Dictionary dictionary = dictionaryService.findById(id);
        return this.dictionaryItemService.findAll(dictionary, 0, Integer.MAX_VALUE, "id", "asc").getContent();
    }

    @RequestMapping(value = "/{dictionaryCode}", method = RequestMethod.GET, produces = "application/json")
    public List<DictionaryItem> listItemsByCode(@PathVariable String dictionaryCode) {
        Dictionary dictionary = dictionaryService.listItemsByCode(dictionaryCode);
        return this.dictionaryItemService.findAll(dictionary, 0, Integer.MAX_VALUE, "id", "asc").getContent();
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addDictionaryItem(@RequestBody String dictionaryItem) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject dictionaryItemJson = new JSONObject(dictionaryItem);
            DictionaryItem item = new DictionaryItem();
            long id = dictionaryItemJson.getLong("dictionary");
            Dictionary dictionary = dictionaryService.findById(id);
            item.setDictionary(dictionary);
            item.setCode(dictionaryItemJson.getString("code"));
            item.setName(dictionaryItemJson.getString("name"));
            DictionaryItem results = this.dictionaryItemService.create(item);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> editDictionary(@RequestBody String dictionaryItem) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            DictionaryItem ditem = new DictionaryItem();
            JSONObject dictionaryItemJson = new JSONObject(dictionaryItem);
            long did = dictionaryItemJson.getLong("itemId");
            DictionaryItem item = dictionaryItemService.findById(did);
            item.setCode(dictionaryItemJson.getString("code"));
            item.setName(dictionaryItemJson.getString("name"));
            this.dictionaryItemService.update(item);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/{dictionaryItemId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeDictionaryItem(@PathVariable long dictionaryItemId) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            DictionaryItem dictionaryItem = this.dictionaryItemService.findById(dictionaryItemId);
            this.dictionaryItemService.delete(dictionaryItem);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }
}

