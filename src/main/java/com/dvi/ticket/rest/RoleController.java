package com.dvi.ticket.rest;

import com.dvi.ticket.model.Role;
import com.dvi.ticket.model.UserRole;
import com.dvi.ticket.resource.UserResource;
import com.dvi.ticket.service.UserService;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

@RestController
@RequestMapping(value = "/roles")
public class RoleController {

    private UserService userService;
    private UserResource userResource;

    @Autowired
    public RoleController(UserService userService, UserResource userResource) {
        this.userService = userService;
        this.userResource = userResource;
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "",  method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<String> addRole(@RequestBody Role role) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                Role results = this.userService.addRole(role);

                if (results != null) {
                    httpStatus = HttpStatus.OK;
                    response.put("status", "success");
                    response.put("message", "New Role added successfully");
                }
            } else {
                httpStatus = HttpStatus.UNAUTHORIZED;
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "",  method = RequestMethod.PUT)
    public @ResponseBody ResponseEntity<String> editRole(@RequestBody Role role) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {

                Role results = this.userService.addRole(role);

                if (results != null) {
                    httpStatus = HttpStatus.OK;
                    response.put("status", "success");
                    response.put("message", "Role updated successfully");
                }
            } else {
                httpStatus = HttpStatus.UNAUTHORIZED;
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "/{roleId}",  method = RequestMethod.DELETE, produces = "application/json")
    public @ResponseBody ResponseEntity<String> removeRole(@PathVariable long roleId) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {

                Role roleInfo = this.userService.findRoleByRoleId(roleId);

                if (this.userResource.deleteRole(roleInfo)) {
                    httpStatus = HttpStatus.OK;
                    response.put("status", "success");
                    response.put("message", "Role removed successfully");
                } else {
                    httpStatus = HttpStatus.EXPECTATION_FAILED;
                    response.put("status", "failed");
                    response.put("message", "Failed to remove role");
                }
            } else {
                httpStatus = HttpStatus.UNAUTHORIZED;
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    // @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN') or hasAuthority('ROLE_ADMIN')")
    @PreAuthorize("hasRole('SUPER_ADMIN') or hasRole('ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<Role> getAllRoles() {
        List<Role> roles = new ArrayList<>();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                Predicate<GrantedAuthority> bySuperAdmin = superAdmin -> superAdmin.getAuthority().equalsIgnoreCase("ROLE_SUPER_ADMIN");
                Predicate<GrantedAuthority> byAdmin = admin -> admin.getAuthority().equalsIgnoreCase("ROLE_ADMIN");
                if (authentication.getAuthorities().stream().anyMatch(bySuperAdmin)) {
                    roles = Arrays.asList(this.userService.findRoleByName("ROLE_SUPER_ADMIN"));
                } else if (authentication.getAuthorities().stream().anyMatch(byAdmin)) {
                    roles = this.userService.findAllRoles();
                    roles.remove(this.userService.findRoleByName("ROLE_SUPER_ADMIN"));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return roles;
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN') or hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{roleName}/users", method = RequestMethod.GET)
    public List<UserRole> getUsersByRole(@PathVariable String roleName) {
        List<UserRole> userRoleList = new ArrayList<>();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                Role role = this.userService.findRoleByName(roleName);
                userRoleList = this.userService.findUserRoleByRole(role);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userRoleList;
    }
}
