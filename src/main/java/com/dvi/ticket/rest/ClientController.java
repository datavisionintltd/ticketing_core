package com.dvi.ticket.rest;

import com.dvi.ticket.config.tenant.InboundRequest;
import com.dvi.ticket.dao.ClientTicketCount;
import com.dvi.ticket.dao.StatusCount;
import com.dvi.ticket.model.Client;
import com.dvi.ticket.model.TicketStatus;
import com.dvi.ticket.model.User;
import com.dvi.ticket.service.ClientService;
import com.dvi.ticket.service.TicketService;
import com.dvi.ticket.service.TicketStatusService;
import com.dvi.ticket.service.UserService;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/clients")
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
public class ClientController {

    private ClientService clientService;
    private TicketStatusService ticketStatusService;
    private TicketService ticketService;
    private UserService userService;

    public ClientController(ClientService clientService, TicketStatusService ticketStatusService, TicketService ticketService, UserService userService) {
        this.clientService = clientService;
        this.ticketStatusService = ticketStatusService;
        this.ticketService = ticketService;
        this.userService = userService;
    }

    @RequestMapping(value = "",  method = RequestMethod.POST, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> addClient(@RequestBody String clientStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject clientJson = new JSONObject(clientStr);
            if (clientJson.has("name")) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (!(authentication instanceof AnonymousAuthenticationToken)) {

                    Client clientData = new Client();
                    clientData.setName(clientJson.getString("name"));
                    if (clientJson.has("email")) {
                        clientData.setEmail(clientJson.getString("email"));
                    }
                    if (clientJson.has("phone")) {
                        clientData.setPhone(clientJson.getString("phone"));
                    }
                    if (clientJson.has("description")) {
                        clientData.setDescription(clientJson.getString("description"));
                    }

                    Client results = this.clientService.addClient(clientData);

                    if (results != null) {
                        httpStatus = HttpStatus.OK;
                        response.put("status", "success");
                        response.put("message", "Client added successfully");
                    } else {
                        httpStatus = HttpStatus.EXPECTATION_FAILED;
                        response.put("status", "failed");
                        response.put("message", "Failed to add client");
                    }
                } else {
                    httpStatus = HttpStatus.UNAUTHORIZED;
                    response.put("status", "unauthorized");
                    response.put("message", "You are not authorized to perform this action");
                }
            } else {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Client name cannot be empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "",  method = RequestMethod.PATCH, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> editClient(@RequestBody String clientStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject clientJson = new JSONObject(clientStr);
            if (clientJson.has("id")) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (!(authentication instanceof AnonymousAuthenticationToken)) {

                    Client clientInfo = this.clientService.findById(clientJson.getLong("id"));
                    if (clientJson.has("name")) {
                        clientInfo.setName(clientJson.getString("name"));
                    }
                    if (clientJson.has("email")) {
                        clientInfo.setEmail(clientJson.getString("email"));
                    }
                    if (clientJson.has("phone")) {
                        clientInfo.setPhone(clientJson.getString("phone"));
                    }
                    if (clientJson.has("description")) {
                        clientInfo.setDescription(clientJson.getString("description"));
                    }

                    this.clientService.update(clientInfo);

                    httpStatus = HttpStatus.OK;
                    response.put("status", "success");
                    response.put("message", "Client updated successfully");
                } else {
                    httpStatus = HttpStatus.UNAUTHORIZED;
                    response.put("status", "unauthorized");
                    response.put("message", "You are not authorized to perform this action");
                }
            } else {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Client id cannot be empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/{clientId}",  method = RequestMethod.DELETE, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> removeClient(@PathVariable long clientId) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {

                Client clientInfo = this.clientService.findById(clientId);

                this.clientService.remove(clientInfo);

                httpStatus = HttpStatus.OK;
                response.put("status", "success");
                response.put("message", "Client removed successfully");
            } else {
                httpStatus = HttpStatus.UNAUTHORIZED;
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/{clientId}", method = RequestMethod.GET, produces = "application/json")
    @InboundRequest
    public Client getClient(@PathVariable long clientId) {
        Client client = new Client();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                client = this.clientService.findById(clientId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    @InboundRequest
    public Page<Client> getClients(@RequestParam(value = "name", defaultValue = "") String name,
                                   @RequestParam(value = "page", defaultValue = "0") int page,
                                   @RequestParam(value = "size", defaultValue = "10") int size,
                                   @RequestParam(value = "sort", defaultValue = "id") String sort,
                                   @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<Client> clientList = Collections.emptyList();
        Page<Client> clients = new PageImpl<>(clientList);
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                clients = this.clientService.findAll(name, page, size, sort, order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clients;
    }

    @RequestMapping(value = "/ticket/statuses", method = RequestMethod.GET, produces = "application/json")
    public List<ClientTicketCount> statusCount(@RequestParam(value = "name", defaultValue = "") String name,
                              @RequestParam(value = "page", defaultValue = "0") int page,
                              @RequestParam(value = "size", defaultValue = "5") int size,
                              @RequestParam(value = "sort", defaultValue = "status") String sort,
                              @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<ClientTicketCount> clientTicketCounts = new ArrayList<>();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                User user = this.userService.findByUsername(authentication.getName());
                for (Long clientId : this.clientService.findAll(user.getTenant(), "pending", size)) {
                    Client clientInfo = this.clientService.findById(clientId);
                    List<StatusCount> statusCounts = new ArrayList<>();
                    for (TicketStatus ticketStatus : this.ticketStatusService.findAll(0, Integer.MAX_VALUE, "id", "asc").getContent()) {
                        StatusCount statusCount = new StatusCount();
                        statusCount.setStatus(ticketStatus.getDetails());
                        statusCount.setCount(this.ticketService.countByClientAndStatus(clientInfo, ticketStatus.getDetails()));
                        statusCounts.add(statusCount);
                    }
                    ClientTicketCount clientTicketCount = new ClientTicketCount();
                    clientTicketCount.setClient(clientInfo);
                    clientTicketCount.setStatusCounts(statusCounts);
                    clientTicketCounts.add(clientTicketCount);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clientTicketCounts;
    }
}
