package com.dvi.ticket.rest;

import com.dvi.ticket.config.tenant.InboundRequest;
import com.dvi.ticket.model.Task;
import com.dvi.ticket.model.TaskThread;
import com.dvi.ticket.model.TaskUser;
import com.dvi.ticket.model.User;
import com.dvi.ticket.service.*;
import com.dvi.ticket.util.DviUtilities;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/tasks")
@Tag(name = "Task APIs", description = "APIs for tasks")
public class TaskController {

    private UserService userService;
	private TaskService taskService;
	private TicketService ticketService;
	private TaskUserService taskUserService;
	private DviUtilities dviUtilities;
	private TaskThreadService taskThreadService;
	private StatusService statusService;

	public TaskController(UserService userService, TaskService taskService, TicketService ticketService, TaskUserService taskUserService,
                          TaskThreadService taskThreadService, DviUtilities dviUtilities, StatusService statusService) {
	    this.userService = userService;
        this.taskService = taskService;
        this.ticketService = ticketService;
        this.taskUserService = taskUserService;
        this.taskThreadService = taskThreadService;
        this.dviUtilities = dviUtilities;
        this.statusService = statusService;
    }

    /**
     * New task.
     *
     * @param taskStr
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    @InboundRequest
    public ResponseEntity<String> addTask(@RequestBody String taskStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        try {
            JSONObject taskJson = new JSONObject(taskStr);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            User user = this.userService.findByUsername(authentication.getName());

            Task task = new Task();
            User agent = new User();

            if (taskJson.has("name")) {
                task.setName(taskJson.getString("name"));
            }
            if (taskJson.has("ticket")) {
                task.setTicket(this.ticketService.findById(taskJson.getJSONObject("ticket").getLong("id")));
            }
            if (taskJson.has("description")) {
                task.setDescription(taskJson.getString("description"));
            }
            if (taskJson.has("agent")) {
                agent = this.userService.findById(taskJson.getJSONObject("agent").getLong("id"));
            }

            Task results = this.taskService.addTask(task);
            if (results != null) {
                TaskUser taskUser = new TaskUser();
                taskUser.setTask(results);
                taskUser.setUser(agent);
                taskUser.setStatus(this.statusService.findByName("Pending"));
                TaskUser taskUserResults = this.taskUserService.addTaskUser(taskUser);
                if (taskUserResults != null) {
                    TaskThread taskThread = new TaskThread();
                    taskThread.setTask(results);
                    taskThread.setMessage("Task created for ticket: '" + results.getTicket().getName() + "' and assigned to " + agent.getFullName());
                    taskThread.setUser(user);
                    this.taskThreadService.update(taskThread);
                    httpStatus = HttpStatus.OK;
                } else {
                    httpStatus = HttpStatus.EXPECTATION_FAILED;
                    this.taskService.remove(results);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return getStringResponseEntity(httpStatus);
    }

    /**
     * List all tasks.
     *
     * @param page
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_AGENT')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    @InboundRequest
    public Page<Task> getTaskList(@RequestParam(value = "name", defaultValue = "") String name,
                                  @RequestParam(value = "page", defaultValue = "0") int page,
                                  @RequestParam(value = "size", defaultValue = "10") int size,
                                  @RequestParam(value = "sort", defaultValue = "id") String sort,
                                  @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<Task> taskList = Collections.emptyList();
        Page<Task> tasks = new PageImpl<>(taskList);
        try {
            tasks = this.taskService.findAll(name, page, size, sort, order);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tasks;
    } 

    /**
     * Update task.
     *
     * @param id
     * @return
     */
    //TODO the one assigned should be able to transfer
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_AGENT')")
    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> editTaskUser(@RequestBody Task task, @PathVariable long id) {
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            this.taskService.update(task);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return dviUtilities.getResponseEntity(httpStatus);
    }

    /**
     * Delete task by its id.
     *
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<String> removeTask(@PathVariable long id) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            Task task = this.taskService.findById(id);
            this.taskService.remove(task);
            response.put("status", "success");
            response.put("message", "Task deleted successfully ....");
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return dviUtilities.getResponseEntity(httpStatus);
    }

    /**
     * get task by its id.
     *
     * @param taskId
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_AGENT')")
    @RequestMapping(value = "/{taskId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @InboundRequest
    public Task getTask(@PathVariable long taskId) {
        return this.taskService.findById(taskId);
    }


    /**
     * New taskUser.
     *
     * @param taskUser
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_AGENT')")
    @RequestMapping(value = "/users/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> addTaskUser(@RequestBody TaskUser taskUser) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        try {
        	Task taskResult = this.taskService.addTask(taskUser.getTask());
        	taskUser.setTask(taskResult);
            TaskUser results = this.taskUserService.addTaskUser(taskUser);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return dviUtilities.getResponseEntity(httpStatus);
    }

    /**
     * Get thread messages
     * 
     * @param httpStatus
     * @return
     */
    @RequestMapping(value = "/{taskId}/messages")
    @InboundRequest
    public Page<TaskThread> messages(@PathVariable long taskId) {
        List<TaskThread> taskThread = Collections.emptyList();
        Page<TaskThread> taskThreads = new PageImpl<>(taskThread);
        try {
            Task task = this.getTask(taskId);
            taskThreads = this.taskThreadService.findAll(task, 0, Integer.MAX_VALUE, "id", "asc");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskThreads;
    }
    
    /**
     * Insert thread message
     * @param httpStatus
     * @return
     */
    @RequestMapping(value = "/{taskId}/messages", method = RequestMethod.POST)
    @InboundRequest
    public ResponseEntity<String> add(@PathVariable long taskId, @Valid @RequestBody String body) {
        JSONObject data = new JSONObject(body);
        try {
            User user = this.userService
                    .findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            Task task = this.getTask(taskId);

            TaskThread _taskThread = new TaskThread();
            _taskThread.setTask(task);
            _taskThread.setUser(user);
            _taskThread.setMessage(data.getString("message"));
            _taskThread = this.taskThreadService.addTaskThread(_taskThread);

            task.setStatus(data.getString("status").toLowerCase());
            this.taskService.update(task);

            if (_taskThread != null) {
                return getStringResponseEntity(HttpStatus.OK);
            } else {
                return getStringResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return getStringResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    /**
     * List all taskUsers.
     *
     * @param page
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_AGENT')")
    @RequestMapping(value = "/users/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<TaskUser> getTaskUserList(@RequestParam(value = "page", defaultValue = "0") int page,
                                        @RequestParam(value = "size", defaultValue = "10") int size,
                                        @RequestParam(value = "sort", defaultValue = "id") String sort,
                                        @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<TaskUser> taskUserList = Collections.emptyList();
        Page<TaskUser> taskUsers = new PageImpl<>(taskUserList);
        try {
            taskUsers = this.taskUserService.findAll(page, size, sort, order);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskUsers;
    }

    /**
     * Update taskUser.
     *
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/users/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> editTaskUser(@RequestBody TaskUser taskUser, @PathVariable long id) {
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            this.taskUserService.update(taskUser);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return dviUtilities.getResponseEntity(httpStatus);
    }

    /**
     * Delete taskUser by its id.
     *
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<String> removeTaskUser(@PathVariable long id) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            TaskUser taskUser = this.taskUserService.findById(id);
            this.taskUserService.remove(taskUser);
            response.put("status", "success");
            response.put("message", "Task deleted successfully");
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.OK;
        }
        return dviUtilities.getResponseEntity(httpStatus);
    }

    /**
     * get taskUser by its id.
     *
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_AGENT')")
    @RequestMapping(value = "/users/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskUser getTaskUser(@PathVariable long id) {
        return this.taskUserService.findById(id);
    }


    /**
     * New taskThread.
     *
     * @param taskThread
     * @return
     */
    //TODO the only one who is assigne to the task, can do more with thread
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_AGENT')")
    @RequestMapping(value = "/threads/", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> addTaskThread(@RequestBody TaskThread taskThread) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        try {
        	TaskThread results = this.taskThreadService.addTaskThread(taskThread);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return dviUtilities.getResponseEntity(httpStatus);
    }

    /**
     * List all taskThreads.
     *
     * @param page
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_AGENT')")
    @RequestMapping(value = "/threads/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Page<TaskThread> getTaskThreadList(@RequestParam(value = "page", defaultValue = "0") int page,
                                        @RequestParam(value = "size", defaultValue = "10") int size,
                                        @RequestParam(value = "sort", defaultValue = "id") String sort,
                                        @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<TaskThread> taskThreadList = Collections.emptyList();
        Page<TaskThread> taskThreads = new PageImpl<>(taskThreadList);
        try {
            taskThreads = this.taskThreadService.findAll(page, size, sort, order);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return taskThreads;
    }

    /**
     * Update taskThread.
     *
     * @param id
     * @return
     */
    //TODO the only one who is assigne to the task, can do more with thread
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_AGENT')")
    @RequestMapping(value = "/threads/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> editTaskThread(@RequestBody TaskThread taskThread, @PathVariable long id) {
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            this.taskThreadService.update(taskThread);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return dviUtilities.getResponseEntity(httpStatus);
    }

    /**
     * Delete taskThread by its id.
     *
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/threads/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody
    ResponseEntity<String> removeTaskThread(@PathVariable long id) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
        	TaskThread taskThread = this.taskThreadService.findById(id);
            this.taskThreadService.remove(taskThread);
            response.put("status", "success");
            response.put("message", "Task deleted successfully");
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.OK;
        }
        return dviUtilities.getResponseEntity(httpStatus);
    }

    /**
     * get taskThread by its id.
     *
     * @param id
     * @return
     */
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_AGENT')")
    @RequestMapping(value = "/threads/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TaskThread getTaskThread(@PathVariable long id) {
        return this.taskThreadService.findById(id);
    }

    private static ResponseEntity<String> getStringResponseEntity(HttpStatus httpStatus) {
        JSONObject response = new JSONObject();
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }
}
