package com.dvi.ticket.rest;

import com.dvi.ticket.config.tenant.InboundRequest;
import com.dvi.ticket.model.*;
import com.dvi.ticket.service.*;
import com.dvi.ticket.util.DviUtilities;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value = "/tickets")
@Tag(name = "Ticket APIs", description = "APIs for tickets")
@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_AGENT') or hasAuthority('ROLE_USER')")
public class TicketController {

    private TicketService ticketService;
    private TicketThreadService ticketThreadService;
    private TicketUserService ticketUserService;
    private TicketStatusService ticketStatusService;
    private UserService userService;
    private TaskService taskService;
    private TopicService topicService;
    private ProjectService projectService;

    public TicketController(TicketService ticketService, TicketThreadService ticketThreadService, TicketUserService ticketUserService, TicketStatusService ticketStatusService,
                            UserService userService, TaskService taskService, TopicService topicService, ProjectService projectService) {
        this.taskService = taskService;
        this.ticketService = ticketService;
        this.ticketThreadService = ticketThreadService;
        this.ticketUserService = ticketUserService;
        this.ticketStatusService = ticketStatusService;
        this.userService = userService;
        this.topicService = topicService;
        this.projectService = projectService;
    }

    /**
     * New ticket.
     *
     * @param ticket
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    @InboundRequest
    public ResponseEntity<String> addTicket(@Valid @RequestBody Ticket ticket) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            User user = this.userService.findByUsername(authentication.getName());

            ticket.setUser(user);

            Ticket results = this.ticketService.addTicket(ticket);
            if (results != null) {

                String number = String.format("%07d", 1);
                Ticket lastAddedTicket = this.ticketService.findByLastAdded(results.getTenant());
                if (lastAddedTicket != null) {
                    int lastNumber = Integer.parseInt(lastAddedTicket.getNumber());
                    number = String.format("%07d", lastNumber + 1);
                }
                results.setNumber(number);
                this.ticketService.update(results);

                if (results.getTopic().getServiceLevelAgreement() != null) {
                    results.setDueDate(Timestamp.valueOf(LocalDateTime.now().plusHours(results.getTopic().getServiceLevelAgreement().getGracePeriod())));
                    this.ticketService.update(ticket);
                } else {
                    results.setDueDate(Timestamp.valueOf(LocalDateTime.now().plusHours(2)));
                    this.ticketService.update(ticket);
                }

                TicketThread ticketThread = new TicketThread();
                ticketThread.setTicket(ticket);
                ticketThread.setMessage("Ticket: '" + ticket.getName() + "' is submitted");
                ticketThread.setUser(user);
                this.ticketThreadService.update(ticketThread);

                TicketUser ticketUser = new TicketUser();
                ticketUser.setTicket(results);
                ticketUser.setUser(user);
                this.ticketUserService.update(ticketUser);

                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return getStringResponseEntity(httpStatus);
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/assign", method = RequestMethod.POST, produces = "application/json")
    @InboundRequest
    public ResponseEntity<String> assignTicket(@RequestBody String ticketStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        try {
            JSONObject ticketJson = new JSONObject(ticketStr);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            User user = this.userService.findByUsername(authentication.getName());

            Ticket ticket = this.ticketService.findById(ticketJson.getLong("ticketId"));
            User agent = this.userService.findById(ticketJson.getLong("agentId"));

            TicketUser ticketUser = new TicketUser();
            ticketUser.setTicket(ticket);
            ticketUser.setUser(agent);
            ticket.setUser(agent);

            TicketUser results = this.ticketUserService.addTicketUser(ticketUser);
            if (results != null) {
                TicketThread ticketThread = new TicketThread();
                ticketThread.setTicket(ticket);
                ticketThread.setMessage("Ticket: '" + ticket.getName() + "' is assigned to " + agent.getFullName());
                ticketThread.setUser(user);
                this.ticketThreadService.update(ticketThread);
                this.ticketService.update(ticket);
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return getStringResponseEntity(httpStatus);
    }

    /**
     * List all tickets.
     *
     * @param page
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @InboundRequest
    public Page<Ticket> getTicketList(@RequestParam(value = "topicId", defaultValue = "0") long topicId,
                                      @RequestParam(value = "projectId", defaultValue = "0") long projectId,
                                      @RequestParam(value = "page", defaultValue = "0") int page,
                                      @RequestParam(value = "size", defaultValue = "10") int size,
                                      @RequestParam(value = "sort", defaultValue = "id") String sort,
                                      @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<Ticket> ticketList = Collections.emptyList();
        Page<Ticket> tickets = new PageImpl<>(ticketList);
        User user = this.userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        try {
            if (DviUtilities.checkAuthority("ROLE_ADMIN") || DviUtilities.checkAuthority("ROLE_SUPER_ADMIN")) {
                if (topicId != 0 && projectId != 0) {
                    Topic topic = this.topicService.findById(topicId);
                    Project project = this.projectService.findById(projectId);
                    tickets = this.ticketService.findAll(topic, project, page, size, sort, order);
                } else if (topicId != 0) {
                    Topic topic = this.topicService.findById(topicId);
                    tickets = this.ticketService.findAll(topic, page, size, sort, order);
                } else if (projectId != 0) {
                    Project project = this.projectService.findById(projectId);
                    tickets = this.ticketService.findAll(project, page, size, sort, order);
                } else {
                    tickets = this.ticketService.findAll(page, size, sort, order);
                }
            } else if (DviUtilities.checkAuthority("ROLE_AGENT") || DviUtilities.checkAuthority("ROLE_USER")) {
                if (topicId != 0 && projectId != 0) {
                    Topic topic = this.topicService.findById(topicId);
                    Project project = this.projectService.findById(projectId);
                    tickets = this.ticketService.findAll(user, topic, project, page, size, sort, order);
                } else if (topicId != 0) {
                    Topic topic = this.topicService.findById(topicId);
                    tickets = this.ticketService.findAll(user, topic, page, size, sort, order);
                } else if (projectId != 0) {
                    Project project = this.projectService.findById(projectId);
                    tickets = this.ticketService.findAll(user, project, page, size, sort, order);
                } else {
                    tickets = this.ticketService.findAll(user, page, size, sort, order);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tickets;
    }

    @RequestMapping(value = "/statuses", method = RequestMethod.GET)
    public Page<TicketStatus> getTicketStatuses(@RequestParam(value = "page", defaultValue = "0") int page,
                                                @RequestParam(value = "size", defaultValue = "10") int size,
                                                @RequestParam(value = "sort", defaultValue = "id") String sort,
                                                @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<TicketStatus> ticketStatusList = Collections.emptyList();
        Page<TicketStatus> ticketStatuses = new PageImpl<>(ticketStatusList);
        try {
            ticketStatuses = this.ticketStatusService.findAll(page, size, sort, order);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ticketStatuses;
    }

    /**
     * Update ticket.
     *
     * @param ticket
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.PATCH, produces = "application/json")
    @InboundRequest
    public ResponseEntity<String> editTicket(@RequestBody Ticket ticket) {
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            this.ticketService.update(ticket);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return getStringResponseEntity(httpStatus);
    }

    /**
     * Delete ticket by its id.
     *
     * @param ticketId
     * @return
     */
    @RequestMapping(value = "/{ticketId}", method = RequestMethod.DELETE)
    @InboundRequest
    public @ResponseBody ResponseEntity<String> removeTicket(@PathVariable long ticketId) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            Ticket ticket = this.ticketService.findById(ticketId);
            this.ticketService.remove(ticket);
            response.put("status", "success");
            response.put("message", "Ticket deleted successfully");
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    /**
     * get ticket by its id.
     *
     * @param ticketId
     * @return
     */
    @RequestMapping(value = "/{ticketId}", method = RequestMethod.GET, produces = "application/json")
    @InboundRequest
    public Ticket getTicket(@PathVariable long ticketId) {
        return this.ticketService.findById(ticketId);
    }

    /**
     * get ticket by its id.
     *
     * @param ticketId
     * @return
     */
    @RequestMapping(value = "/{ticketId}/tasks", method = RequestMethod.GET, produces = "application/json")
    @InboundRequest
    public Page<Task> getTasks(@PathVariable long ticketId) {
        List<Task> tasks = Collections.emptyList();
        Page<Task> _tasks = new PageImpl<>(tasks);
        try {
            Ticket ticket = this.getTicket(ticketId);
            _tasks = this.taskService.findAll(ticket, 0, Integer.MAX_VALUE, "id", "asc");
        } catch (Exception e) {
            e.printStackTrace(); 
        } 
        return _tasks;
    }

    /**
     * Get thread messages
     * 
     * @param ticketId
     * @return
     */
    @RequestMapping(value = "/{ticketId}/messages")
    @InboundRequest
    public Page<TicketThread> messages(@PathVariable long ticketId) {
        List<TicketThread> ticketThread = Collections.emptyList();
        Page<TicketThread> ticketThreads = new PageImpl<>(ticketThread);
        try {
            Ticket ticket = this.getTicket(ticketId);
            ticketThreads = this.ticketThreadService.findAll(ticket, 0, Integer.MAX_VALUE, "id", "asc");
        } catch (Exception e) {
            e.printStackTrace(); 
        }
        return ticketThreads;
    }

    /**
     * Insert thread message
     * @param body
     * @return
     */
    @RequestMapping(value = "/{ticketId}/messages", method = RequestMethod.POST)
    @InboundRequest
    public ResponseEntity<String> add(@PathVariable long ticketId, @Valid @RequestBody String body) {
        JSONObject data = new JSONObject(body);
        try {
            User user = this.userService
                    .findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            Ticket ticket = this.getTicket(ticketId);

            TicketThread _ticketThread = new TicketThread();
            _ticketThread.setTicket(ticket);
            _ticketThread.setUser(user);
            _ticketThread.setMessage(data.getString("message"));
            _ticketThread = this.ticketThreadService.addTicketThread(_ticketThread);

            ticket.setStatus(data.getString("status").toLowerCase());
            this.ticketService.update(ticket);

            if(_ticketThread != null){
                return getStringResponseEntity(HttpStatus.OK);
            } else {
                return getStringResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (Exception e) {
            e.printStackTrace();
                return getStringResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    private static ResponseEntity<String> getStringResponseEntity(HttpStatus httpStatus) {
        JSONObject response = new JSONObject();
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

}