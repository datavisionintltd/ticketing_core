package com.dvi.ticket.rest;
import com.dvi.ticket.model.DictionaryItem;
import com.dvi.ticket.model.settings.SystemSettings;
import com.dvi.ticket.service.DictionaryItemService;
import com.dvi.ticket.service.SystemSettingsService;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.transaction.annotation.Transactional;

@RestController
@Transactional
@RequestMapping(value = "/settings/systems")
@PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
public class SystemSettingsController {
    private SystemSettingsService systemSettingsService;
    private DictionaryItemService dictionaryItemService;

    public SystemSettingsController( SystemSettingsService systemSettingsService, DictionaryItemService dictionaryItemService){
        this.systemSettingsService = systemSettingsService;
        this.dictionaryItemService = dictionaryItemService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public SystemSettings getTicketSettings() {

        return this.systemSettingsService.findTopByOrderByIdDesc();
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addSystemSettings(@RequestBody String systemSetting) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject ticketSettingJson = new JSONObject(systemSetting);
            SystemSettings systemSet =new SystemSettings();
            long language=ticketSettingJson.getLong("language");
            long defaultTimeZone=ticketSettingJson.getLong("defaultTimeZone");
            long defaultPageSize=ticketSettingJson.getLong("defaultPageSize");
            String fileSize=ticketSettingJson.getString("fileSize");
            DictionaryItem languageItem=dictionaryItemService.findById(language);
            DictionaryItem defaultTimeZoneItem=dictionaryItemService.findById(defaultTimeZone);
            DictionaryItem defaultPageSizeItem=dictionaryItemService.findById(defaultPageSize);

            systemSet.setLanguage(languageItem);
            systemSet.setDefaultTimeZone(defaultTimeZoneItem);
            systemSet.setDefaultPageSize(defaultPageSizeItem);
            systemSet.setFileSize(fileSize);
            SystemSettings results = this.systemSettingsService.save(systemSet);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> updateSystemSettings(@RequestBody String systemSetting) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {

            JSONObject ticketSettingJson = new JSONObject(systemSetting);
            long settingId=ticketSettingJson.getLong("id");
            System.out.println(settingId);
            SystemSettings systemSet =this.systemSettingsService.findById(settingId);
            System.out.println(systemSet.getId());
            String fileSize=ticketSettingJson.getString("fileSize");
            long defaultTimeZone=ticketSettingJson.getLong("defaultTimeZone");
            long language=ticketSettingJson.getLong("language");

            long defaultPageSize=ticketSettingJson.getLong("defaultPageSize");

            DictionaryItem defaultPageSizeItem=dictionaryItemService.findById(defaultPageSize);
            System.out.println(defaultPageSizeItem.getCode());
            DictionaryItem languageItem=dictionaryItemService.findById(language);

            DictionaryItem defaultTimeZoneItem=dictionaryItemService.findById(defaultTimeZone);

            System.out.println(defaultTimeZoneItem.getCode());
            systemSet.setDefaultTimeZone(defaultTimeZoneItem);
            systemSet.setLanguage(languageItem);

            systemSet.setDefaultPageSize(defaultPageSizeItem);
            systemSet.setFileSize(fileSize);
            SystemSettings results = this.systemSettingsService.save(systemSet);
            httpStatus = HttpStatus.OK;

        } catch (Exception e) {
            System.out.println("error"+e);
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }
}
