package com.dvi.ticket.rest;

import com.dvi.ticket.model.ServiceLevelAgreement;
import com.dvi.ticket.service.ServiceLevelAgreementService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/slas")
@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
public class ServiceLevelAgreementController {

    private ServiceLevelAgreementService serviceLevelAgreementService;

    public ServiceLevelAgreementController(ServiceLevelAgreementService serviceLevelAgreementService) {
        this.serviceLevelAgreementService = serviceLevelAgreementService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<ServiceLevelAgreement> getAllSLAs(@RequestParam(value = "page", defaultValue = "0") int page,
                                                  @RequestParam(value = "size", defaultValue = "10") int size,
                                                  @RequestParam(value = "sort", defaultValue = "id") String sort,
                                                  @RequestParam(value = "direction", defaultValue = "desc") String order) {
        Page<ServiceLevelAgreement> sla = null;
        try {
                sla = this.serviceLevelAgreementService.findAll(page, size, sort, order);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return sla;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addSLA(@RequestBody ServiceLevelAgreement sla) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            ServiceLevelAgreement results = this.serviceLevelAgreementService.addServiceLevelAgreement(sla);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addSLAs(@RequestBody String slaStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            List<ServiceLevelAgreement> slas = new ArrayList<>();
            JSONArray slaArray = new JSONArray(slaStr);
            for (int i = 0; i < slaArray.length(); i++) {
                JSONObject salJson = slaArray.getJSONObject(i);
                ServiceLevelAgreement sla = new ServiceLevelAgreement();
                sla.setName(salJson.getString("name"));
                sla.setGracePeriod(salJson.getInt("gracePeriod"));
                sla.setDescription(salJson.getString("description"));
                slas.add(sla);
            }

            if (!slas.isEmpty()) {
                slas = this.serviceLevelAgreementService.addServiceLevelAgreement(slas);
            }
            if (slas != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> updateSLA(@RequestBody ServiceLevelAgreement sla) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
             this.serviceLevelAgreementService.update(sla);
             httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/{slaId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeSLA(@PathVariable long slaId) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            ServiceLevelAgreement sla = this.serviceLevelAgreementService.findById(slaId);
            this.serviceLevelAgreementService.remove(sla);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }
}
