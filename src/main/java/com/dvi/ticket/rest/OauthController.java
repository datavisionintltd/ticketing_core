package com.dvi.ticket.rest;

import com.dvi.ticket.config.security.SecurityConfig;
import com.dvi.ticket.model.*;
import com.dvi.ticket.repository.*;
import com.dvi.ticket.repository.config.SysConfigRepository;
import com.dvi.ticket.resource.GalaxyResource;
import com.dvi.ticket.service.*;
import com.dvi.ticket.util.*;
import org.apache.commons.codec.binary.Base64;
import org.json.JSONObject;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.*;

@RestController
@RequestMapping(value = "/auth")
@Transactional
public class OauthController {

    @Resource(name = "tokenServices")
    private ConsumerTokenServices tokenServices;
    private MessageSource messageSource;
    private UserRepository userRepository;
    private RoleRepository roleRepository;
    private UserRoleRepository userRoleRepository;
    private DviUtilities dviUtilities;
    private SmsUtilities smsUtilities;
    private EmailUtilities emailUtilities;
    private EmailModule emailModule;
    private VerificationCodeService verificationCodeService;
    private SmsLogRepository smsLogRepository;
    private TenantRepository tenantRepository;
    private SysConfigRepository sysConfigRepository;
    private CountryService countryService;
    private UserGroupService userGroupService;
    private SmsBalanceRepository smsBalanceRepository;
    private RemoteUtilities remoteUtilities;
    private SecurityConfig securityConfig;
    private GalaxyResource galaxyResource;

    public OauthController(MessageSource messageSource, UserRepository userRepository, RoleRepository roleRepository, UserRoleRepository userRoleRepository,
                           DviUtilities dviUtilities, SmsUtilities smsUtilities, EmailUtilities emailUtilities, EmailModule emailModule, SmsLogRepository smsLogRepository,
                           VerificationCodeService verificationCodeService, TenantRepository tenantRepository, SysConfigRepository sysConfigRepository,
                           CountryService countryService, UserGroupService userGroupService, SmsBalanceRepository smsBalanceRepository, RemoteUtilities remoteUtilities,
                           SecurityConfig securityConfig, GalaxyResource galaxyResource) {
        this.messageSource = messageSource;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userRoleRepository = userRoleRepository;
        this.dviUtilities = dviUtilities;
        this.smsUtilities = smsUtilities;
        this.emailUtilities = emailUtilities;
        this.emailModule = emailModule;
        this.smsLogRepository = smsLogRepository;
        this.verificationCodeService = verificationCodeService;
        this.tenantRepository = tenantRepository;
        this.sysConfigRepository = sysConfigRepository;
        this.countryService = countryService;
        this.userGroupService = userGroupService;
        this.smsBalanceRepository = smsBalanceRepository;
        this.remoteUtilities = remoteUtilities;
        this.securityConfig = securityConfig;
        this.galaxyResource = galaxyResource;
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    /*@ResponseStatus(HttpStatus.OK)*/
    public @ResponseBody ResponseEntity<String> logout(HttpServletRequest request) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject responseJson = new JSONObject();
        Locale locale = LocaleContextHolder.getLocale();
        Task task = new Task();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                String authHeader = request.getHeader("Authorization");
                if (authHeader != null) {
                    String tokenValue = authHeader.replace("Bearer", "").trim();
                    this.tokenServices.revokeToken(tokenValue);
                }
                responseJson.put("status", this.messageSource.getMessage("status.success", null, locale));
                responseJson.put("message", this.messageSource.getMessage("status.success.message.logout", null, locale));
            } else {
                httpStatus = HttpStatus.UNAUTHORIZED;
                responseJson.put("status", this.messageSource.getMessage("status.fail", null, locale));
                responseJson.put("message", this.messageSource.getMessage("status.fail.message.logout", null, locale));
            }
        } catch (Exception e) {
            httpStatus = HttpStatus.BAD_REQUEST;
            responseJson.put("status", this.messageSource.getMessage("status.error", null, locale));
            responseJson.put("message", this.messageSource.getMessage("status.error.message", null, locale));
            e.printStackTrace();
        }
        return new ResponseEntity<>(responseJson.toString(), httpStatus);
    }

    @RequestMapping(value = "/token", method = RequestMethod.GET)
    public ModelAndView galaxyToken(@RequestParam(value = "sso_token") String sso_token) {
        String ticketToken = "none";
        String redirectUrl = "https://maxdesk.softwaregalaxy.co.tz/#/galaxy/";
        try {
            redirectUrl = this.sysConfigRepository.findByConfigName("redirect_url").getConfigValue();
            String clientSecret = this.sysConfigRepository.findByConfigName("galaxy_secret").getConfigValue();
            String galaxyUser = this.galaxyResource.galaxyUser(sso_token, clientSecret);
            JSONObject userInfo = new JSONObject(galaxyUser);
            String username = userInfo.getJSONObject("payload").getString("username");
            User user = this.userRepository.findByUsername(username);

            if (user == null) {
                user = this.galaxyResource.registerUser(userInfo.getJSONObject("payload").toString());
            }

            OAuth2AccessToken accessToken = this.securityConfig.getAccessToken(user);
            ticketToken = accessToken.getValue();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("redirect:" + redirectUrl + ticketToken);
    }

    @RequestMapping(value = "/sign/up", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ResponseEntity<String> signingUp(@RequestBody String user_json) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject userResponse = new JSONObject();
        try {
            JSONObject userJson = new JSONObject(user_json);
            User userData = new User();
            long tenantId = 0;
            Tenant tenantInfo = new Tenant();
            if (userJson.has("uuid")) {
                tenantInfo = this.tenantRepository.findByUuid(userJson.getString("uuid"));
                tenantId = tenantInfo.getId();
            }

            if (!userJson.has("fullName")) {
                userResponse.put("status", "Incomplete");
                userResponse.put("message", "Full name must not be empty.");
                httpStatus = HttpStatus.NO_CONTENT;
            } else if (!userJson.has("phoneNumber")) {
                userResponse.put("status", "Incomplete");
                userResponse.put("message", "Phone number must not be empty.");
                httpStatus = HttpStatus.NO_CONTENT;
            } else if (!userJson.has("username")) {
                userResponse.put("status", "Incomplete");
                userResponse.put("message", "Username must not be empty.");
                httpStatus = HttpStatus.NO_CONTENT;
            } else if (!userJson.has("password")) {
                userResponse.put("status", "Incomplete");
                userResponse.put("message", "Password must not be empty.");
                httpStatus = HttpStatus.NO_CONTENT;
            } else if (tenantInfo == null) {
                userResponse.put("status", "Incomplete");
                userResponse.put("message", "Client must not be null.");
                httpStatus = HttpStatus.NO_CONTENT;
            } else if (this.userRepository.findByUsernameOrEmail(userJson.getString("username"), userJson.getString("username")) != null) {
                userResponse.put("status", "duplicate");
                userResponse.put("message", "This Username is already taken, please use another email");
                httpStatus = HttpStatus.CONFLICT;
            } else {
                UserGroup userGroup = this.userGroupService.findByName("CUSTOMERS");
                String encodedPassword = new BCryptPasswordEncoder().encode(userJson.getString("password"));
                userData.setUsername(userJson.getString("username"));
                userData.setFullName(userJson.getString("fullName"));
                userData.setEmail(userJson.getString("username"));
                userData.setPhoneNumber(this.dviUtilities.formatPhoneNumber("+255", userJson.getString("phoneNumber")));
                userData.setPassword(encodedPassword);
                userData.setTenant(tenantInfo);
                userData.setUserGroup(userGroup);
                User userResults = this.userRepository.save(userData);

                Role roleInfo = this.roleRepository.findByName("ROLE_USER");
                UserRole userRole = this.userRoleRepository.save(new UserRole(userResults, roleInfo));
                if (userRole != null) {
                    VerificationCode verificationCode = new VerificationCode();
                    verificationCode.setUser(userResults);
                    verificationCode.setCurrentCode(this.dviUtilities.getRandomNumber());
                    VerificationCode verificationCodeResults = this.verificationCodeService.newVerificationCode(verificationCode);
                    String verCode = verificationCodeResults.getCurrentCode();
                    /*if (userResults.getEmail() != null) {
                        JSONObject mandrillResponse = new JSONObject(this.emailModule.verificationEmail(userResults.getEmail(), verCode, userResults.getFullName()));
                        if (!mandrillResponse.getString("Status").equalsIgnoreCase("sent")) {
                            this.emailUtilities.verificationEmail(userResults.getEmail(), verCode);
                        }
                    }*/
                    EmailThreads emailThreads = new EmailThreads("Send sign up Email", this.emailModule, userResults.getEmail(), verCode, userResults.getFullName());
                    emailThreads.start();
                    if (userResults.getPhoneNumber() != null) {
                        if (userResults.getTenant() == null) {
                            this.userRepository.updateUser(userResults.getId(), tenantId);
                        }
                        SmsThreads smsThreads = new SmsThreads("Send sign up Sms", this.smsUtilities, this.smsLogRepository, userResults.getPhoneNumber(), verCode);
                        smsThreads.start();
                    }
                    userResponse.put("status", "success");
                    userResponse.put("message", "Account created successfully, Visit your email or message to activate");
                } else {
                    this.userRepository.delete(userResults);
                    userResponse.put("status", "failed");
                    userResponse.put("message", "Failed to create account");
                    httpStatus = HttpStatus.EXPECTATION_FAILED;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            userResponse.put("status", "error");
            userResponse.put("message", "Error occurred, Please try again");
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return new ResponseEntity<>(userResponse.toString(), httpStatus);
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addTenant(@RequestBody String tenantStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject tenantJson = new JSONObject(tenantStr);
            if (!tenantJson.has("name")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Company name cannot be empty");
            } else if (!tenantJson.has("phoneNumber")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Phone number cannot be empty");
            } else if (!tenantJson.has("fullName")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Full name cannot be empty");
            } else if (!tenantJson.has("email")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Email cannot be empty");
            } else if (this.userRepository.findByUsername(tenantJson.getString("email")) != null) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "This Username is already taken, please use another email");
            } else {
                Tenant tenant = new Tenant();
                tenant.setName(tenantJson.getString("name"));
                tenant.setPhone(this.dviUtilities.formatPhoneNumber("255", tenantJson.getString("phoneNumber")));
                tenant.setEmail(tenantJson.getString("email"));
                tenant.setCountry(this.countryService.findByDialCode(tenantJson.getString("dialCode")));

                String tillSuffix = String.format("%04d", 1);
                Tenant lastAddedTenant = this.tenantRepository.findByLastAdded();
                if (lastAddedTenant != null) {
                    int lastSuffix = Integer.parseInt(lastAddedTenant.getTillSuffix());
                    tillSuffix = String.format("%04d", lastSuffix + 1);
                }
                tenant.setTillSuffix(tillSuffix);
                String tillNumber = this.sysConfigRepository.findByConfigName("tillPrefix").getConfigValue() + tillSuffix;
                tenant.setTillNumber(tillNumber);
                Tenant results = this.tenantRepository.save(tenant);

                if (results != null) {

                    String encodedPassword = new BCryptPasswordEncoder().encode("123456");
                    List<Role> userRoles = Arrays.asList(this.roleRepository.findByName("ROLE_USER"), this.roleRepository.findByName("ROLE_ADMIN"));
                    User user = new User(tenantJson.getString("email"), encodedPassword, userRoles);
                    user.setFullName(tenantJson.getString("fullName"));
                    user.setEmail(tenantJson.getString("email"));
                    user.setPhoneNumber(this.dviUtilities.formatPhoneNumber("+255", tenantJson.getString("phoneNumber")));
                    user.setEnabled(false);
                    user.setTenant(results);

                    User userResult = userRepository.save(user);

                    if (userResult != null) {
                        if (userResult.getTenant() == null) {
                            this.userRepository.updateUser(userResult.getId(), results.getId());
                        }
                        SmsBalance smsBalance = new SmsBalance();
                        smsBalance.setBalanceSms(100);
                        smsBalance.setBalanceAmt(4000);
                        smsBalance.setTenant(results);
                        this.smsBalanceRepository.save(smsBalance);

                        VerificationCode verificationCode = new VerificationCode();
                        verificationCode.setUser(userResult);
                        verificationCode.setCurrentCode(this.dviUtilities.getRandomNumber());
                        VerificationCode verificationCodeResults = this.verificationCodeService.newVerificationCode(verificationCode);
                        String verCode = verificationCodeResults.getCurrentCode();
                        /*if (userResult.getEmail() != null) {
                            JSONObject mandrillResponse = new JSONObject(this.emailModule.verificationEmail(userResult.getEmail(), verCode, userResult.getFullName()));
                            if (!mandrillResponse.getString("Status").equalsIgnoreCase("sent")) {
                                this.emailUtilities.verificationEmail(userResult.getEmail(), verCode);
                            }
                        }*/
                        EmailThreads emailThreads = new EmailThreads("Send register Email", this.emailModule, userResult.getEmail(), verCode, userResult.getFullName());
                        emailThreads.start();
                        SmsThreads smsThreads = new SmsThreads("Send register Sms", this.smsUtilities, this.smsLogRepository, userResult.getPhoneNumber(), verCode);
                        smsThreads.start();

                        /*SmsLog smsLog = new SmsLog();
                        List<String> receivers_list = new ArrayList<String>();
                        receivers_list.add(userResult.getPhoneNumber());
                        smsLog.setSender("INFO");
                        smsLog.setPush("your delivery link");
                        smsLog.setStatus("Committed");
                        smsLog.setMessage("Your Max Desk Account is ready. To activate this account, enter this verification code M-" + verCode);
                        smsLog.setRecipient(receivers_list.toString());
                        SmsLog sms_results = this.smsLogRepository.save(smsLog);

                        JSONObject sent_response = new JSONObject(smsUtilities.sendSms(sms_results));
                        if (sent_response.getString("status").equalsIgnoreCase("succeed")) {
                            sms_results.setStatus("SUCCESSFUL");
                        } else {
                            sms_results.setStatus(sent_response.getString("status"));
                        }
                        this.smsLogRepository.save(sms_results);*/

                        httpStatus = HttpStatus.OK;
                        OAuth2AccessToken accessToken = this.securityConfig.getAccessToken(userResult);
                        response.put("access_token", accessToken.getValue());
                        response.put("status", "success");
                        response.put("message", "Company added successfully");
                    } else {
                        this.tenantRepository.delete(results);
                        httpStatus = HttpStatus.EXPECTATION_FAILED;
                        response.put("status", "failed");
                        response.put("message", "Failed to add a user");
                    }
                } else {
                    httpStatus = HttpStatus.EXPECTATION_FAILED;
                    response.put("status", "failed");
                    response.put("message", "Failed to add a company");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/activate", method = RequestMethod.POST)
    public @ResponseBody ResponseEntity<String> activateUserAccount(@RequestBody String code_json) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject userResponse = new JSONObject();
        try {
            JSONObject codeJson = new JSONObject(code_json);
            if (!codeJson.has("code")) {
                userResponse.put("status", "Incomplete");
                userResponse.put("message", "Activation code must not be empty.");
                httpStatus = HttpStatus.NO_CONTENT;
                return new ResponseEntity<>(userResponse.toString(), httpStatus);
            } else {
                VerificationCode verificationCode = this.verificationCodeService.findByCurrentCode(codeJson.getString("code").replace("M-", ""));
                if (verificationCode == null) {
                    userResponse.put("message", "Incorrect verification code");
                    userResponse.put("status", "invalid");
                    httpStatus = HttpStatus.NOT_ACCEPTABLE;
                } else {
                    User user = verificationCode.getUser();
                    long tenantId = user.getTenant().getId();
                    user.setEnabled(true);
                    User userResults = this.userRepository.save(user);

                    if (userResults.isEnabled()) {
                        this.userRepository.updateUser(userResults.getId(), tenantId);
                        this.verificationCodeService.remove(verificationCode);
                        userResponse.put("message", "Account activated successfully");
                        userResponse.put("status", "success");
                    } else {
                        userResponse.put("message", "Failed to activate account");
                        userResponse.put("status", "fail");
                        httpStatus = HttpStatus.EXPECTATION_FAILED;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            userResponse.put("status", "error");
            userResponse.put("message", "Error occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(userResponse.toString(), httpStatus);
    }

    @RequestMapping(value = "/resend/activation/code", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ResponseEntity<String> resendActivationCode(@RequestBody String user_json) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject userResponse = new JSONObject();
        try {
            JSONObject userJson = new JSONObject(user_json);

            if (!userJson.has("email")) {
                userResponse.put("status", "Incomplete");
                userResponse.put("message", "Email must not be empty.");
                httpStatus = HttpStatus.NO_CONTENT;
            } else if (this.userRepository.findByEmail(userJson.getString("email")) == null) {
                userResponse.put("status", "Incomplete");
                userResponse.put("message", "You are not registered, Please register.");
                httpStatus = HttpStatus.NO_CONTENT;
            } else {
                User userInfo = this.userRepository.findByEmail(userJson.getString("email"));
                VerificationCode verificationCode = new VerificationCode();
                verificationCode.setUser(userInfo);
                verificationCode.setCurrentCode(this.dviUtilities.getRandomNumber());
                VerificationCode verCodeResults = this.verificationCodeService.newVerificationCode(verificationCode);

                /*if (userInfo.getEmail() != null) {
                    JSONObject mandrillResponse = new JSONObject(this.emailModule.resendActivationCode(userInfo.getEmail(), verCodeResults.getCurrentCode(), userInfo.getFullName()));
                    if (!mandrillResponse.getString("Status").equalsIgnoreCase("sent")) {
                        this.emailUtilities.resendActivationCode(userInfo.getEmail(), verCodeResults.getCurrentCode());
                    }
                }
                if (userInfo.getPhoneNumber() != null) {
                    SmsLog smsLog = new SmsLog();
                    List<String> receivers_list = new ArrayList<String>();
                    receivers_list.add(userInfo.getPhoneNumber());
                    smsLog.setSender("INFO");
                    smsLog.setPush("your delivery link");
                    smsLog.setStatus("Committed");
                    smsLog.setMessage("To activate your cibo account, enter this verification code B-" + verCodeResults.getCurrentCode());
                    smsLog.setRecipient(receivers_list.toString());
                    SmsLog sms_results = this.smsLogRepository.save(smsLog);

                    JSONObject sent_response = new JSONObject(smsUtilities.sendSms(sms_results));
                    if (sent_response.getString("status").equalsIgnoreCase("succeed")) {
                        sms_results.setStatus("SUCCESSFUL");
                    } else {
                        sms_results.setStatus(sent_response.getString("status"));
                    }
                    this.smsLogRepository.save(sms_results);
                }*/

                EmailThreads emailThreads = new EmailThreads("Send Reset Password Email", this.emailModule, userInfo.getEmail(), verCodeResults.getCurrentCode(), userInfo.getFullName());
                emailThreads.start();
                SmsThreads smsThreads = new SmsThreads("Send Reset Password Sms", this.smsUtilities, this.smsLogRepository, userInfo.getPhoneNumber(), verCodeResults.getCurrentCode());
                smsThreads.start();

                userResponse.put("status", "success");
                userResponse.put("message", "Activation code sent successfully, Visit your email or messages to activate");
            }
        } catch (Exception e) {
            e.printStackTrace();
            userResponse.put("status", "error");
            userResponse.put("message", "Error occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(userResponse.toString(), httpStatus);
    }

    @RequestMapping(value = "/details", method = RequestMethod.GET)
    public @ResponseBody User getUser() {
        User userInfo = new User();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                /*userInfo = this.userService.findByUsername(authentication.getName());*/
                userInfo = this.userRepository.findByUsername(authentication.getName());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfo;
    }

    @RequestMapping(value = "/password/reset/request", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ResponseEntity<String> passwordResetRequest(@RequestBody String user_json) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject userResponse = new JSONObject();
        try {
            JSONObject userJson = new JSONObject(user_json);

            if (!userJson.has("email")) {
                userResponse.put("status", "Incomplete");
                userResponse.put("message", "Email must not be empty.");
                httpStatus = HttpStatus.NO_CONTENT;
                return new ResponseEntity<>(userResponse.toString(), httpStatus);
            } else if (this.userRepository.findByEmail(userJson.getString("email")) == null) {
                userResponse.put("status", "Incomplete");
                userResponse.put("message", "You are not registered, Please register.");
                httpStatus = HttpStatus.NO_CONTENT;
                return new ResponseEntity<>(userResponse.toString(), httpStatus);
            } else {
                User userInfo = this.userRepository.findByEmail(userJson.getString("email"));

                VerificationCode verificationCode = new VerificationCode();
                verificationCode.setUser(userInfo);
                verificationCode.setCurrentCode(this.dviUtilities.getRandomNumber());
                VerificationCode verCodeResults = this.verificationCodeService.newVerificationCode(verificationCode);

                /*if (userInfo.getPhoneNumber() != null) {
                    SmsLog smsLog = new SmsLog();
                    List<String> receivers_list = new ArrayList<String>();
                    receivers_list.add(userInfo.getPhoneNumber());
                    smsLog.setSender("INFO");
                    smsLog.setStatus("Committed");
                    smsLog.setPush("your delivery link");
                    smsLog.setMessage("To reset your password on Max Desk, enter this verification code M-" + verCodeResults.getCurrentCode());
                    smsLog.setRecipient(receivers_list.toString());
                    SmsLog sms_results = this.smsLogRepository.save(smsLog);

                    JSONObject sent_response = new JSONObject(smsUtilities.sendTenantSms(sms_results, userInfo.getTenant()));
                    if (sent_response.getString("status").equalsIgnoreCase("succeed")) {
                        sms_results.setStatus("SUCCESSFUL");
                    } else {
                        sms_results.setStatus(sent_response.getString("status").toUpperCase());
                    }
                    this.smsLogRepository.save(sms_results);
                }

                if (userInfo.getEmail() != null) {
                    JSONObject mandrillResponse = new JSONObject(this.emailModule.resetPasswordEmail(userInfo.getEmail(), userInfo.getFullName(), verCodeResults.getCurrentCode()));
                    if (!mandrillResponse.getString("Status").equalsIgnoreCase("sent")) {
                        this.emailUtilities.resetPasswordEmail(userInfo.getEmail(), userInfo.getFullName(), verCodeResults.getCurrentCode());
                    }
                }*/

                EmailThreads emailThreads = new EmailThreads("Send Reset Password Email", this.emailModule, userInfo.getEmail(), verCodeResults.getCurrentCode(), userInfo.getFullName());
                emailThreads.start();
                SmsThreads smsThreads = new SmsThreads("Send Reset Password Sms", this.smsUtilities, this.smsLogRepository, userInfo.getPhoneNumber(), verCodeResults.getCurrentCode());
                smsThreads.start();

                userResponse.put("status", "success");
                userResponse.put("message", "Visit your email to rest password");
            }
        } catch (Exception e) {
            e.printStackTrace();
            userResponse.put("status", "error");
            userResponse.put("message", "Error occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(userResponse.toString(), httpStatus);
    }

    @RequestMapping(value = "/reset/password", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ResponseEntity<String> resetPassword(@RequestBody String passwordStr) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            JSONObject passwordJson = new JSONObject(passwordStr);
            String password = passwordJson.getString("password");
            VerificationCode verificationCode = this.verificationCodeService.findByCurrentCode(passwordJson.getString("code").replace("M-", ""));
            User user = verificationCode.getUser();
            if (password.equalsIgnoreCase(passwordJson.getString("confirm")) && user != null) {
                user.setPassword(new BCryptPasswordEncoder().encode(password));
                user.setPasswordProtected(true);
                this.userRepository.save(user);
                response.put("status", "success");
                response.put("message", "User Password updated successfully"); // zaburi 1: yeremia 30:17
                for (VerificationCode code : this.verificationCodeService.findAllByUser(user)) {
                    this.verificationCodeService.remove(code);
                }
            } else {
                response.put("status", "fail");
                response.put("message", "Failed To update Password, Please try again");
                httpStatus = HttpStatus.EXPECTATION_FAILED;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/countries", method = RequestMethod.GET)
    public Page<Country> getCountries(@RequestParam(value = "name", defaultValue = "") String name,
                                      @RequestParam(value = "page", defaultValue = "0") int page,
                                      @RequestParam(value = "size", defaultValue = "10") int size,
                                      @RequestParam(value = "sort", defaultValue = "id") String sort,
                                      @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<Country> countryList = Collections.emptyList();
        Page<Country> countries = new PageImpl<>(countryList);
        try {
            countries = this.countryService.findAll(name, page, size, sort, order);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return countries;
    }

    @RequestMapping(value = "/fetch/countries", method = RequestMethod.GET)
    public String fetchCountries() {
        try {
            this.remoteUtilities.getCountries("http://country.io/names.json", "names");
            this.remoteUtilities.getCountries("http://country.io/phone.json", "codes");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "Done";
    }
}
