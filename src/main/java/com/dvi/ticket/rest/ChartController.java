package com.dvi.ticket.rest;

import com.dvi.ticket.model.TicketStatus;
import com.dvi.ticket.model.User;
import com.dvi.ticket.service.TicketService;
import com.dvi.ticket.service.TicketStatusService;
import com.dvi.ticket.service.UserService;
import org.json.JSONObject;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/charts")
public class ChartController {

    private UserService userService;
    private TicketStatusService ticketStatusService;
    private TicketService ticketService;

    public ChartController(UserService userService, TicketStatusService ticketStatusService, TicketService ticketService) {
        this.userService = userService;
        this.ticketStatusService = ticketStatusService;
        this.ticketService = ticketService;
    }

    @RequestMapping(value = "/status", method = RequestMethod.GET, produces = "application/json")
    public String statusChart(@RequestParam(value = "since") String since) {
        JSONObject statusJson = new JSONObject();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                User userInfo = this.userService.findByUsername(authentication.getName());
                List<String> statuses = new ArrayList<>();
                List<Long> counts = new ArrayList<>();
                for (TicketStatus ticketStatus : this.ticketStatusService.findAll(0, Integer.MAX_VALUE, "id", "asc").getContent()) {
                    statuses.add(ticketStatus.getDetails());
                    counts.add(this.ticketService.countByStatusAndCreatedAt(userInfo.getTenant(), ticketStatus.getDetails(), since));
                }
                statusJson.put("statuses", statuses);
                statusJson.put("counts", counts);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return statusJson.toString();
    }
}
