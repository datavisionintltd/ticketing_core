package com.dvi.ticket.rest;

import com.dvi.ticket.config.tenant.InboundRequest;
import com.dvi.ticket.dao.ProjectTicketCount;
import com.dvi.ticket.dao.StatusCount;
import com.dvi.ticket.model.Client;
import com.dvi.ticket.model.Project;
import com.dvi.ticket.model.TicketStatus;
import com.dvi.ticket.model.User;
import com.dvi.ticket.service.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/projects")
@Tag(name = "Project APIs", description = "APIs for projects")
@PreAuthorize("hasAuthority('ROLE_ADMIN')")
public class ProjectController {

    private UserService userService;
    private ProjectService projectService;
    private ClientService clientService;
    private TicketService ticketService;
    private TicketStatusService ticketStatusService;

    public ProjectController(UserService userService, ProjectService projectService, ClientService clientService, TicketService ticketService, TicketStatusService ticketStatusService) {
        this.userService = userService;
        this.projectService = projectService;
        this.clientService = clientService;
        this.ticketService = ticketService;
        this.ticketStatusService = ticketStatusService;
    }

    /**
     * New project.
     *
     * @param projectStr
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    @InboundRequest
    public ResponseEntity<String> addProject(@RequestBody String projectStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject projectJson = new JSONObject(projectStr);
            if (!projectJson.has("name")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Project name cannot be empty");
            } else if (!projectJson.has("clientId")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Client id cannot be empty");
            } else {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (!(authentication instanceof AnonymousAuthenticationToken)) {

                    Project projectData = new Project();
                    projectData.setName(projectJson.getString("name"));
                    projectData.setClient(this.clientService.findById(projectJson.getLong("clientId")));
                    if (projectJson.has("description")) {
                        projectData.setDescription(projectJson.getString("description"));
                    }

                    Project results = this.projectService.addProject(projectData);

                    if (results != null) {
                        httpStatus = HttpStatus.OK;
                        response.put("status", "success");
                        response.put("message", "Project added successfully");
                    } else {
                        httpStatus = HttpStatus.EXPECTATION_FAILED;
                        response.put("status", "failed");
                        response.put("message", "Failed to add project");
                    }
                } else {
                    httpStatus = HttpStatus.UNAUTHORIZED;
                    response.put("status", "unauthorized");
                    response.put("message", "You are not authorized to perform this action");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }
    /**
     * List all projects.
     *
     * @param page
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.GET)
    @InboundRequest
    public Page<Project> getProjectList(@RequestParam(value = "name", defaultValue = "") String name,
                                        @RequestParam(value = "page", defaultValue = "0") int page,
                                        @RequestParam(value = "size", defaultValue = "10") int size,
                                        @RequestParam(value = "sort", defaultValue = "id") String sort,
                                        @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<Project> projectList = Collections.emptyList();
        Page<Project> projects = new PageImpl<>(projectList);
        try {
            projects = this.projectService.findAll(name, page, size, sort, order);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return projects;
    }

    @RequestMapping(value = "/ticket/statuses", method = RequestMethod.GET)
    @InboundRequest
    public List<ProjectTicketCount> statusCount(@RequestParam(value = "name", defaultValue = "") String name,
                                                @RequestParam(value = "page", defaultValue = "0") int page,
                                                @RequestParam(value = "size", defaultValue = "5") int size,
                                                @RequestParam(value = "sort", defaultValue = "status") String sort,
                                                @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<ProjectTicketCount> projectTicketCounts = new ArrayList<>();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                User user = this.userService.findByUsername(authentication.getName());
                for (Long projectId : this.projectService.findAll(user.getTenant(), "pending", size)) {
                    Project projectInfo = this.projectService.findById(projectId);
                    List<StatusCount> statusCounts = new ArrayList<>();
                    for (TicketStatus ticketStatus : this.ticketStatusService.findAll(0, Integer.MAX_VALUE, "id", "asc").getContent()) {
                        StatusCount statusCount = new StatusCount();
                        statusCount.setStatus(ticketStatus.getDetails());
                        statusCount.setCount(this.ticketService.countByProjectAndStatus(projectInfo, ticketStatus.getDetails()));
                        statusCounts.add(statusCount);
                    }
                    ProjectTicketCount projectTicketCount = new ProjectTicketCount();
                    projectTicketCount.setProject(projectInfo);
                    projectTicketCount.setStatusCounts(statusCounts);
                    projectTicketCounts.add(projectTicketCount);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return projectTicketCounts;
    }

    /**
     * Update project.
     *
     * @param project
     * @return
     */
    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    @InboundRequest
    public ResponseEntity<String> editProject(@RequestBody Project project) {
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            this.projectService.update(project);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return getStringResponse(httpStatus);
    }

    /**
     * Delete project by its id.
     *
     * @param projectId
     * @return
     */

    @RequestMapping(value = "/{projectId}", method = RequestMethod.DELETE)
    @InboundRequest
    public @ResponseBody ResponseEntity<String> removeProject(@PathVariable long projectId) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Project project = this.projectService.findById(projectId);
            this.projectService.remove(project);
            response.put("status", "success");
            response.put("message", "Project deleted successfully");
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    /**
     * get project by its id.
     *
     * @param projectId
     * @return
     */
    @RequestMapping(value = "/{projectId}", method = RequestMethod.GET, produces = "application/json")
    @InboundRequest
    public Project getProject(@PathVariable long projectId) {
        return this.projectService.findById(projectId);
    }

    private ResponseEntity<String> getStringResponse(HttpStatus httpStatus) {
        JSONObject response = new JSONObject();
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }
}