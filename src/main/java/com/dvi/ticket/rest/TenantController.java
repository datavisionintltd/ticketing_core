package com.dvi.ticket.rest;

import com.dvi.ticket.config.tenant.InboundRequest;
import com.dvi.ticket.dao.AgentTicketCount;
import com.dvi.ticket.dao.StatusCount;
import com.dvi.ticket.model.*;
import com.dvi.ticket.repository.SmsBalanceRepository;
import com.dvi.ticket.service.*;
import com.dvi.ticket.service.config.SysConfigService;
import com.dvi.ticket.util.DviUtilities;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

@RestController
@RequestMapping(value = "/tenants")
@PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN') or hasAuthority('ROLE_ADMIN')")
public class TenantController {

    private TenantService tenantService;
    private SysConfigService sysConfigService;
    private DviUtilities dviUtilities;
    private UserService userService;
    private UserGroupService userGroupService;
    private SmsBalanceRepository smsBalanceRepository;
    private TicketService ticketService;
    private TicketStatusService ticketStatusService;

    public TenantController(TenantService tenantService, SysConfigService sysConfigService, DviUtilities dviUtilities,
                            UserService userService, UserGroupService userGroupService, SmsBalanceRepository smsBalanceRepository,
                            TicketService ticketService, TicketStatusService ticketStatusService) {
        this.tenantService = tenantService;
        this.sysConfigService = sysConfigService;
        this.dviUtilities = dviUtilities;
        this.userService = userService;
        this.userGroupService = userGroupService;
        this.smsBalanceRepository = smsBalanceRepository;
        this.ticketService = ticketService;
        this.ticketStatusService = ticketStatusService;
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    @InboundRequest
    public ResponseEntity<String> addTenant(@RequestBody String tenantStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject tenantJson = new JSONObject(tenantStr);
            JSONObject companyJson = tenantJson.getJSONObject("company");
            JSONObject userJson = tenantJson.getJSONObject("admin");
            if (!companyJson.has("name")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Company name cannot be empty");
            } else if (!companyJson.has("phone")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Company phone cannot be empty");
            } else if (!companyJson.has("address")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Company address cannot be empty");
            } else if (!companyJson.has("email")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Company email cannot be empty");
            } else {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (!(authentication instanceof AnonymousAuthenticationToken)) {
                    Tenant tenant = new Tenant();
                    tenant.setName(companyJson.getString("name"));
                    tenant.setPhone(this.dviUtilities.formatPhoneNumber("255", companyJson.getString("phone")));
                    tenant.setAddress(companyJson.getString("address"));
                    tenant.setEmail(companyJson.getString("email"));
                    if (companyJson.has("website")) {
                        tenant.setWebsite(companyJson.getString("website"));
                    }
                    if (companyJson.has("logo")) {
                        tenant.setLogo(companyJson.getString("logo"));
                    }

                    String tillSuffix = String.format("%04d", 1);
                    Tenant lastAddedTenant = this.tenantService.findByLastAdded();
                    if (lastAddedTenant != null) {
                        int lastSuffix = Integer.parseInt(lastAddedTenant.getTillSuffix());
                        tillSuffix = String.format("%04d", lastSuffix + 1);
                    }
                    tenant.setTillSuffix(tillSuffix);
                    String tillNumber = this.sysConfigService.findByConfigName("tillPrefix").getConfigValue() + tillSuffix;
                    tenant.setTillNumber(tillNumber);
                    Tenant results = this.tenantService.addTenant(tenant);

                    if (results != null) {

                        List<Role> userRoles = Arrays.asList(this.userService.findRoleByName("ROLE_USER"), this.userService.findRoleByName("ROLE_ADMIN"));
                        User user = new User(userJson.getString("username"), userJson.getString("password"), userRoles);
                        user.setFullName(userJson.getString("fullName"));
                        user.setEmail(userJson.getString("email"));
                        user.setPhoneNumber(this.dviUtilities.formatPhoneNumber("+255", userJson.getString("phoneNumber")));
                        user.setEnabled(true);
                        user.setTenant(results);

                        User userResult = userService.addUser(user);

                        if (userResult != null) {
                            this.userService.updateQuery(userResult, results);
                            SmsBalance smsBalance = new SmsBalance();
                            smsBalance.setBalanceAmt(4000);
                            smsBalance.setBalanceSms(100);
                            smsBalance.setTenant(results);
                            this.smsBalanceRepository.save(smsBalance);
                            httpStatus = HttpStatus.OK;
                            response.put("status", "success");
                            response.put("message", "Company added successfully");
                        } else {
                            this.tenantService.remove(results);
                            httpStatus = HttpStatus.EXPECTATION_FAILED;
                            response.put("status", "failed");
                            response.put("message", "Failed to add a user");
                        }
                    } else {
                        httpStatus = HttpStatus.EXPECTATION_FAILED;
                        response.put("status", "failed");
                        response.put("message", "Failed to add a company");
                    }
                } else {
                    httpStatus = HttpStatus.UNAUTHORIZED;
                    response.put("status", "unauthorized");
                    response.put("message", "You are not authorized to perform this action");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> editTenant(@RequestBody Tenant tenant) {
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            this.tenantService.update(tenant);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        JSONObject response = new JSONObject();
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "/{tenantId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeTenant(@PathVariable long tenantId) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            Tenant tenant = this.tenantService.findById(tenantId);
            this.tenantService.remove(tenant);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/sms/balances", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> setSmsBalances() {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            for (Tenant tenant : this.tenantService.findAll(0,Integer.MAX_VALUE, "id","asc").getContent()) {
                SmsBalance smsBalance = this.smsBalanceRepository.findByTenant(tenant);
                if (smsBalance == null) {
                    smsBalance = new SmsBalance();
                    smsBalance.setBalanceAmt(4000);
                    smsBalance.setBalanceSms(100);
                    smsBalance.setTenant(tenant);
                    this.smsBalanceRepository.save(smsBalance);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "/{tenantId}", method = RequestMethod.GET, produces = "application/json")
    public Tenant getTenant(@PathVariable long tenantId) {
        return this.tenantService.findById(tenantId);
    }

    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public Page<Tenant> getTenants(@RequestParam(value = "page", defaultValue = "0") int page,
                                     @RequestParam(value = "size", defaultValue = "10") int size,
                                     @RequestParam(value = "sort", defaultValue = "id") String sort,
                                     @RequestParam(value = "order", defaultValue = "desc") String order) {
        return this.tenantService.findAll(page, size, sort, order);
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @RequestMapping(value = "/agents", method = RequestMethod.POST)
    @InboundRequest
    public @ResponseBody ResponseEntity<String> addAgent(@RequestBody String userStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject userJson = new JSONObject(userStr);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                if (this.userService.findByUsername(userJson.getString("email")) != null) {
                    response.put("status", "duplicate");
                    response.put("message", "This email is already taken, choose another email");
                    httpStatus = HttpStatus.CONFLICT;
                } else {
                    List<Role> userRoles = new ArrayList<>();
                    userRoles.add(this.userService.findRoleByName("ROLE_USER"));
                    userRoles.add(this.userService.findRoleByName("ROLE_AGENT"));
                    UserGroup userGroup = this.userGroupService.findByName("AGENTS");

                    User user = new User(userJson.getString("email"), userJson.getString("password"), userRoles);
                    user.setFullName(userJson.getString("fullName"));
                    user.setEmail(userJson.getString("email"));
                    user.setPhoneNumber(this.dviUtilities.formatPhoneNumber("+255", userJson.getString("phoneNumber")));
                    user.setEnabled(true);
                    user.setUserGroup(userGroup);
                    User user_result = userService.addUser(user);

                    if (user_result != null) {
                        response.put("status", "success");
                        response.put("message", "Account created successfully");
                        httpStatus = HttpStatus.OK;
                    }
                }
            } else {
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
                httpStatus = HttpStatus.UNAUTHORIZED;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    @InboundRequest
    @RequestMapping(value = "/agents", method = RequestMethod.GET)
    public Page<User> getAgents(@RequestParam(value = "searchKey", defaultValue = "") String searchKey,
                                  @RequestParam(value = "page", defaultValue = "0") int page,
                                  @RequestParam(value = "size", defaultValue = "10") int size,
                                  @RequestParam(value = "sort", defaultValue = "id") String sort,
                                  @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<User> userList = Collections.emptyList();
        Page<User> users = new PageImpl<>(userList);
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                User userInfo = this.userService.findByUsername(authentication.getName());
                UserGroup userGroup = this.userGroupService.findByName("AGENTS");
                users = this.userService.findAllAgents(userInfo.getTenant(), userGroup, searchKey, page, size, sort, order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

    @RequestMapping(value = "/ticket/statuses", method = RequestMethod.GET)
    @InboundRequest
    public List<AgentTicketCount> statusCount(@RequestParam(value = "name", defaultValue = "") String name,
                                              @RequestParam(value = "page", defaultValue = "0") int page,
                                              @RequestParam(value = "size", defaultValue = "5") int size,
                                              @RequestParam(value = "sort", defaultValue = "status") String sort,
                                              @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<AgentTicketCount> agentTicketCounts = new ArrayList<>();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                User user = this.userService.findByUsername(authentication.getName());
                for (Long userId : this.userService.findAllByTenantAndStatus(user.getTenant(), "pending", size)) {
                    User userInfo = this.userService.findById(userId);
                    List<StatusCount> statusCounts = new ArrayList<>();
                    for (TicketStatus ticketStatus : this.ticketStatusService.findAll(0, Integer.MAX_VALUE, "id", "asc").getContent()) {
                        StatusCount statusCount = new StatusCount();
                        statusCount.setStatus(ticketStatus.getDetails());
                        statusCount.setCount(this.ticketService.countByAgentAndStatus(userInfo, ticketStatus.getDetails()));
                        statusCounts.add(statusCount);
                    }
                    AgentTicketCount agentTicketCount = new AgentTicketCount();
                    agentTicketCount.setUser(userInfo);
                    agentTicketCount.setStatusCounts(statusCounts);
                    agentTicketCounts.add(agentTicketCount);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return agentTicketCounts;
    }
}
