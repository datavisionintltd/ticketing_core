package com.dvi.ticket.rest;

import com.dvi.ticket.config.tenant.InboundRequest;
import com.dvi.ticket.model.Team;
import com.dvi.ticket.model.TeamUser;
import com.dvi.ticket.model.User;
import com.dvi.ticket.resource.TeamResource;
import com.dvi.ticket.service.DepartmentService;
import com.dvi.ticket.service.TeamService;
import com.dvi.ticket.service.TeamUserService;
import com.dvi.ticket.service.UserService;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/teams")
@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
public class TeamController {

    private UserService userService;
    private TeamService teamService;
    private TeamUserService teamUserService;
    private DepartmentService departmentService;
    private TeamResource teamResource;

    public TeamController(UserService userService, TeamService teamService, TeamUserService teamUserService,
                          DepartmentService departmentService, TeamResource teamResource) {
        this.userService = userService;
        this.teamService = teamService;
        this.teamUserService = teamUserService;
        this.departmentService = departmentService;
        this.teamResource = teamResource;
    }

    
    @RequestMapping(value = "",  method = RequestMethod.POST, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> addTeam(@RequestBody String teamStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject teamJson = new JSONObject(teamStr);
            if (!teamJson.has("name")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Team name cannot be empty");
            } else {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (!(authentication instanceof AnonymousAuthenticationToken)) {

                    Team teamData = new Team();
                    teamData.setName(teamJson.getString("name"));
                    if (teamJson.has("departmentId")) {
                        teamData.setDepartment(this.departmentService.findById(teamJson.getLong("departmentId")));
                    }

                    Team results = this.teamService.addTeam(teamData);

                    if (results != null) {
                        httpStatus = HttpStatus.OK;
                        response.put("status", "success");
                        response.put("message", "Team added successfully");
                    } else {
                        httpStatus = HttpStatus.EXPECTATION_FAILED;
                        response.put("status", "failed");
                        response.put("message", "Failed to add a team");
                    }
                } else {
                    httpStatus = HttpStatus.UNAUTHORIZED;
                    response.put("status", "unauthorized");
                    response.put("message", "You are not authorized to perform this action");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    
    @RequestMapping(value = "",  method = RequestMethod.PUT, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> editTeam(@RequestBody String teamStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject teamJson = new JSONObject(teamStr);
            if (teamJson.has("id")) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (!(authentication instanceof AnonymousAuthenticationToken)) {

                    Team teamData = this.teamService.findById(teamJson.getLong("id"));
                    if (teamJson.has("name")) {
                        teamData.setName(teamJson.getString("name"));
                    }
                    if (teamJson.has("departmentId")) {
                        teamData.setDepartment(this.departmentService.findById(teamJson.getLong("departmentId")));
                    }

                    this.teamService.update(teamData);

                    httpStatus = HttpStatus.OK;
                    response.put("status", "success");
                    response.put("message", "Team updated successfully");
                } else {
                    httpStatus = HttpStatus.UNAUTHORIZED;
                    response.put("status", "unauthorized");
                    response.put("message", "You are not authorized to perform this action");
                }
            } else {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Team id cannot be empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    
    @RequestMapping(value = "/{teamId}",  method = RequestMethod.DELETE, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> removeTeam(@PathVariable long teamId) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {

                Team teamData = this.teamService.findById(teamId);

                this.teamService.update(teamData);

                if (this.teamResource.deleteTeam(teamData)) {
                    httpStatus = HttpStatus.OK;
                    response.put("status", "success");
                    response.put("message", "Team removed successfully");
                } else {
                    httpStatus = HttpStatus.EXPECTATION_FAILED;
                    response.put("status", "failed");
                    response.put("message", "Failed to remove team");
                }
            } else {
                httpStatus = HttpStatus.UNAUTHORIZED;
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    
    @RequestMapping(value = "/{teamId}",  method = RequestMethod.GET)
    @InboundRequest
    public @ResponseBody Team getTeam(@PathVariable long teamId) {
        Team teamInfo = new Team();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                teamInfo = this.teamService.findById(teamId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teamInfo;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    @InboundRequest
    
    public Page<Team> getTeams(@RequestParam(value = "departmentId", defaultValue = "0") long departmentId,
                               @RequestParam(value = "name", defaultValue = "") String name,
                               @RequestParam(value = "page", defaultValue = "0") int page,
                               @RequestParam(value = "size", defaultValue = "10") int size,
                               @RequestParam(value = "sort", defaultValue = "id") String sort,
                               @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<Team> teamList = Collections.emptyList();
        Page<Team> teams = new PageImpl<>(teamList);
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                if (departmentId > 0) {
                    teams = this.teamService.findAll(this.departmentService.findById(departmentId), name, page, size, sort, order);
                } else {
                    teams = this.teamService.findAll(name, page, size, sort, order);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teams;
    }

    
    @RequestMapping(value = "/members",  method = RequestMethod.POST, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> addTeamMember(@RequestBody String teamMemberStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject teamMemberJson = new JSONObject(teamMemberStr);
            if (!teamMemberJson.has("teamId")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Team Id cannot be empty");
            } else if (!teamMemberJson.has("userId")) {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "User Id cannot be empty");
            } else {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (!(authentication instanceof AnonymousAuthenticationToken)) {

                    Team teamInfo = this.teamService.findById(teamMemberJson.getLong("teamId"));
                    User userInfo = this.userService.findById(teamMemberJson.getLong("userId"));
                    TeamUser teamUserData = new TeamUser();
                    teamUserData.setTeam(teamInfo);
                    teamUserData.setUser(userInfo);

                    TeamUser results = this.teamUserService.addTeamUser(teamUserData);

                    if (results != null) {
                        httpStatus = HttpStatus.OK;
                        response.put("status", "success");
                        response.put("message", "Member added to a team successfully");
                    } else {
                        httpStatus = HttpStatus.EXPECTATION_FAILED;
                        response.put("status", "failed");
                        response.put("message", "Failed to add member to a team");
                    }
                } else {
                    httpStatus = HttpStatus.UNAUTHORIZED;
                    response.put("status", "unauthorized");
                    response.put("message", "You are not authorized to perform this action");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    
    @RequestMapping(value = "/users/{teamId}/{userId}",  method = RequestMethod.DELETE, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> removeTeamMember(@PathVariable long teamId, @PathVariable long userId) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {

                Team teamInfo = this.teamService.findById(teamId);
                User userInfo = this.userService.findById(userId);
                TeamUser teamUserData = new TeamUser();
                teamUserData.setTeam(teamInfo);
                teamUserData.setUser(userInfo);

                this.teamUserService.remove(teamUserData);

                httpStatus = HttpStatus.OK;
                response.put("status", "success");
                response.put("message", "Member removed from a team successfully");
            } else {
                httpStatus = HttpStatus.UNAUTHORIZED;
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    @InboundRequest
    public Page<TeamUser> getTeamMembers(@RequestParam(value = "teamId") long teamId,
                                         @RequestParam(value = "page", defaultValue = "0") int page,
                                         @RequestParam(value = "size", defaultValue = "10") int size,
                                         @RequestParam(value = "sort", defaultValue = "id") String sort,
                                         @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<TeamUser> teamUserList = Collections.emptyList();
        Page<TeamUser> teamUsers = new PageImpl<>(teamUserList);
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                Team teamInfo = this.teamService.findById(teamId);
                teamUsers = this.teamUserService.findAll(teamInfo, page, size, sort, order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return teamUsers;
    }
}
