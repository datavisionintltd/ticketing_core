package com.dvi.ticket.rest;
import com.dvi.ticket.model.GeneralSetting;
import com.dvi.ticket.service.GeneralSettingService;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value="/general/settings")
public class GeneralSettingController {

    private GeneralSettingService generalSettingService;

    public GeneralSettingController(GeneralSettingService generalSettingService) {
        this.generalSettingService = generalSettingService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addGeneralSetting(@RequestBody GeneralSetting setting) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            System.out.println("hh"+setting);
            GeneralSetting results = this.generalSettingService.addGeneralSetting(setting);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> editGeneralSetting(@RequestBody GeneralSetting setting) {
        JSONObject response = new JSONObject();
        HttpStatus httpStatus = HttpStatus.OK;
        try {
            this.generalSettingService.update(setting);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/{settingId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeGeneralSetting(@PathVariable long settingId) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            GeneralSetting setting = this.generalSettingService.findById(settingId);
            this.generalSettingService.remove(setting);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/{settingId}", method = RequestMethod.GET, produces = "application/json")
    public GeneralSetting getGeneralSetting(@PathVariable long settingId) {
        return this.generalSettingService.findById(settingId);
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public Page<GeneralSetting> getGeneralSettingList(@RequestParam(value = "name", defaultValue = "") String name,
                                                      @RequestParam(value = "page", defaultValue = "0") int page,
                                                      @RequestParam(value = "size", defaultValue = "10") int size,
                                                      @RequestParam(value = "sort", defaultValue = "id") String sort,
                                                      @RequestParam(value = "order", defaultValue = "desc") String order) {
        return this.generalSettingService.findAll(name, page, size, sort, order);
    }
}
