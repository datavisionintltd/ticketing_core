package com.dvi.ticket.rest;


import com.dvi.ticket.model.TenantPackage;
import com.dvi.ticket.model.TenantSubscription;
import com.dvi.ticket.service.TenantPackageService;
import com.dvi.ticket.service.TenantSubscriptionService;
import com.dvi.ticket.service.SubscriptionLogService;
import com.dvi.ticket.util.DviUtilities;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/subscriptions")
@PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN') or hasAuthority('ROLE_ADMIN')")
public class TenantSubscriptionController {

    private DviUtilities dviUtilities;
    private TenantSubscriptionService tenantSubscriptionService;
    private SubscriptionLogService subscriptionLogService;
    private TenantPackageService tenantPackageService;

    public TenantSubscriptionController(DviUtilities dviUtilities, TenantSubscriptionService tenantSubscriptionService,
                                        SubscriptionLogService subscriptionLogService, TenantPackageService tenantPackageService) {
        this.dviUtilities = dviUtilities;
        this.tenantSubscriptionService = tenantSubscriptionService;
        this.subscriptionLogService = subscriptionLogService;
        this.tenantPackageService = tenantPackageService;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addClientSubscription(@RequestBody TenantSubscription tenantSubscription) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        try {
            TenantSubscription results = this.tenantSubscriptionService.addTenantSubscription(tenantSubscription);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            } else {
                httpStatus = HttpStatus.EXPECTATION_FAILED;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return this.dviUtilities.getResponseEntity(httpStatus);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> editClientSubscription(@RequestBody TenantSubscription tenantSubscription) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        try {
            this.tenantSubscriptionService.update(tenantSubscription);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return this.dviUtilities.getResponseEntity(httpStatus);
    }

    @RequestMapping(value = "/{clientSubscriptionId}", method = RequestMethod.GET, produces = "application/json")
    public TenantSubscription getClientSubscription(@PathVariable long clientSubscriptionId) {
        TenantSubscription tenantSubscription = new TenantSubscription();
        try {
            tenantSubscription = this.tenantSubscriptionService.findById(clientSubscriptionId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tenantSubscription;
    }

    @RequestMapping(value = "/{clientSubscriptionId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeClientSubscription(@PathVariable long clientSubscriptionId) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        try {
            this.tenantSubscriptionService.remove(clientSubscriptionId);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return this.dviUtilities.getResponseEntity(httpStatus);
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<TenantSubscription> getClientSubscriptionList(@RequestParam(value = "page", defaultValue = "0") int page,
                                                              @RequestParam(value = "size", defaultValue = "10") int size,
                                                              @RequestParam(value = "sort", defaultValue = "id") String sort,
                                                              @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<TenantSubscription> tenantSubscriptionList = Collections.emptyList();
        Page<TenantSubscription> clientSubscriptions = new PageImpl<>(tenantSubscriptionList);
        try {
            clientSubscriptions = this.tenantSubscriptionService.findAll(page, size, sort, order);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clientSubscriptions;
    }

    @RequestMapping(value = "/packages", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addClientPackage(@RequestBody TenantPackage tenantPackage) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        try {
            TenantPackage results = this.tenantPackageService.addTenantPackage(tenantPackage);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            } else {
                httpStatus = HttpStatus.EXPECTATION_FAILED;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return this.dviUtilities.getResponseEntity(httpStatus);
    }

    @RequestMapping(value = "/packages", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> editClientPackage(@RequestBody TenantPackage tenantPackage) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        try {
            this.tenantPackageService.update(tenantPackage);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return this.dviUtilities.getResponseEntity(httpStatus);
    }

    @RequestMapping(value = "/packages/{clientPackageId}", method = RequestMethod.GET, produces = "application/json")
    public TenantPackage getClientPackage(@PathVariable long clientPackageId) {
        TenantPackage tenantPackage = new TenantPackage();
        try {
            tenantPackage = this.tenantPackageService.findById(clientPackageId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tenantPackage;
    }

    @RequestMapping(value = "/packages/{clientPackageId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeClientPackage(@PathVariable long clientPackageId) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        try {
            this.tenantPackageService.remove(clientPackageId);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return this.dviUtilities.getResponseEntity(httpStatus);
    }

    @RequestMapping(value = "/packages", method = RequestMethod.GET)
    public Page<TenantPackage> getClientPackageList(@RequestParam(value = "page", defaultValue = "0") int page,
                                                    @RequestParam(value = "size", defaultValue = "10") int size,
                                                    @RequestParam(value = "sort", defaultValue = "id") String sort,
                                                    @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<TenantPackage> tenantPackageList = Collections.emptyList();
        Page<TenantPackage> clientPackages = new PageImpl<>(tenantPackageList);
        try {
            clientPackages = this.tenantPackageService.findAll(page, size, sort, order);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return clientPackages;
    }
}
