package com.dvi.ticket.rest;

import com.dvi.ticket.model.Topic;
import com.dvi.ticket.model.settings.EmailTemplatesSettings;
import com.dvi.ticket.service.EmailTemplatesSettingsService;
import net.minidev.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController

@RequestMapping(value = "/email/templates")
@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
public class EmailTemplatesSettingsController {

    private EmailTemplatesSettingsService emailTemplatesSettingsService;

    public EmailTemplatesSettingsController(EmailTemplatesSettingsService emailTemplatesSettingsService) {
        this.emailTemplatesSettingsService = emailTemplatesSettingsService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<EmailTemplatesSettings> getAllTemplates(@RequestParam(value = "page", defaultValue = "0") int page,
                                                        @RequestParam(value = "size", defaultValue = "10") int size,
                                                        @RequestParam(value = "sort", defaultValue = "id") String sort,
                                                        @RequestParam(value = "direction", defaultValue = "desc") String order) {
        Page<EmailTemplatesSettings> topic = null;
        try {
            topic = this.emailTemplatesSettingsService.findAll(page, size, sort, order);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return topic;
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> updateTemplates(@RequestBody EmailTemplatesSettings emailTemplatesSettings) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            this.emailTemplatesSettingsService.update(emailTemplatesSettings);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/{templateId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeTemplates(@PathVariable long templateId) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            EmailTemplatesSettings emailTemplatesSettings = this.emailTemplatesSettingsService.findById(templateId);
            this.emailTemplatesSettingsService.remove(emailTemplatesSettings);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }
}
