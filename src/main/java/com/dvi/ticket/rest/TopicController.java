package com.dvi.ticket.rest;

import com.dvi.ticket.model.Topic;
import com.dvi.ticket.service.DepartmentService;
import com.dvi.ticket.service.ServiceLevelAgreementService;
import com.dvi.ticket.service.TopicService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/topics")
@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN') or hasAuthority('ROLE_AGENT')")
public class TopicController {

    private TopicService topicService;
    private DepartmentService departmentService;
    private ServiceLevelAgreementService serviceLevelAgreementService;

    public TopicController(TopicService topicService, DepartmentService departmentService, ServiceLevelAgreementService serviceLevelAgreementService) {
        this.topicService = topicService;
        this.departmentService = departmentService;
        this.serviceLevelAgreementService = serviceLevelAgreementService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<Topic> getAllTopics(@RequestParam(value = "page", defaultValue = "0") int page,
                                    @RequestParam(value = "size", defaultValue = "10") int size,
                                    @RequestParam(value = "sort", defaultValue = "id") String sort,
                                    @RequestParam(value = "direction", defaultValue = "desc") String order) {
        Page<Topic> topic = null;
        try {
            topic = this.topicService.findAll(page, size, sort, order);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return topic;
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addTopic(@Valid @RequestBody Topic topic) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Topic results = this.topicService.addTopic(topic);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/list", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addTopics(@RequestBody String topicStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            List<Topic> topics = new ArrayList<>();
            JSONArray topicArray = new JSONArray(topicStr);
            for (int i = 0; i < topicArray.length(); i++) {
                Topic topic = new Topic();
                JSONObject topicJson = topicArray.getJSONObject(i);
                topic.setDepartment(this.departmentService.findById(topicJson.getLong("departmentId")));
                topic.setServiceLevelAgreement(this.serviceLevelAgreementService.findById(topicJson.getLong("serviceLevelAgreementId")));
                topic.setName(topicJson.getString("name"));
                topic.setDescription(topicJson.getString("description").isEmpty() ? "N/A" : topicJson.getString("description"));
                topics.add(topic);
            }
            if (!topics.isEmpty()) {
                topics = this.topicService.addTopic(topics);
            }
            if (topics != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> updateTopic(@RequestBody Topic topic) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            this.topicService.update(topic);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/{topicId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<String> removeTopic(@PathVariable long topicId) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            Topic topic = this.topicService.findById(topicId);
            this.topicService.remove(topic);
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        response.put("message", httpStatus.getReasonPhrase());
        response.put("status", httpStatus.value());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }
}
