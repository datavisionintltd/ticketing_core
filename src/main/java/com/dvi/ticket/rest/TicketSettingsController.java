package com.dvi.ticket.rest;

import com.dvi.ticket.model.*;
import com.dvi.ticket.service.DictionaryItemService;
import com.dvi.ticket.service.StatusService;
import com.dvi.ticket.service.TicketSettingsService;
import com.dvi.ticket.service.TopicService;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import org.springframework.transaction.annotation.Transactional;

@RestController
@RequestMapping(value = "/settings/tickets")
@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
public class TicketSettingsController {

    private TicketSettingsService ticketSettingsService;
    private DictionaryItemService dictionaryItemService;
    private StatusService statusService;
    private TopicService topicService;

    public TicketSettingsController(  TopicService topicService,StatusService statusService,TicketSettingsService ticketSettingsService,DictionaryItemService dictionaryItemService){
        this.ticketSettingsService = ticketSettingsService;
        this.topicService = topicService;
        this.statusService = statusService;
        this.dictionaryItemService = dictionaryItemService;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public TicketSetting getTicketSettings() {
        return this.ticketSettingsService.findTopByOrderByIdDesc();
    }

    @RequestMapping(value = "", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> addTicketSettings(@RequestBody String ticketSetting) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject ticketSettingJson = new JSONObject(ticketSetting);
            TicketSetting ticketSet =new TicketSetting();
            long lockSemantic=ticketSettingJson.getLong("lockSemantic");
            long defaultStatus=ticketSettingJson.getLong("defaultStatus");
            long defaultTopic=ticketSettingJson.getLong("defaultTopic");
            String defaultNumberFormat=ticketSettingJson.getString("defaultNumberFormat");
            DictionaryItem lockSemanticItem=dictionaryItemService.findById(lockSemantic);
            Status defaultStatusItem= statusService.findById(defaultStatus);
            Topic defaultTopicItem= topicService.findById(defaultTopic);

            ticketSet.setLockSemantic(lockSemanticItem);
            ticketSet.setDefaultStatus(defaultStatusItem);
            ticketSet.setDefaultTopic(defaultTopicItem);
            ticketSet.setDefaultNumberFormat(defaultNumberFormat);
            TicketSetting results = this.ticketSettingsService.save(ticketSet);
            if (results != null) {
                httpStatus = HttpStatus.OK;
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<String> updateSystemSettings(@RequestBody String systemSetting) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {

            JSONObject ticketSettingJson = new JSONObject(systemSetting);
            long settingId=ticketSettingJson.getLong("id");

            TicketSetting ticketSet =this.ticketSettingsService.findById(settingId);

            String defaultNumberFormat=ticketSettingJson.getString("defaultNumberFormat");
            long lockSemanticItem=ticketSettingJson.getLong("lockSemantic");
            long defaultTopic=ticketSettingJson.getLong("defaultTopic");

            long defaultStatus=ticketSettingJson.getLong("defaultStatus");

            DictionaryItem lockSemantic=dictionaryItemService.findById(lockSemanticItem);

            Status status=statusService.findById(defaultStatus);

            Topic topic=topicService.findById(defaultTopic);

            ticketSet.setDefaultNumberFormat(defaultNumberFormat);
            ticketSet.setLockSemantic(lockSemantic);

            ticketSet.setDefaultTopic(topic);
            ticketSet.setDefaultStatus(status);
            TicketSetting results = this.ticketSettingsService.save(ticketSet);
            httpStatus = HttpStatus.OK;
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
        }

        response.put("status", httpStatus.value());
        response.put("message", httpStatus.getReasonPhrase());
        return new ResponseEntity<>(response.toString(), httpStatus);
    }
}
