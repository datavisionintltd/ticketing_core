package com.dvi.ticket.rest;

import com.dvi.ticket.model.*;
import com.dvi.ticket.resource.SmsResource;
import com.dvi.ticket.service.*;
import com.dvi.ticket.service.config.SysConfigService;
import com.dvi.ticket.util.DviUtilities;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;

@RestController
@RequestMapping(value = "/payments")
public class PaymentController {

    private PaymentService paymentService;
    private PaymentLogService paymentLogService;
    private ValidatePaymentService validatePaymentService;
    private ValidatePaymentLogService validatePaymentLogService;
    private UserService userService;
    private TenantService tenantService;
    private SmsFeeService smsFeeService;
    private SmsBalanceService smsBalanceService;
    private SysConfigService sysConfigService;
    private DviUtilities dviUtilities;
    private SmsPaymentRequestService smsPaymentRequestService;
    private SmsResource smsResource;

    public PaymentController(PaymentService paymentService, PaymentLogService paymentLogService, UserService userService, TenantService tenantService,
                             ValidatePaymentService validatePaymentService, ValidatePaymentLogService validatePaymentLogService, SmsFeeService smsFeeService,
                             SmsBalanceService smsBalanceService, SysConfigService sysConfigService, DviUtilities dviUtilities, SmsPaymentRequestService smsPaymentRequestService) {
        this.paymentService = paymentService;
        this.paymentLogService = paymentLogService;
        this.validatePaymentService = validatePaymentService;
        this.validatePaymentLogService = validatePaymentLogService;
        this.userService = userService;
        this.tenantService = tenantService;
        this.smsFeeService = smsFeeService;
        this.smsBalanceService = smsBalanceService;
        this.sysConfigService = sysConfigService;
        this.dviUtilities = dviUtilities;
        this.smsPaymentRequestService = smsPaymentRequestService;
    }

    private void fillSmsFee(@RequestBody Payment payment) {
        try {
            SmsFee smsFeeData = new SmsFee();
            smsFeeData.setTenant(payment.getTenant());
            smsFeeData.setTxnTime(payment.getTimestamp());
            smsFeeData.setAmount(payment.getAmount());
            smsFeeData.setNumberOfSms((int) (payment.getAmount() / 40));
            smsFeeData.setResponse("SUCCESS");
            SmsFee results = this.smsFeeService.newSmsFee(smsFeeData);
            if (results != null) {
                SmsBalance smsBalance = this.smsBalanceService.findByTenant(results.getTenant());
                smsBalance.setBalanceAmt(smsBalance.getBalanceAmt() + results.getAmount());
                smsBalance.setBalanceSms(smsBalance.getBalanceSms() + results.getNumberOfSms());
                this.smsBalanceService.update(smsBalance);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @RequestMapping(value = "/fee", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<String> fee(@RequestBody String mlipaString) {
        org.springframework.http.HttpStatus httpStatus = HttpStatus.OK;
        Payment paymentData = new Payment();
        JSONObject jsonRequest = new JSONObject(mlipaString);
        JSONObject payment_list = new JSONObject();

        try {
            ValidatePayment validated = this.validatePaymentService.findByMkey(jsonRequest.getString("MKEY"));

            if (!jsonRequest.has("AMOUNT")) {
                jsonRequest.put("AMOUNT", "1001");
            }

            if (jsonRequest.getFloat("AMOUNT") < 1001 || jsonRequest.getFloat("AMOUNT") > 3001000) {
                payment_list.put("MKEY", jsonRequest.getString("MKEY"));
                payment_list.put("ACTION", jsonRequest.getString("ACTION"));
                payment_list.put("REFERENCE", jsonRequest.getString("REFERENCE"));
                payment_list.put("STATUS", "REJECTED");
                httpStatus = HttpStatus.EXPECTATION_FAILED;
            } else if (jsonRequest.getString("ACTION").equalsIgnoreCase("VALIDATE")) {
                ValidatePaymentLog validatePaymentLog = new ValidatePaymentLog();
                validatePaymentLog.setValidatePaymentJson(mlipaString);
                this.validatePaymentLogService.addValidatePaymentLog(validatePaymentLog);

                payment_list.put("MKEY", jsonRequest.getString("MKEY"));
                payment_list.put("ACTION", jsonRequest.getString("ACTION"));
                payment_list.put("REFERENCE", jsonRequest.getString("REFERENCE"));

                if (this.tenantService.findByTillNumber(jsonRequest.getString("REFERENCE")) != null) {
                    payment_list.put("STATUS", "SUCCESS");
                } else {
                    payment_list.put("STATUS", "NOT_VALID");
                    httpStatus = HttpStatus.EXPECTATION_FAILED;
                }

                ValidatePayment validatePayment = new ValidatePayment();
                validatePayment.setMkey(jsonRequest.getString("MKEY"));
                validatePayment.setAction(jsonRequest.getString("ACTION"));
                validatePayment.setReference(jsonRequest.getString("REFERENCE"));
                if (!jsonRequest.has("AMOUNT")) {
                    validatePayment.setAmount(jsonRequest.getFloat("AMOUNT"));
                }
                this.validatePaymentService.addValidatePayment(validatePayment);
            } else if (validated != null) {
                PaymentLog paymentLog = new PaymentLog();
                paymentLog.setPaymentJson(mlipaString);
                PaymentLog paymentLogResults = this.paymentLogService.addPaymentLog(paymentLog);
                paymentData.setPaymentLog(paymentLogResults);

                float payment_amount = jsonRequest.getFloat("amount");
                float charges = Float.parseFloat(this.sysConfigService.findByConfigName("charges").getConfigValue());
                if (jsonRequest.has("charges")) {
                    charges = jsonRequest.getFloat("charges");
                }
                payment_amount = payment_amount - charges;

                Date req = this.dviUtilities.parseDate(jsonRequest.getString("timestamp"));
                Timestamp req_time = new Timestamp(req.getTime());

                paymentData.setReference(jsonRequest.getString("reference"));
                paymentData.setAmount(payment_amount);
                paymentData.setReceipt(jsonRequest.getString("receipt"));
                paymentData.setAction(jsonRequest.getString("ACTION"));
                if (jsonRequest.has("charges")) {
                    paymentData.setCharges(jsonRequest.getFloat("charges"));
                } else {
                    paymentData.setCharges(charges);
                }
                if (jsonRequest.has("msisdn")) {
                    paymentData.setMsisdn(jsonRequest.getString("msisdn"));
                } else {
                    paymentData.setMsisdn("0000000000");
                }
                paymentData.setTimestamp(req_time);
                Tenant tenant = this.tenantService.findByTillNumber(this.defineInstitution(jsonRequest.getString("reference")));
                paymentData.setTenant(tenant);
                if (this.paymentService.addPayment(paymentData) != null) {
                    if (this.smsPaymentRequestService.findByPaymentReference(jsonRequest.getString("reference")) != null) {
                        this.fillSmsFee(paymentData);
                    } else {
                        this.tenantPayment(paymentData);
                    }
                    payment_list.put("status", "SUCCESS");
                } else {
                    payment_list.put("status", "DUPLICATE");
                    httpStatus = HttpStatus.EXPECTATION_FAILED;
                }

                payment_list.put("receipt", jsonRequest.getString("receipt"));
                payment_list.put("clientID", tenant.getTillNumber());
                payment_list.put("clientName", tenant.getName());

                validated.setAction(jsonRequest.getString("ACTION"));
                validated.setTimestamp(jsonRequest.getString("timestamp"));
                validated.setReceipt(jsonRequest.getString("receipt"));
                validated.setAmount(jsonRequest.getFloat("amount"));
                if (jsonRequest.has("charges")) {
                    validated.setCharges(String.format("%f", jsonRequest.getFloat("charges")));
                } else {
                    validated.setCharges(String.format("%f", charges));
                }
                if (jsonRequest.has("msisdn")) {
                    validated.setMsisdn(jsonRequest.getString("msisdn"));
                } else {
                    validated.setMsisdn("0000000000");
                }
                this.validatePaymentService.update(validated);
            } else {
                payment_list.put("status", "wrong mkey");
                if (jsonRequest.has("RECEIPT")) {
                    payment_list.put("receipt", jsonRequest.getString("RECEIPT"));
                } else if (jsonRequest.has("receipt"))  {
                    payment_list.put("receipt", jsonRequest.getString("receipt"));
                }
                payment_list.put("clientID", "001");
                payment_list.put("clientName", "TICKET");
                httpStatus = HttpStatus.EXPECTATION_FAILED;
            }
        } catch (Exception e){
            if (jsonRequest.getString("ACTION").equalsIgnoreCase("VALIDATE")) {
                payment_list.put("STATUS", "REJECTED");
                payment_list.put("MKEY", jsonRequest.getString("MKEY"));
                payment_list.put("ACTION", jsonRequest.getString("ACTION"));
                if (jsonRequest.has("REFERENCE")) {
                    payment_list.put("REFERENCE", jsonRequest.getString("REFERENCE"));
                }
            } else {
                payment_list.put("status", "REJECTED");
                payment_list.put("MKEY", jsonRequest.getString("MKEY"));
                payment_list.put("ACTION", jsonRequest.getString("ACTION"));
                if (jsonRequest.has("REFERENCE")) {
                    payment_list.put("reference", jsonRequest.getString("REFERENCE"));
                }
            }
            e.printStackTrace();
        }

        return new ResponseEntity<>(payment_list.toString(), httpStatus);
    }

    private void tenantPayment(Payment payment) {
        float paidAmount = payment.getAmount();

        try {
            /*this.dbUtilities.updateFeeBalance(payment.getTenant(), paidAmount, 0);*/
            this.smsResource.tenantPaymentSms(payment.getTenant(), payment);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String defineInstitution(String businessAccount) {
        String mlipaAccount = businessAccount.substring(0, 3);
        try {
            if (this.tenantService.findByTillNumber(mlipaAccount) != null) {
                mlipaAccount = businessAccount.substring(0, 3);
            } else {
                mlipaAccount = businessAccount.substring(0, 7);
            }
        } catch (Exception e) {
            mlipaAccount = "NONE";
        }
        return mlipaAccount;
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<Payment> getPayments(@RequestParam(value = "searchKey", defaultValue = "") String searchKey,
                                     @RequestParam(value = "page", defaultValue = "0") int page,
                                     @RequestParam(value = "size", defaultValue = "10") int size,
                                     @RequestParam(value = "sort", defaultValue = "id") String sort,
                                     @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<Payment> paymentList = Collections.emptyList();
        Page<Payment> payments = new PageImpl<>(paymentList);
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                Predicate<GrantedAuthority> bySuperAdmin = role -> role.getAuthority().equalsIgnoreCase("ROLE_SUPER_ADMIN");
                Predicate<GrantedAuthority> bySuperUser = role -> role.getAuthority().equalsIgnoreCase("ROLE_SUPER_USER");
                if (authentication.getAuthorities().stream().anyMatch(bySuperAdmin) || authentication.getAuthorities().stream().anyMatch(bySuperUser)) {
                    payments = this.paymentService.findAll(searchKey, page, size, sort, order);
                } else {
                    User user = this.userService.findByUsername(authentication.getName());
                    payments = this.paymentService.findAll(user.getTenant(), searchKey, page, size, sort, order);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return payments;
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "/log/{logId}",  method = RequestMethod.GET)
    public @ResponseBody PaymentLog getPaymentLogs(@PathVariable long logId) {
        PaymentLog logInfo = new PaymentLog();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                logInfo = this.paymentLogService.findById(logId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return logInfo;
    }
}
