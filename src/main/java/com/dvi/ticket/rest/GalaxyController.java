package com.dvi.ticket.rest;

import com.dvi.ticket.config.security.SecurityConfig;
import com.dvi.ticket.model.User;
import com.dvi.ticket.resource.GalaxyResource;
import com.dvi.ticket.service.UserService;
import com.dvi.ticket.service.config.SysConfigService;
import org.json.JSONObject;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@RequestMapping(value = "/galaxy")
public class GalaxyController {

    private SysConfigService sysConfigService;
    private GalaxyResource galaxyResource;
    private UserService userService;
    private SecurityConfig securityConfig;

    public GalaxyController(SysConfigService sysConfigService, GalaxyResource galaxyResource, UserService userService, SecurityConfig securityConfig) {
        this.sysConfigService = sysConfigService;
        this.galaxyResource = galaxyResource;
        this.userService = userService;
        this.securityConfig = securityConfig;
    }

    @RequestMapping(value = "/token", method = RequestMethod.GET)
    public ModelAndView galaxyToken(@RequestParam(value = "sso_token") String sso_token) {
        String ticketSystemToken = "none";
        String redirectUrl = "http://maxdesk.softwaregalaxy.co.tz/#/galaxy/";
        try {
            redirectUrl = this.sysConfigService.findByConfigName("redirect_url").getConfigValue();
            String clientSecret = this.sysConfigService.findByConfigName("galaxy_secret").getConfigValue();
            String galaxyUser = this.galaxyResource.galaxyUser(sso_token, clientSecret);
            JSONObject userInfo = new JSONObject(galaxyUser);
            String username = userInfo.getJSONObject("payload").getString("username");
            User user = this.userService.findByUsername(username);

            if (user == null) {
                user = this.galaxyResource.registerUser(userInfo.getJSONObject("payload").toString());
            }

            OAuth2AccessToken accessToken = this.securityConfig.getAccessToken(user);
            ticketSystemToken = accessToken.getValue();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ModelAndView("redirect:" + redirectUrl + ticketSystemToken);
    }
}
