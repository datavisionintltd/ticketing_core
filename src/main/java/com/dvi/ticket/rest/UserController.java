package com.dvi.ticket.rest;

import com.dvi.ticket.config.tenant.InboundRequest;
import com.dvi.ticket.model.*;
import com.dvi.ticket.service.UserService;
import com.dvi.ticket.util.DviUtilities;
import com.dvi.ticket.util.EmailUtilities;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;


import java.util.*;

@RestController
@RequestMapping(value = "/users")
@Tag(name = "User APIs", description = "APIs for users")
public class UserController {

    private UserService userService;
    private DviUtilities dviUtilities;
    private EmailUtilities emailUtilities;

    public UserController(UserService userService, DviUtilities dviUtilities, EmailUtilities emailUtilities) {
        this.userService = userService;
        this.dviUtilities = dviUtilities;
        this.emailUtilities = emailUtilities;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> addUser(@RequestBody String userStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject detailsJson = new JSONObject(userStr);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                JSONObject userJson = detailsJson.getJSONObject("user");
                if (this.userService.findByUsername(userJson.getString("username")) != null) {
                    response.put("status", "duplicate");
                    response.put("message", "This Username is already taken, choose another name");
                    httpStatus = HttpStatus.CONFLICT;
                } else {
                    List<Role> userRoles = new ArrayList<>();
                    JSONArray rolesArray = detailsJson.getJSONArray("roles");

                    for(int i=0; i<rolesArray.length(); i++) {
                        JSONObject roleJson = rolesArray.getJSONObject(i);
                        Role role = this.userService.findRoleByRoleId(roleJson.getLong("id"));
                        userRoles.add(role);
                    }

                    User user = new User(userJson.getString("username"), userJson.getString("password"), userRoles);
                    user.setFullName(userJson.getString("fullName"));
                    user.setEmail(userJson.getString("email"));
                    user.setPhoneNumber(this.dviUtilities.formatPhoneNumber("+255", userJson.getString("phoneNumber")));
                    user.setEnabled(true);
                    User user_result = userService.addUser(user);

                    if (user_result != null) {
                        response.put("status", "success");
                        response.put("message", "Account created successfully");
                        httpStatus = HttpStatus.OK;
                    }

                    emailUtilities.registrationEmail(userJson.getString("username"), userJson.getString("password"));
                }
            } else {
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
                httpStatus = HttpStatus.UNAUTHORIZED;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "", method = RequestMethod.PUT)
    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> editUser(@RequestBody String userStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject detailsJson = new JSONObject(userStr);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                JSONObject userJson = detailsJson.getJSONObject("user");
                if (userJson.has("id")) {
                    User userInfo = this.userService.findById(userJson.getLong("id"));
                    List<Role> userRoles = new ArrayList<>();
                    JSONArray rolesArray = detailsJson.getJSONArray("roles");

                    for (int i = 0; i < rolesArray.length(); i++) {
                        JSONObject roleJson = rolesArray.getJSONObject(i);
                        Role role = this.userService.findRoleByRoleId(roleJson.getLong("id"));
                        if (this.userService.findUserRole(userInfo, role) == null) {
                            userRoles.add(role);
                        }
                    }

                    if (!userRoles.isEmpty()) {
                        userInfo.setRoles(userRoles);
                    }
                    if (userJson.has("fullName")) {
                        userInfo.setFullName(userJson.getString("fullName"));
                    }
                    if (userJson.has("email")) {
                        userInfo.setEmail(userJson.getString("email"));
                    }
                    if (userJson.has("phoneNumber")) {
                        userInfo.setPhoneNumber(this.dviUtilities.formatPhoneNumber("+255", userJson.getString("phoneNumber")));
                    }
                    User user_result = userService.updateUser(userInfo);

                    if (user_result != null) {
                        response.put("status", "success");
                        response.put("message", "Account updated successfully");
                        httpStatus = HttpStatus.OK;
                    }
                } else {
                    response.put("status", "incomplete");
                    response.put("message", "User Id cannot be empty");
                    httpStatus = HttpStatus.NOT_ACCEPTABLE;
                }
            } else {
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
                httpStatus = HttpStatus.UNAUTHORIZED;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "/{userId}", method = RequestMethod.GET)
    @InboundRequest
    public @ResponseBody User getUser(@PathVariable long userId) {
        User userInfo = new User();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                userInfo = this.userService.findById(userId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userInfo;
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "/enable", method = RequestMethod.POST)
    @InboundRequest
    public @ResponseBody ResponseEntity<String> enableUser(@RequestBody String userStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject userJson = new JSONObject(userStr);
            if (userJson.has("userId") || userJson.has("enable")) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (!(authentication instanceof AnonymousAuthenticationToken)) {
                    User userInfo = this.userService.findById(userJson.getLong("userId"));
                    userInfo.setEnabled(userJson.getBoolean("enable"));
                    this.userService.update(userInfo);

                    if (userJson.getBoolean("enable")) {
                        response.put("status", "success");
                        response.put("message", "User enabled successfully");
                    } else {
                        response.put("status", "success");
                        response.put("message", "User disabled successfully");
                    }
                    httpStatus = HttpStatus.OK;
                } else {
                    response.put("status", "unauthorized");
                    response.put("message", "You are not authorized to perform this action");
                    httpStatus = HttpStatus.UNAUTHORIZED;
                }
            } else {
                response.put("status", "incomplete");
                response.put("message", "Missing user id or enable logic");
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/reset/password", method = RequestMethod.POST, produces = "application/json")
    public @ResponseBody ResponseEntity<String> resetPassword(@RequestBody String passwordStr) {
        HttpStatus httpStatus = HttpStatus.OK;
        JSONObject response = new JSONObject();
        try {
            JSONObject passwordJson = new JSONObject(passwordStr);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                String password = passwordJson.getString("password");
                User user = this.userService.findByUsername(authentication.getName());
                if (password.equalsIgnoreCase(passwordJson.getString("confirm")) && user != null) {
                    user.setPassword(new BCryptPasswordEncoder().encode(password));
                    user.setPasswordProtected(true);
                    this.userService.update(user);
                    response.put("status", "success");
                    response.put("message", "User Password updated successfully");
                } else {
                    response.put("status", "fail");
                    response.put("message", "Failed To update Password, Please try again");
                    httpStatus = HttpStatus.EXPECTATION_FAILED;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "/roles", method = RequestMethod.POST)
    @InboundRequest
    public @ResponseBody ResponseEntity<String> addUserRole(@RequestBody String userRoleStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject userRoleJson = new JSONObject(userRoleStr);
            if (userRoleJson.has("userId") || userRoleJson.has("roleId")) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (!(authentication instanceof AnonymousAuthenticationToken)) {
                    User user = this.userService.findById(userRoleJson.getLong("userId"));
                    Role role = this.userService.findRoleByRoleId(userRoleJson.getLong("roleId"));
                    UserRole userRole = this.userService.addUserRole(user, role);

                    if (userRole != null) {
                        response.put("status", "success");
                        response.put("message", "User Role added successfully");
                        httpStatus = HttpStatus.OK;
                    }
                } else {
                    response.put("status", "unauthorized");
                    response.put("message", "You are not authorized to perform this action");
                    httpStatus = HttpStatus.UNAUTHORIZED;
                }
            } else {
                response.put("status", "incomplete");
                response.put("message", "Missing user id or role id");
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "/roles/{userId}", method = RequestMethod.GET)
    @InboundRequest
    public @ResponseBody List<Role> getUserRoles(@PathVariable long userId) {
        List<Role> roles = new ArrayList<>();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                User user = this.userService.findById(userId);
                for (UserRole userRole : this.userService.findUserRolesByUser(user)) {
                    roles.add(userRole.getRole());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return roles;
    }

    @RequestMapping(value = "/roles/details", method = RequestMethod.GET)
    @InboundRequest
    public @ResponseBody List<Role> getUserRoles() {
        List<Role> roles = new ArrayList<>();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                User user = this.userService.findByUsername(authentication.getName());
                for (UserRole userRole : this.userService.findUserRolesByUser(user)) {
                    roles.add(userRole.getRole());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return roles;
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "/roles/{userId}/{roleId}", method = RequestMethod.DELETE)
    @InboundRequest
    public @ResponseBody ResponseEntity<String> removeUserRole(@PathVariable long userId, @PathVariable long roleId) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                User user = this.userService.findById(userId);
                Role role = this.userService.findRoleByRoleId(roleId);
                UserRole userRole = this.userService.findUserRole(user, role);
                this.userService.removeUserRole(userRole);

                response.put("status", "success");
                response.put("message", "User Role deleted successfully");
                httpStatus = HttpStatus.OK;
            } else {
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
                httpStatus = HttpStatus.UNAUTHORIZED;
            }
        } catch (Exception e) {
            e.printStackTrace();
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    /**
     * up for discussion*/
    @PreAuthorize("hasAuthority('ROLE_SUPER_ADMIN')")
    @RequestMapping(value = "/roles", method = RequestMethod.DELETE)
    @InboundRequest
    public @ResponseBody ResponseEntity<String> removeUserRoles(@RequestBody String userRoleStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject userRoleJson = new JSONObject(userRoleStr);
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                JSONObject userJson = userRoleJson.getJSONObject("user");
                if (userJson.has("id")) {
                    User userInfo = this.userService.findById(userJson.getLong("id"));
                    List<UserRole> userRoles = new ArrayList<>();
                    JSONArray rolesArray = userRoleJson.getJSONArray("roles");

                    for (int i = 0; i < rolesArray.length(); i++) {
                        JSONObject roleJson = rolesArray.getJSONObject(i);
                        Role role = this.userService.findRoleByRoleId(roleJson.getLong("id"));
                        userRoles.add(this.userService.findUserRole(userInfo, role));
                    }

                    if (!userRoles.isEmpty()) {
                        this.userService.removeUserRole(userRoles);
                    }
                    response.put("status", "success");
                    response.put("message", "User roles removed successfully");
                    httpStatus = HttpStatus.OK;
                } else {
                    response.put("status", "incomplete");
                    response.put("message", "User Id cannot be empty");
                    httpStatus = HttpStatus.NOT_ACCEPTABLE;
                }
            } else {
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
                httpStatus = HttpStatus.UNAUTHORIZED;
            }
        } catch (Exception e) {
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
            httpStatus = HttpStatus.BAD_REQUEST;
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
    @InboundRequest
    @RequestMapping(value = "", method = RequestMethod.GET)
    public Page<User> getAllUsers(@RequestParam(value = "searchKey", defaultValue = "") String searchKey,
                                  @RequestParam(value = "page", defaultValue = "0") int page,
                                  @RequestParam(value = "size", defaultValue = "10") int size,
                                  @RequestParam(value = "sort", defaultValue = "id") String sort,
                                  @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<User> userList = Collections.emptyList();
        Page<User> users = new PageImpl<>(userList);
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                users = this.userService.findAll(searchKey, page, size, sort, order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }
}
