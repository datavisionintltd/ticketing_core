package com.dvi.ticket.rest;

import com.dvi.ticket.config.tenant.InboundRequest;
import com.dvi.ticket.model.Status;
import com.dvi.ticket.model.Topic;
import com.dvi.ticket.service.StatusService;
import com.dvi.ticket.service.TopicService;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

//TODO Ezilom to confirm clarity...
@RestController
@RequestMapping(value = "/common")
public class CommonController {

    private StatusService statusService;
    private TopicService topicService;

    public CommonController(StatusService statusService,TopicService topicService) {
        this.statusService = statusService;
        this.topicService = topicService;
    }
    @RequestMapping(value = "/statuses", method = RequestMethod.GET, produces = "application/json")
    @InboundRequest
    public Page<Status> getStatus(@RequestParam(value = "page", defaultValue = "0") int page,
                                  @RequestParam(value = "size", defaultValue = "10") int size,
                                  @RequestParam(value = "sort", defaultValue = "id") String sort,
                                  @RequestParam(value = "order", defaultValue = "desc") String order) {
        return this.statusService.findAll(page, size, sort, order);
    }

    @RequestMapping(value = "/topics", method = RequestMethod.GET, produces = "application/json")
    @InboundRequest
    public Page<Topic> getTopics(@RequestParam(value = "page", defaultValue = "0") int page,
                                @RequestParam(value = "size", defaultValue = "10") int size,
                                @RequestParam(value = "sort", defaultValue = "id") String sort,
                                @RequestParam(value = "order", defaultValue = "desc") String order) {
        return this.topicService.findAll(page, size, sort, order);
    }
}
