package com.dvi.ticket.rest;

import com.dvi.ticket.config.tenant.InboundRequest;
import com.dvi.ticket.model.Department;
import com.dvi.ticket.resource.DepartmentResource;
import com.dvi.ticket.service.DepartmentService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping(value = "/departments")
@PreAuthorize("hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_SUPER_ADMIN')")
public class DepartmentController {

    private DepartmentService departmentService;
    private DepartmentResource departmentResource;

    public DepartmentController(DepartmentService departmentService, DepartmentResource departmentResource) {
        this.departmentService = departmentService;
        this.departmentResource = departmentResource;
    }

    @RequestMapping(value = "",  method = RequestMethod.POST, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> addDepartment(@RequestBody String depStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject depJson = new JSONObject(depStr);
            if (depJson.has("name")) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (!(authentication instanceof AnonymousAuthenticationToken)) {

                    Department departmentData = new Department();
                    departmentData.setName(depJson.getString("name"));
                    if (depJson.has("description")) {
                        departmentData.setDescription(depJson.getString("description"));
                    }

                    Department results = this.departmentService.addDepartment(departmentData);

                    if (results != null) {
                        httpStatus = HttpStatus.OK;
                        response.put("status", "success");
                        response.put("message", "Department added successfully");
                    } else {
                        httpStatus = HttpStatus.EXPECTATION_FAILED;
                        response.put("status", "failed");
                        response.put("message", "Failed to add department");
                    }
                } else {
                    httpStatus = HttpStatus.UNAUTHORIZED;
                    response.put("status", "unauthorized");
                    response.put("message", "You are not authorized to perform this action");
                }
            } else {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Department name cannot be empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/list",  method = RequestMethod.POST, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> addDepartments(@RequestBody String depStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {

                List<Department> departments = new ArrayList<>();

                JSONArray depArray = new JSONArray(depStr);
                for (int i = 0; i < depArray.length(); i++) {
                    JSONObject depJson = depArray.getJSONObject(i);
                    Department departmentData = new Department();
                    departmentData.setName(depJson.getString("name"));
                    if (depJson.has("description")) {
                        departmentData.setDescription(depJson.getString("description"));
                    }
                    departments.add(departmentData);
                }

                if (!departments.isEmpty()) {
                    departments = this.departmentService.addDepartment(departments);
                }

                if (departments != null) {
                    httpStatus = HttpStatus.OK;
                    response.put("status", "success");
                    response.put("message", "Departments added successfully");
                } else {
                    httpStatus = HttpStatus.EXPECTATION_FAILED;
                    response.put("status", "failed");
                    response.put("message", "Failed to add departments");
                }
            } else {
                httpStatus = HttpStatus.UNAUTHORIZED;
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "",  method = RequestMethod.PUT, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> editDepartment(@RequestBody String depStr) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            JSONObject depJson = new JSONObject(depStr);
            if (depJson.has("id")) {
                Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
                if (!(authentication instanceof AnonymousAuthenticationToken)) {

                    Department departmentData = this.departmentService.findById(depJson.getLong("id"));
                    departmentData.setName(depJson.getString("name"));
                    if (depJson.has("name")) {
                        departmentData.setName(depJson.getString("name"));
                    }
                    if (depJson.has("description")) {
                        departmentData.setDescription(depJson.getString("description"));
                    }

                    this.departmentService.update(departmentData);

                    httpStatus = HttpStatus.OK;
                    response.put("status", "success");
                    response.put("message", "Department updated successfully");
                } else {
                    httpStatus = HttpStatus.UNAUTHORIZED;
                    response.put("status", "unauthorized");
                    response.put("message", "You are not authorized to perform this action");
                }
            } else {
                httpStatus = HttpStatus.NOT_ACCEPTABLE;
                response.put("status", "incomplete");
                response.put("message", "Department id cannot be empty");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/{depId}",  method = RequestMethod.DELETE, produces = "application/json")
    @InboundRequest
    public @ResponseBody ResponseEntity<String> removeDepartment(@PathVariable long depId) {
        HttpStatus httpStatus = HttpStatus.NOT_FOUND;
        JSONObject response = new JSONObject();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {

                Department departmentData = this.departmentService.findById(depId);

                this.departmentService.remove(departmentData);
                httpStatus = HttpStatus.OK;
                response.put("status", "success");
                response.put("message", "Department removed successfully");

                /*if (this.departmentResource.deleteDepartment(departmentData)) {
                    httpStatus = HttpStatus.OK;
                    response.put("status", "success");
                    response.put("message", "Department removed successfully");
                } else {
                    httpStatus = HttpStatus.EXPECTATION_FAILED;
                    response.put("status", "failed");
                    response.put("message", "Failed to remove department");
                }*/
            } else {
                httpStatus = HttpStatus.UNAUTHORIZED;
                response.put("status", "unauthorized");
                response.put("message", "You are not authorized to perform this action");
            }
        } catch (Exception e) {
            e.printStackTrace();
            httpStatus = HttpStatus.BAD_REQUEST;
            response.put("status", "error");
            response.put("message", "Errors occurred, Please try again");
        }
        return new ResponseEntity<>(response.toString(), httpStatus);
    }

    @RequestMapping(value = "/{depId}",  method = RequestMethod.GET)
    @InboundRequest
    public @ResponseBody Department getDepartment(@PathVariable long depId) {
        Department departmentInfo = new Department();
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                departmentInfo = this.departmentService.findById(depId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return departmentInfo;
    }

    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    @InboundRequest
    public Page<Department> getDepartments(@RequestParam(value = "name", defaultValue = "") String name,
                                           @RequestParam(value = "page", defaultValue = "0") int page,
                                           @RequestParam(value = "size", defaultValue = "10") int size,
                                           @RequestParam(value = "sort", defaultValue = "id") String sort,
                                           @RequestParam(value = "order", defaultValue = "desc") String order) {
        List<Department> departmentList = Collections.emptyList();
        Page<Department> departments = new PageImpl<>(departmentList);
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            if (!(authentication instanceof AnonymousAuthenticationToken)) {
                departments = this.departmentService.findAll(name, page, size, sort, order);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return departments;
    }
}
