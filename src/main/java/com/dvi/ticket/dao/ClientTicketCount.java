package com.dvi.ticket.dao;

import com.dvi.ticket.model.Client;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ClientTicketCount {

    private Client client;
    private List<StatusCount> statusCounts;
}
