package com.dvi.ticket.dao;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StatusCount {

    private String status;
    private long count;
}
