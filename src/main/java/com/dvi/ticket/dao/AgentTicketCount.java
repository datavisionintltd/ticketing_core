package com.dvi.ticket.dao;

import com.dvi.ticket.model.User;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AgentTicketCount {

    private User user;
    private List<StatusCount> statusCounts;
}
