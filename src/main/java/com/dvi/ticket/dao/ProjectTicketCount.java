package com.dvi.ticket.dao;

import com.dvi.ticket.model.Project;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ProjectTicketCount {

    private Project project;
    private List<StatusCount> statusCounts;
}
