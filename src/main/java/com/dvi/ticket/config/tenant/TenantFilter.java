package com.dvi.ticket.config.tenant;

import com.dvi.ticket.model.Tenant;
import com.dvi.ticket.repository.TenantRepository;
import com.dvi.ticket.util.DviUtilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.function.Predicate;

@Component
public class TenantFilter implements Filter {

    @Autowired
    private TenantRepository tenantRepository;
    private static final String TENANT_HEADER = "tenantId";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        String tenantHeader = request.getHeader(TENANT_HEADER);
        if (tenantHeader != null && !tenantHeader.isEmpty() && !request.getHeader("tenantId").equalsIgnoreCase("0")) {
            Tenant tenant = tenantRepository
                    .findById(Long.parseLong(tenantHeader));
            TenantContext.setCurrentTenant(tenant);
        } else if (request.getHeader("tenantId") == null || request.getHeader("tenantId").equalsIgnoreCase("0")) {
            if (!(DviUtilities.checkUri("/oauth/token", servletRequest) || DviUtilities.checkUri("/auth/", servletRequest) || DviUtilities.checkAuthority("ROLE_SUPER_ADMIN"))) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                response.setContentType(MediaType.APPLICATION_JSON_VALUE);
                response.getWriter().write("{\"error\": \"Missing tenant id\"}");
                response.getWriter().flush();
                return;
            }
        }
        /*filterChain.doFilter(servletRequest, servletResponse);*/

        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, PUT, PATCH, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Allow-Headers", "Content-Type, Authorization, Accept, X-Requested-With, remember-me, tenantId");
        response.setHeader("Access-Control-Max-Age", "31622400");

        if (HttpMethod.OPTIONS.name().equalsIgnoreCase(((HttpServletRequest) servletRequest).getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            // filterChain.doFilter(servletRequest, servletRequest);
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {
    }
}
