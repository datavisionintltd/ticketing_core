package com.dvi.ticket.config.tenant;

import com.dvi.ticket.model.Tenant;
import com.dvi.ticket.repository.TenantRepository;
import com.dvi.ticket.util.DviUtilities;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Aspect
@Component
@RequiredArgsConstructor
public class TenantInterceptor {

    private static final Logger log = LoggerFactory.getLogger(TenantInterceptor.class);

    private final TenantRepository tenantRepository;

    @Around("@annotation(InboundRequest)")
    public Object logInboundRequest(ProceedingJoinPoint joinPoint) throws Throwable {
        log.debug("Intercepting inbound request...");

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        HttpServletResponse response = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getResponse();
        log.debug("Extracting tenant id from method arguments!");
        /*String companyId = extractTenantId(joinPoint)
                .orElseThrow(RuntimeException::new);*/

        if (request.getHeader("tenantId") == null) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
            response.getWriter().write("{\"error\": \"Tenant id cannot be null\"}");
            response.getWriter().flush();
            return response;
        }

        String companyId = request.getHeader("tenantId");

        log.debug("Finding tenant by id!");
        Tenant tenant = tenantRepository
                .findById(Long.parseLong(companyId));
        /*.orElseThrow(RuntimeException::new);*/

        log.debug("Setting current tenant to Thread local variable!");
        if (!DviUtilities.checkAuthority("ROLE_SUPER_ADMIN")) {
            TenantContext.setCurrentTenant(tenant);
        }

        try {
            log.debug("Continuing with the execution!");
            return joinPoint.proceed();
        } finally {
            /*log.error(" Clearing tenant context!");*/
            TenantContext.clear();
        }
    }
}
