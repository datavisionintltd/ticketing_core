package com.dvi.ticket.config.language;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Locale;

import com.dvi.ticket.util.constants.LocaleConstants;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;

public class SmartLocaleResolver extends AcceptHeaderLocaleResolver {
    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        if (StringUtils.isBlank(request.getHeader("Accept-Language"))) {
            /*return Locale.getDefault();*/
            return Locale.US;
        }
        List<Locale.LanguageRange> list = Locale.LanguageRange.parse(request.getHeader("Accept-Language"));
        return Locale.lookup(list, LocaleConstants.LOCALES);
    }
}
