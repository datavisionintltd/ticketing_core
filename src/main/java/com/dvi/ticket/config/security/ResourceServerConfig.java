package com.dvi.ticket.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpMethod;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.error.OAuth2AccessDeniedHandler;

import javax.sql.DataSource;

@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Autowired
    private Environment env;

    @Override
    public void configure(HttpSecurity http) throws Exception {
        //-- define URL patterns to enable OAuth2 security
        http.anonymous().disable()
                .requestMatchers()
                .antMatchers("/users/**")
                .antMatchers("/roles/**")
                .antMatchers("/departments/**")
                .antMatchers("/teams/**")
                .antMatchers("/common/**")
                .antMatchers("/tenants/**")
                .antMatchers("/clients/**")
                .antMatchers("/projects/**")
                .antMatchers("/dictionaries/**")
                .antMatchers("/items/**")
                .antMatchers("/general/settings/**")
                .antMatchers("/tickets/**")
                .antMatchers("/ticket/settings/**")
                .antMatchers("/subscriptions/**")
                .antMatchers("/chart/**")
                .antMatchers("/report/**")
                .antMatchers("/tasks/**")
                .antMatchers("/charts/**")
                .antMatchers("/int/api/**")
                .antMatchers("/settings/system/**")
                .antMatchers("/**")
                .and().authorizeRequests()
                .antMatchers("/users/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/roles/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/departments/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/teams/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/common/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/tenants/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/clients/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/projects/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/dictionaries/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/items/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/general/settings/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/tickets/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/ticket/settings/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/subscriptions/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/chart/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/report/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/tasks/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/charts/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers("/int/api/**").access("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER') or hasRole('ROLE_SUPER_ADMIN') or hasRole('ROLE_AGENT')")
                .antMatchers(HttpMethod.OPTIONS, "/oauth/token").permitAll()
                .and().exceptionHandling().accessDeniedHandler(new OAuth2AccessDeniedHandler());
    }

    @Bean
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("spring.datasource.url"));
        dataSource.setUsername(env.getProperty("spring.datasource.username"));
        dataSource.setPassword(env.getProperty("spring.datasource.password"));
        return dataSource;
    }
}
