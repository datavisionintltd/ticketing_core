package com.dvi.ticket.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Getter
@Setter
@DiscriminatorValue("TI")
@FilterDef(name = "ticketThreadFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "ticketThreadFilter", condition = "tenant_id = :tenantId")
public class TicketThread extends ThreadMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;
}
