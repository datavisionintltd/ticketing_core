package com.dvi.ticket.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Getter
@Setter
@DiscriminatorValue("TA")
@FilterDef(name = "taskThreadFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "taskThreadFilter", condition = "tenant_id = :tenantId")
public class TaskThread extends ThreadMessage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @ManyToOne
    @JoinColumn(name = "task_id")
    private Task task;
}
