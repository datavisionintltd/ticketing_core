package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@Entity
@Getter
@Setter
@DiscriminatorColumn(name = "THREAD_TYPE")
@Table(name = "thread_messages")
@FilterDef(name = "threadMessageFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "threadMessageFilter", condition = "tenant_id = :tenantId")
public class ThreadMessage extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(columnDefinition = "TEXT")
    private String message;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
