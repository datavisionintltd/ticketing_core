package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "notification_id")
@FilterDef(name = "notificationFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "notificationFilter", condition = "tenant_id = :tenantId")
public class Notification extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
    @Column(columnDefinition = "TEXT")
    private String content;
    private boolean received;
    private boolean seen;
}
