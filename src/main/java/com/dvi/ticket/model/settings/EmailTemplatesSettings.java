package com.dvi.ticket.model.settings;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "email_template_settings")
public class EmailTemplatesSettings {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String code;
    private String name;
    @Column(columnDefinition = "TEXT")
    private String template;
}
