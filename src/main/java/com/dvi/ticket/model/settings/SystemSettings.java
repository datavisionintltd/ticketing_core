package com.dvi.ticket.model.settings;

import com.dvi.ticket.model.DictionaryItem;
import com.dvi.ticket.model.Setting;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "system_settings")
public class SystemSettings extends Setting {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "dictionary_item_id_language")
    private DictionaryItem language;
    @ManyToOne
    @JoinColumn(name = "dictionary_item_id_time_zone")
    private DictionaryItem defaultTimeZone;
    @ManyToOne
    @JoinColumn(name = "dictionary_item_id_page_size")
    private DictionaryItem defaultPageSize;
    private String fileSize;

    public SystemSettings() {}

    public SystemSettings(long id, DictionaryItem language, DictionaryItem defaultTimeZone, DictionaryItem defaultPageSize, String fileSize) {
        this.id = id;
        this.language = language;
        this.defaultTimeZone = defaultTimeZone;
        this.defaultPageSize = defaultPageSize;
        this.fileSize = fileSize;
    }
}
