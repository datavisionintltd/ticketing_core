package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "client", uniqueConstraints = {@UniqueConstraint(columnNames = {"tenant_id", "name"})})
@FilterDef(name = "clientFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "clientFilter", condition = "tenant_id = :tenantId")
public class Client extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String email = "N/A";
    private String phone = "N/A";
    @Column(columnDefinition = "TEXT")
    private String description = "N/A";

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<Project> projects;
}
