package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "team_users", uniqueConstraints = {@UniqueConstraint(columnNames = {"team_id", "user_id"})})
@FilterDef(name = "teamUserFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "teamUserFilter", condition = "tenant_id = :tenantId")
public class TeamUser extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "team_id")
    private Team team;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
