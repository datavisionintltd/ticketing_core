package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "sms_fees")
public class SmsFee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @ManyToOne
    @JoinColumn(name = "tenant_id")
    private Tenant tenant;
    @Column(name = "txn_time")
    private Timestamp txnTime;
    @Column(name = "amount")
    private float amount;
    @Column(name = "number_of_sms")
    private long numberOfSms;
    @Column(name = "response")
    private String response;
    @Column(name = "updated_time")
    @UpdateTimestamp
    private Timestamp updatedTime;
}
