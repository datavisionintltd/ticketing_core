package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "payment_logs")
/*@FilterDef(name = "paymentLogFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "paymentLogFilter", condition = "tenant_id = :tenantId")*/
public class PaymentLog extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(columnDefinition = "longtext")
    private String paymentJson;
}
