package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "dictionary_items")
@FilterDef(name = "dictionaryItemFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "dictionaryItemFilter", condition = "tenant_id = :tenantId")
public class DictionaryItem extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String code;
    @Column(columnDefinition = "TEXT")
    private String description;
    @ManyToOne
    @JoinColumn(name = "dictionary_id")
    private Dictionary dictionary;

    @OneToMany(mappedBy = "lockSemantic", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<TicketSetting> ticketSettings;
}