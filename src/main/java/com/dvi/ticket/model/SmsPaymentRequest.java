package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "sms_payment_requests")
public class SmsPaymentRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @ManyToOne
    @JoinColumn(name = "tenant_id")
    private Tenant tenant;
    @Column(name = "payment_reference")
    private String paymentReference;
    @Column(name = "requested_time")
    @CreationTimestamp
    private Timestamp requestedTime;
}
