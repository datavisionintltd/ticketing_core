package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "system_settings")
@FilterDef(name = "systemSettingFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "systemSettingFilter", condition = "tenant_id = :tenantId")
public class SystemSetting extends Setting {
    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;
}
