package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "user_details")
@FilterDef(name = "userDetailFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "userDetailFilter", condition = "tenant_id = :tenantId")
public class UserDetail extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
