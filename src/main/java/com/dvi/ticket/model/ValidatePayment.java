package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "validate_payments")
@FilterDef(name = "validatePaymentFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "validatePaymentFilter", condition = "tenant_id = :tenantId")
public class ValidatePayment extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "validate_payment_log_id")
    private ValidatePaymentLog validatePaymentLog;
    private String mkey;
    private String action;
    private String reference;
    private String timestamp;
    private String receipt;
    private float amount;
    private String charges;
    private String msisdn;
}
