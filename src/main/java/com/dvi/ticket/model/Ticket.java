package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "tickets", uniqueConstraints = {@UniqueConstraint(columnNames = {"tenant_id", "number"})})
@FilterDef(name = "ticketFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "ticketFilter", condition = "tenant_id = :tenantId")
public class Ticket extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String number;
    @ManyToOne
    @JoinColumn(name = "project_id")
    private Project project;
    @ManyToOne
    @JoinColumn(name = "topic_id")
    private Topic topic;
    @Column(columnDefinition = "TEXT")
    private String description;
    @Column(columnDefinition = "varchar(50) default 'unassigned'")
    private String status = "unassigned";
    private Timestamp dueDate;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<Task> tasks;
    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TicketThread> ticketThreads;
    @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<TicketUser> ticketUsers;

}
