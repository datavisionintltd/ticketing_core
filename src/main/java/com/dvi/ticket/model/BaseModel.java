package com.dvi.ticket.model;

import com.dvi.ticket.config.tenant.TenantContext;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@MappedSuperclass
public abstract class BaseModel {

        @ManyToOne
        @JoinColumn(name = "tenant_id")
        private Tenant tenant;

        @PrePersist
        @PreUpdate
        public void prePersist() {
                setTenant(TenantContext.getCurrentTenant());
        }

        @CreationTimestamp
        protected Timestamp createdAt;
        @UpdateTimestamp
        protected Timestamp updatedAt;
        @Column(nullable = true)
        protected Timestamp deletedAt;
}
