package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "tenant_settings")
@FilterDef(name = "tenantSettingFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "tenantSettingFilter", condition = "tenant_id = :tenantId")
public class TenantSetting extends Setting {
}
