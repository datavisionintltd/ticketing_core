package com.dvi.ticket.model;

import com.dvi.ticket.config.tenant.TenantContext;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "sms_balances")
public class SmsBalance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @ManyToOne
    @JoinColumn(name = "tenant_id", unique = true)
    private Tenant tenant;

    @PrePersist
    @PreUpdate
    public void prePersist() {
        setTenant(TenantContext.getCurrentTenant());
    }

    @Column(name = "balance_amt")
    private float balanceAmt;
    @Column(name = "balance_sms")
    private long balanceSms;
    @Column(name = "updated_time")
    @UpdateTimestamp
    private Timestamp updatedTime;
}
