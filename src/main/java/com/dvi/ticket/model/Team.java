package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "teams", uniqueConstraints = {@UniqueConstraint(columnNames = {"department_id", "name"})})
@FilterDef(name = "teamFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "teamFilter", condition = "tenant_id = :tenantId")
public class Team extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;
    private String name;

    @OneToMany(mappedBy = "team", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<TeamUser> teamUsers;
}
