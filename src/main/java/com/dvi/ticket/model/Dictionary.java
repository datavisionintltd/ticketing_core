package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "dictionaries")
@FilterDef(name = "dictionaryFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "dictionaryFilter", condition = "tenant_id = :tenantId")
public class Dictionary extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String code;
    @Column(columnDefinition = "TEXT")
    private String description;

    @OneToMany(mappedBy = "dictionary", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<DictionaryItem> dictionaryItems;
}
