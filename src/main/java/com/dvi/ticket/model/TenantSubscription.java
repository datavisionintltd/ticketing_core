package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
@Table(name = "tenant_subscriptions")
@FilterDef(name = "tenantSubscriptionFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "tenantSubscriptionFilter", condition = "tenant_id = :tenantId")
public class TenantSubscription extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "tenant_package_id")
    @NotNull(message = "Tenant Package cannot be null")
    private TenantPackage tenantPackage;
}
