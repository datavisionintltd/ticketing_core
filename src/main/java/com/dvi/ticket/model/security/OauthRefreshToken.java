package com.dvi.ticket.model.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oauth_refresh_token")
public class OauthRefreshToken {

    @Id
    private String tokenId;
    @Column(columnDefinition = "BLOB")
    private long token;
    @Column(columnDefinition = "BLOB")
    private long authentication;
}
