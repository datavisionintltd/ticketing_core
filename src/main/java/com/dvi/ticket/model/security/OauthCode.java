package com.dvi.ticket.model.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oauth_code")
public class OauthCode {

    @Id
    private String code;
    @Column(columnDefinition = "BLOB")
    private long authentication;
}
