package com.dvi.ticket.model.security;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "oauth_client_details")
public class OauthClientDetails {

    @Id
    private String clientId;
    private String resourceIds;
    private String clientSecret;
    private String scope;
    private String authorizedGrantTypes;
    private String webServerRedirectUri;
    private String authorities;
    private int access_token_validity;
    private int refreshTokenValidity;
    private String additionalInformation;
    private boolean autoapprove;
}
