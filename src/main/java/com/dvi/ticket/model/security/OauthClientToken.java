package com.dvi.ticket.model.security;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "oauth_client_token")
public class OauthClientToken {

    private String tokenId;
    @Column(columnDefinition = "BLOB")
    private long token;
    @Id
    private String authenticationId;
    private String userName;
    private String clientId;
}
