package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "projects", uniqueConstraints = {@UniqueConstraint(columnNames = {"tenant_id", "name", "client_id"})})
@FilterDef(name = "projectFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "projectFilter", condition = "tenant_id = :tenantId")
public class Project extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private Client client;
    @Column(columnDefinition = "TEXT")
    private String description = "N/A";

    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<ProjectUser> projectUsers;
    @OneToMany(mappedBy = "project", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<Ticket> tickets;
}
