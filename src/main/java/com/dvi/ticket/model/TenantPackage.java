package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "tenant_packages")
@FilterDef(name = "tenantPackageFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "tenantPackageFilter", condition = "tenant_id = :tenantId")
public class TenantPackage extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    // In Months
    @NotNull(message = "Duration cannot be null")
    private int duration = 6;
    @NotNull(message = "Number of projects cannot be null")
    private int numberOfProjects = 5;
    @NotNull(message = "Number of agents cannot be null")
    private int numberOfAgents = 5;
    @NotNull(message = "Package name cannot be null")
    private String name;

    @OneToMany(mappedBy = "tenantPackage", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<TenantSubscription> tenantSubscriptions;
}
