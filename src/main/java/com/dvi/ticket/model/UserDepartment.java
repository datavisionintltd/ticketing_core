package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "user_departments")
@FilterDef(name = "userDepartmentFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "userDepartmentFilter", condition = "tenant_id = :tenantId")
public class UserDepartment extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
