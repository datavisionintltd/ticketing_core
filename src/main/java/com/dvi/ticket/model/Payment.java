package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@Table(name = "payments")
/*@FilterDef(name = "paymentFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "paymentFilter", condition = "tenant_id = :tenantId")*/
public class Payment extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "payment_log_id")
    private PaymentLog paymentLog;
    private String mkey;
    private String action;
    private String reference;
    private float amount;
    private String receipt;
    private String msisdn;
    private float charges;
    private Timestamp timestamp;
}
