package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "tasks")
@FilterDef(name = "taskFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "taskFilter", condition = "tenant_id = :tenantId")
public class Task extends BaseModel {

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private long id;
        private String name;
        @ManyToOne
        @JoinColumn(name = "ticket_id")
        private Ticket ticket;
        @Column(columnDefinition = "varchar(50) default 'opened'")
        private String status = "opened";
        @Column(columnDefinition = "TEXT")
        private String description;

        @OneToMany(mappedBy = "task", cascade = CascadeType.ALL, orphanRemoval = true)
        private List<TaskThread> taskThreads;
        @OneToMany(mappedBy = "task", cascade = CascadeType.ALL, orphanRemoval = true)
        @Getter(AccessLevel.NONE)
        @Setter(AccessLevel.NONE)
        private List<TaskUser> taskUsers;
}
