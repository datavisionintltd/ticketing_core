package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@MappedSuperclass
public abstract class Setting extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String sKey;
    private String sValue;
    @Column(columnDefinition = "TEXT")
    private String description;
}
