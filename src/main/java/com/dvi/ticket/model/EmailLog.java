package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "email_logs")
@FilterDef(name = "emailLogFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "emailLogFilter", condition = "tenant_id = :tenantId")
public class EmailLog extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    private String subject;
    @Column(columnDefinition = "LONGTEXT")
    private String content;
    private String receiver;
    private String cc;
    private String bcc;
    private String status;
}
