package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "topics", uniqueConstraints = {@UniqueConstraint(columnNames = {"service_level_agreement_id", "name"})})
@FilterDef(name = "topicFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "topicFilter", condition = "tenant_id = :tenantId")
public class Topic extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;
    @ManyToOne
    @JoinColumn(name = "service_level_agreement_id")
    private ServiceLevelAgreement serviceLevelAgreement;
    @Column(columnDefinition = "TEXT")
    private String description;

    @OneToMany(mappedBy = "topic", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<Ticket> tickets;
    @OneToMany(mappedBy = "defaultTopic", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<TicketSetting> ticketSettings;
}
