package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "departments", uniqueConstraints = {@UniqueConstraint(columnNames = {"tenant_id", "name"})})
@FilterDef(name = "departmentFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "departmentFilter", condition = "tenant_id = :tenantId")
public class Department extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @NotNull(message = "Department name cannot be null")
    @NotBlank(message = "Department name cannot be blank")
    @NotEmpty(message = "Department name cannot be empty")
    private String name;
    @Column(columnDefinition = "TEXT")
    private String description = "N/A";

    @OneToMany(mappedBy = "department", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<Team> teams;
    @OneToMany(mappedBy = "department", cascade = {CascadeType.REMOVE}, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<Topic> topics;
    @OneToMany(mappedBy = "department", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<UserDepartment> userDepartments;
}
