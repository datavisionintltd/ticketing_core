package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "validate_payment_logs")
@FilterDef(name = "validatePaymentLogFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "validatePaymentLogFilter", condition = "tenant_id = :tenantId")
public class ValidatePaymentLog extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(columnDefinition = "longtext")
    private String validatePaymentJson;
}
