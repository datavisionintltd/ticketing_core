package com.dvi.ticket.model;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "ticket_settings")
@FilterDef(name = "ticketSettingFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "ticketSettingFilter", condition = "tenant_id = :tenantId")
public class TicketSetting extends Setting {
    @ManyToOne
    @JoinColumn(name = "dictionary_item_id")
    private DictionaryItem lockSemantic;
    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status defaultStatus;
    @ManyToOne
    @JoinColumn(name = "topic_id")
    private Topic defaultTopic;
    private String defaultNumberFormat;
}
