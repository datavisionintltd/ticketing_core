package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "sms_logs")
@FilterDef(name = "smsLogFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "smsLogFilter", condition = "tenant_id = :tenantId")
public class SmsLog extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "sender")
    private String sender;
    @Column(name = "message", columnDefinition = "longtext")
    private String message;
    @Column(name = "push")
    private String push;
    @Column(name = "recipient", columnDefinition = "longtext")
    private String recipient;
    @Column(name = "status")
    private String status;
}