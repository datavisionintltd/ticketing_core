package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "service_level_agreements", uniqueConstraints = {@UniqueConstraint(columnNames = {"tenant_id", "name"})})
@FilterDef(name = "serviceLevelAgreementFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "serviceLevelAgreementFilter", condition = "tenant_id = :tenantId")
public class ServiceLevelAgreement extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private int gracePeriod;
    @Column(columnDefinition = "TEXT")
    private String description;

    @OneToMany(mappedBy = "serviceLevelAgreement", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<Topic> topics;
}
