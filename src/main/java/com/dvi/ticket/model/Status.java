package com.dvi.ticket.model;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "statuses")
@FilterDef(name = "statusFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "statusFilter", condition = "tenant_id = :tenantId")
public class Status extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

    @OneToMany(mappedBy = "status", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<TaskUser> taskUsers;
    @OneToMany(mappedBy = "defaultStatus", cascade = CascadeType.ALL, orphanRemoval = true)
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private List<TicketSetting> ticketSettings;
}
