package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "subscription_logs")
@FilterDef(name = "subscriptionLogFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "subscriptionLogFilter", condition = "tenant_id = :tenantId")
public class SubscriptionLog extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "tenant_subscription_id")
    private TenantSubscription tenantSubscription;
}
