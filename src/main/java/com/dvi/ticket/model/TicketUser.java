package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;

import javax.persistence.*;

@Entity
@Getter
@Setter
@Table(name = "ticket_users")
@FilterDef(name = "ticketUserFilter", parameters = {@ParamDef(name = "tenantId", type = "int")})
@Filter(name = "ticketUserFilter", condition = "tenant_id = :tenantId")
public class TicketUser extends BaseModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "ticket_id")
    private Ticket ticket;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
