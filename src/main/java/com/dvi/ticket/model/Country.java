package com.dvi.ticket.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@Table(name = "country")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(unique = true)
    private String code;
    private String name;
    private String dialCode;
    private String currency;

    @OneToMany(mappedBy = "country")
    private List<Tenant> tenants;
}
