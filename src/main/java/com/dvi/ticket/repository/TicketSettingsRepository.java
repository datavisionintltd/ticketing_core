package com.dvi.ticket.repository;

import com.dvi.ticket.model.TicketSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketSettingsRepository extends JpaRepository<TicketSetting,Long> {
    TicketSetting findById(long id);
    TicketSetting findTopByOrderByIdDesc();
}
