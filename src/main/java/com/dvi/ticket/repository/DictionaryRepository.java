package com.dvi.ticket.repository;

import com.dvi.ticket.model.Dictionary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DictionaryRepository extends JpaRepository<Dictionary,Long> {

    Dictionary findById(long id);
    Dictionary findByCode(String code);
    Dictionary findByIdAndCode(long id, String code);
}
