package com.dvi.ticket.repository;

import com.dvi.ticket.model.User;
import com.dvi.ticket.model.VerificationCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VerificationCodeRepository extends JpaRepository<VerificationCode, Long> {

    VerificationCode findById(long id);
    VerificationCode findByUser(User user);
    VerificationCode findByCurrentCode(String currentCode);

    List<VerificationCode> findAllByUser(User user);
}
