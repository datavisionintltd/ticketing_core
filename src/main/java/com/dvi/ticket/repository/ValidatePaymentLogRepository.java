package com.dvi.ticket.repository;

import com.dvi.ticket.model.ValidatePaymentLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ValidatePaymentLogRepository extends JpaRepository<ValidatePaymentLog, Long> {

    ValidatePaymentLog findById(long id);
}
