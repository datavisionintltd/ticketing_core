package com.dvi.ticket.repository;

import com.dvi.ticket.model.TenantSubscription;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TenantSubscriptionRepository extends JpaRepository<TenantSubscription, Long> {

    TenantSubscription findById(long id);
}
