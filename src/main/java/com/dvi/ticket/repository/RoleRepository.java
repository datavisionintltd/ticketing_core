package com.dvi.ticket.repository;

import com.dvi.ticket.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findById(long id);
    Role findByName(String name);

    List<Role> findAll();
}
