package com.dvi.ticket.repository;

import com.dvi.ticket.model.settings.EmailTemplatesSettings;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmailTemplatesSettingsRepository extends JpaRepository<EmailTemplatesSettings, Long> {
    EmailTemplatesSettings findById(long id);
}
