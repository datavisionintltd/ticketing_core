package com.dvi.ticket.repository;

import com.dvi.ticket.model.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {

    Status findById(long id);
    Status findByName(String name);
}
