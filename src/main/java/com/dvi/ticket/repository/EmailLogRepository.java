package com.dvi.ticket.repository;

import com.dvi.ticket.model.EmailLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailLogRepository extends JpaRepository<EmailLog, Long> {

    EmailLog findById(long id);
}
