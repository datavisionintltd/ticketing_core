package com.dvi.ticket.repository;

import com.dvi.ticket.model.SmsBalance;
import com.dvi.ticket.model.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsBalanceRepository extends JpaRepository<SmsBalance, Long> {

    SmsBalance findById(long id);
    SmsBalance findByTenant(Tenant tenant);
}
