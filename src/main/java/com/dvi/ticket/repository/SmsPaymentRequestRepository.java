package com.dvi.ticket.repository;

import com.dvi.ticket.model.SmsPaymentRequest;
import com.dvi.ticket.model.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsPaymentRequestRepository extends JpaRepository<SmsPaymentRequest, Long> {

    SmsPaymentRequest findById(long id);
    SmsPaymentRequest findByTenant(Tenant tenant);
    SmsPaymentRequest findByPaymentReference(String paymentReference);
}
