package com.dvi.ticket.repository;

import com.dvi.ticket.model.Department;
import com.dvi.ticket.model.Topic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicRepository extends JpaRepository<Topic, Long> {

    Topic findById(long id);
    Topic findByName(String name);

    Page<Topic> findAllByNameContaining(String name, Pageable pageable);
    Page<Topic> findAllByDepartment(Department department, Pageable pageable);
    Page<Topic> findAllByDepartmentAndNameContaining(Department department, String name, Pageable pageable);
}
