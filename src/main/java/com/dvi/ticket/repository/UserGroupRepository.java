package com.dvi.ticket.repository;

import com.dvi.ticket.model.UserGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserGroupRepository extends JpaRepository<UserGroup, Long> {

    UserGroup findById(long id);
    UserGroup findByName(String name);

    Page<UserGroup> findAllByNameContaining(String name, Pageable pageable);
}
