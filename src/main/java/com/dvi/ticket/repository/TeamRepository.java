package com.dvi.ticket.repository;

import com.dvi.ticket.model.Department;
import com.dvi.ticket.model.Team;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamRepository extends JpaRepository<Team, Long> {

    Team findById(long id);

    Page<Team> findAllByNameContaining(String name, Pageable pageable);
    Page<Team> findAllByDepartment(Department department, Pageable pageable);
    Page<Team> findAllByDepartmentAndNameContaining(Department department, String name, Pageable pageable);
}
