package com.dvi.ticket.repository;
import com.dvi.ticket.model.Dictionary;
import com.dvi.ticket.model.DictionaryItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DictionaryItemRepository extends JpaRepository<DictionaryItem, Long> {
    DictionaryItem findById(long id);
    DictionaryItem findByCode(String code);
    Page<DictionaryItem> findAllByDictionary(Dictionary dictionary, Pageable pageable);
}
