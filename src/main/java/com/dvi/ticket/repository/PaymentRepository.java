package com.dvi.ticket.repository;

import com.dvi.ticket.model.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Long> {

    Payment findById(long id);

    @Query(value = "SELECT p.* FROM payments p WHERE p.reference LIKE %?1% OR p.receipt LIKE %?1% AND p.msisdn LIKE %?1%",
            countQuery = "SELECT COUNT(*) FROM payments p WHERE p.reference LIKE %?1% OR p.receipt LIKE %?1% AND p.msisdn LIKE %?1%",
            nativeQuery = true)
    Page<Payment> findAllBySearchKey(String searchKey, Pageable pageable);

    @Query(value = "SELECT p.* FROM payments p WHERE p.tenant_id = ?1 AND (p.reference LIKE %?2% OR p.receipt LIKE %?2% AND p.msisdn LIKE %?2%)",
            countQuery = "SELECT COUNT(*) FROM payments p WHERE p.tenant_id = ?1 (p.reference LIKE %?2% OR p.receipt LIKE %?2% AND p.msisdn LIKE %?2%)",
            nativeQuery = true)
    Page<Payment> findAllByTenantIdAndSearchKey(long tenantId, String searchKey, Pageable pageable);
}
