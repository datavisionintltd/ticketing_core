package com.dvi.ticket.repository;
import com.dvi.ticket.model.settings.SystemSettings;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SystemSettingsRepository  extends JpaRepository<SystemSettings,Long> {
    SystemSettings findById(long id);
    SystemSettings findTopByOrderByIdDesc();
}
