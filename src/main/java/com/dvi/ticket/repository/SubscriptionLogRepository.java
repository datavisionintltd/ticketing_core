package com.dvi.ticket.repository;

import com.dvi.ticket.model.SubscriptionLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SubscriptionLogRepository extends JpaRepository<SubscriptionLog, Long> {

    SubscriptionLog findById(long id);
}
