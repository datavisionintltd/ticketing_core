package com.dvi.ticket.repository;

import com.dvi.ticket.model.Task;
import com.dvi.ticket.model.TaskThread;
 
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskThreadRepository extends JpaRepository<TaskThread, Long> {

    TaskThread findById(long id);

    Page<TaskThread> findAllByTask(Task task, Pageable pageable);
}
