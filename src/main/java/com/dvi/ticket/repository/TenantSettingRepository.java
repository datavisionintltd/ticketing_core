package com.dvi.ticket.repository;

import com.dvi.ticket.model.TenantSetting;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TenantSettingRepository extends JpaRepository<TenantSetting, Long> {

    TenantSetting findById(long id);
}
