package com.dvi.ticket.repository;

import com.dvi.ticket.model.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends JpaRepository<Project, Long> {

    Project findById(long id);

    Page<Project> findAllByNameContaining(String name, Pageable pageable);

    @Query(value = "SELECT p.id FROM projects p INNER JOIN tickets t ON t.project_id = p.id WHERE t.tenant_id = ?1 AND t.status = ?2 GROUP BY p.id ORDER BY COUNT(t.status) DESC LIMIT ?3", nativeQuery = true)
    List<Long> findAllByTenantIdAndStatus(long tenantId, String status, int limit);
}
