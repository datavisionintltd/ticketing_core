package com.dvi.ticket.repository;

import com.dvi.ticket.model.Task;
import com.dvi.ticket.model.Ticket;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskRepository extends JpaRepository<Task, Long> {

    Task findById(long id);

    Page<Task> findAllByNameContaining(String name, Pageable pageable);

    Page<Task> findAllByTicket(Ticket ticket, Pageable pageable);
}
