package com.dvi.ticket.repository;

import com.dvi.ticket.model.Project;
import com.dvi.ticket.model.Ticket;
import com.dvi.ticket.model.Topic;
import com.dvi.ticket.model.User;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TicketRepository extends JpaRepository<Ticket, Long> {

    Ticket findById(long id);
    @Query(value = "SELECT t.* from tickets t WHERE t.tenant_id = ?1 AND t.number IS NOT NULL ORDER BY t.number DESC LIMIT 1", nativeQuery = true)
    Ticket findByLastAdded(long tenantId);

    @Query(value = "SELECT COUNT(*) FROM tickets t INNER JOIN projects p ON t.project_id = p.id WHERE t.tenant_id = ?1 AND p.client_id = ?2 AND t.status = ?3", nativeQuery = true)
    long countByTenantIdAndClientIdAndStatus(long tenantId, long clientId, String status);
    long countByProjectAndStatus(Project project, String status);
    @Query(value = "SELECT COUNT(*) FROM tickets t INNER JOIN ticket_users tu ON tu.ticket_id = t.id WHERE t.tenant_id = ?1 AND tu.user_id = ?2 AND t.status = ?3", nativeQuery = true)
    long countByTenantIdAndAgentIdAndStatus(long tenantId, long agentId, String status);

    Page<Ticket> findAllByIdIn(List<Long> ids, Pageable pageable);
    Page<Ticket> findAllByUser(User user, Pageable pageable);
    Page<Ticket> findAllByTopic(Topic topic, Pageable pageable);
    Page<Ticket> findAllByProject(Project project, Pageable pageable);

    Page<Ticket> findAllByUserAndTopic(User user, Topic topic, Pageable pageable);
    Page<Ticket> findAllByUserAndProject(User user, Project project, Pageable pageable);
    Page<Ticket> findAllByTopicAndProject(Topic topic, Project project, Pageable pageable);

    Page<Ticket> findAllByUserAndTopicAndProject(User user, Topic topic, Project project, Pageable pageable);

    @Query(value = "SELECT t.* FROM tickets t WHERE t.tenant_id = ?1 AND t.status = ?2 AND DATE_FORMAT(t.created_at, '%Y-%m-%d') >= ?3", nativeQuery = true,
            countQuery = "SELECT COUNT(*) FROM tickets t WHERE t.tenant_id = ?1 AND t.status = ?2 AND DATE_FORMAT(t.created_at, '%Y-%m-%d') >= ?3")
    Page<Ticket> findAllByStatusAndAndCreatedAt(long tenantId, String status, String createdAt, Pageable pageable);
    @Query(value = "SELECT COUNT(*) FROM tickets t WHERE t.tenant_id = ?1 AND t.status = ?2 AND DATE_FORMAT(t.created_at, '%Y-%m-%d') >= ?3", nativeQuery = true)
    long countByStatusAndCreatedAt(long tenantId, String status, String createdAt);
}
