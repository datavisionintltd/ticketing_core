package com.dvi.ticket.repository;

import com.dvi.ticket.model.Team;
import com.dvi.ticket.model.TeamUser;
import com.dvi.ticket.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamUserRepository extends JpaRepository<TeamUser, Long> {

    TeamUser findById(long id);
    TeamUser findByUserAndTeam(User user, Team team);

    Page<TeamUser> findAllByUser(User user, Pageable pageable);
    Page<TeamUser> findAllByTeam(Team team, Pageable pageable);
}
