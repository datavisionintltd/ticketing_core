package com.dvi.ticket.repository;

import com.dvi.ticket.model.TaskUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaskUserRepository extends JpaRepository<TaskUser, Long> {

    TaskUser findById(long id);
}
