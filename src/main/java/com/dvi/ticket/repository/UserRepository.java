package com.dvi.ticket.repository;

import com.dvi.ticket.model.Role;
import com.dvi.ticket.model.User;
import com.dvi.ticket.model.UserGroup;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    User findById(long id);
    User findByUsername(String username);
    User findByEmail(String email);
    User findByUsernameOrEmail(String username, String email);

    @Modifying
    @Query(value = "UPDATE users u SET u.tenant_id = ?2 WHERE u.id = ?1", nativeQuery = true)
    void updateUser(long userId, long tenantId);

    List<User> findAll();
    Page<User> findAll(Pageable pageable);

    Page<User> findAllByFullNameContainingOrUsernameContainingOrEmailContainingOrPhoneNumberContaining(String fullName, String username, String email, String phoneNumber, Pageable pageable);
    Page<User> findAllByUserGroupAndFullNameContainingOrUsernameContainingOrEmailContainingOrPhoneNumberContaining(UserGroup userGroup, String fullName, String username, String email, String phoneNumber, Pageable pageable);

    @Query(value = "SELECT usr.* FROM users usr WHERE usr.tenant_id = ?1 AND usr.user_group_id = ?2 AND (usr.full_name LIKE %?3% OR usr.username LIKE %?3% OR usr.email LIKE %?3% OR usr.phone_number LIKE %?3%)",
            countQuery = "SELECT COUNT(*) FROM users usr WHERE usr.tenant_id = ?1 AND usr.user_group_id = ?2 AND (usr.full_name LIKE %?3% OR usr.username LIKE %?3% OR usr.email LIKE %?3% OR usr.phone_number LIKE %?3%)",
            nativeQuery = true)
    Page<User> findAllBySearch(long tenantId, long userGroupId, String searchKey, Pageable pageable);

    @Query(value = "SELECT tu.user_id FROM ticket_users tu INNER JOIN tickets t ON tu.ticket_id = t.id WHERE t.tenant_id = ?1 AND t.status = ?2 GROUP BY tu.user_id ORDER BY COUNT(t.status) DESC LIMIT ?3", nativeQuery = true)
    List<Long> findAllByTenantIdAndStatus(long tenantId, String status, int limit);
}
