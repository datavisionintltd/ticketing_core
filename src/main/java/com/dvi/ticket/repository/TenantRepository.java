package com.dvi.ticket.repository;

import com.dvi.ticket.model.Tenant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface TenantRepository extends JpaRepository<Tenant, Long> {

    Tenant findById(long id);
    Tenant findByTillNumber(String tillNumber);
    Tenant findByUuid(String uuid);
    @Query(value = "SELECT t.* FROM companies t WHERE t.name LIKE %?1% ORDER BY t.id ASC LIMIT 1", nativeQuery = true)
    Tenant findFirstByNameContaining(String name);
    @Query(value = "SELECT t.* from companies t ORDER BY t.till_suffix DESC LIMIT 1", nativeQuery = true)
    Tenant findByLastAdded();
    // Optional<Tenant> findById(String companyId);
    // Fetch company by email
    //Company findByEmail(String email);

}
