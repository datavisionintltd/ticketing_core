package com.dvi.ticket.repository;

import com.dvi.ticket.model.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {

    Client findById(long id);

    Page<Client> findAllByNameContainingOrEmailContainingOrPhoneContaining(String name, String email, String phone, Pageable pageable);

    @Query(value = "SELECT c.id FROM client c INNER JOIN projects p ON p.client_id = c.id INNER JOIN tickets t ON t.project_id = p.id WHERE t.tenant_id = ?1 AND t.status = ?2 GROUP BY c.id ORDER BY COALESCE(COUNT(t.status), 0) DESC LIMIT ?3", nativeQuery = true)
    List<Long> findAllByTenantIdAndStatus(long tenantId, String status, int limit);
}
