package com.dvi.ticket.repository;

import com.dvi.ticket.model.TicketStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketStatusRepository extends JpaRepository<TicketStatus, Long> {

    TicketStatus findById(long id);
    TicketStatus findByDetails(String details);

    Page<TicketStatus> findAllByDetailsContaining(String details, Pageable pageable);
}
