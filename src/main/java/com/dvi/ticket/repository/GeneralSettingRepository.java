package com.dvi.ticket.repository;

import com.dvi.ticket.model.GeneralSetting;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GeneralSettingRepository extends JpaRepository<GeneralSetting, Long> {

    GeneralSetting findById(long id);

    Page<GeneralSetting> findAllByNameContaining(String name, Pageable pageable);
}
