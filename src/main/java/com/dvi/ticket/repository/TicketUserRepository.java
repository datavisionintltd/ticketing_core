package com.dvi.ticket.repository;

import com.dvi.ticket.model.TicketUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketUserRepository extends JpaRepository<TicketUser, Long> {

    TicketUser findById(long id);
}
