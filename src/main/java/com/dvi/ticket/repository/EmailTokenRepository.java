package com.dvi.ticket.repository;

import com.dvi.ticket.model.EmailToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.List;

@Repository
public interface EmailTokenRepository extends JpaRepository<EmailToken, Long> {

    EmailToken findById(long id);
    EmailToken findByEmail(String email);
    EmailToken findByToken(String token);

    List<EmailToken> findAll();
    List<EmailToken> findAllByAddedTimeAfter(Timestamp addedTime);
}
