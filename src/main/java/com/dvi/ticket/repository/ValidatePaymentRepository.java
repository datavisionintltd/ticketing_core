package com.dvi.ticket.repository;

import com.dvi.ticket.model.ValidatePayment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ValidatePaymentRepository extends JpaRepository<ValidatePayment, Long> {

    ValidatePayment findById(long id);
    ValidatePayment findByMkey(String mkey);
}
