package com.dvi.ticket.repository;

import com.dvi.ticket.model.Role;
import com.dvi.ticket.model.User;
import com.dvi.ticket.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

    UserRole findById(long id);
    UserRole findByUserAndRole(User user, Role role);
    @Query(value = "SELECT urs.* FROM user_roles urs WHERE urs.roles_id = ?1 ORDER BY urs.id ASC LIMIT 1", nativeQuery = true)
    UserRole findFirstUserRoleByRoleId(long roleId);

    List<UserRole> findAllByUser(User user);
    List<UserRole> findAllByRole(Role role);
    List<UserRole> findAll();
}
