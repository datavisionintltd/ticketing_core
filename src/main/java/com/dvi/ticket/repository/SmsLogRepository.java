package com.dvi.ticket.repository;

import com.dvi.ticket.model.SmsLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SmsLogRepository extends JpaRepository<SmsLog, Long> {

    SmsLog findById(long id);

    List<SmsLog> findAllBySender(String sender);
    List<SmsLog> findAllByRecipient(String recipient);
}
