package com.dvi.ticket.repository;

import com.dvi.ticket.model.SmsFee;
import com.dvi.ticket.model.Tenant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SmsFeeRepository extends JpaRepository<SmsFee, Long> {

    SmsFee findById(long id);

    Page<SmsFee> findAllByTenant(Tenant tenant, Pageable pageable);
}
