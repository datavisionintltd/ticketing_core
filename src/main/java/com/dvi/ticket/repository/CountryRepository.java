package com.dvi.ticket.repository;

import com.dvi.ticket.model.Country;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {

    Country findById(long id);
    Country findByDialCode(String dialCode);
    Country findByCode(String code);

    Page<Country> findAllByNameContaining(String name, Pageable pageable);
}
