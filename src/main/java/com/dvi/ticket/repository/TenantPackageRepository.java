package com.dvi.ticket.repository;

import com.dvi.ticket.model.TenantPackage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TenantPackageRepository extends JpaRepository<TenantPackage, Long> {

    TenantPackage findById(long id);
}
