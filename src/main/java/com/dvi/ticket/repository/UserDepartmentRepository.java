package com.dvi.ticket.repository;

import com.dvi.ticket.model.Department;
import com.dvi.ticket.model.User;
import com.dvi.ticket.model.UserDepartment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDepartmentRepository extends JpaRepository<UserDepartment, Long> {

    UserDepartment findById(long id);
    UserDepartment findByUser(User user);

    Page<UserDepartment> findAllByDepartment(Department department, Pageable pageable);
}
