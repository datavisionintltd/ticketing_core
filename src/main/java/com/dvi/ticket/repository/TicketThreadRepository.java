package com.dvi.ticket.repository;

import com.dvi.ticket.model.Ticket;
import com.dvi.ticket.model.TicketThread;
 
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketThreadRepository extends JpaRepository<TicketThread, Long> {

    TicketThread findById(long id);

    Page<TicketThread> findAllByTicket(Ticket ticket, Pageable pageable);
}
