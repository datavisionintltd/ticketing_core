package com.dvi.ticket.repository;

import com.dvi.ticket.model.PaymentLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentLogRepository extends JpaRepository<PaymentLog, Long> {

    PaymentLog findById(long id);
}
