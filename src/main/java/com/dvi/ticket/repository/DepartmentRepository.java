package com.dvi.ticket.repository;

import com.dvi.ticket.model.Department;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends JpaRepository<Department, Long> {

    Department findById(long id);

    Page<Department> findAllByNameContaining(String name, Pageable pageable);
}
