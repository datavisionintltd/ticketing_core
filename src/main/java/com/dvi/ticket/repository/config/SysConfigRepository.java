package com.dvi.ticket.repository.config;

import com.dvi.ticket.model.config.SysConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SysConfigRepository extends JpaRepository<SysConfig, Long> {

    SysConfig findById(long id);
    SysConfig findByConfigName(String configName);

    List<SysConfig> findAll();
}
